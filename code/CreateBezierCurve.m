function [A] = CreateBezierCurve(P,nPoints,plotOption)
%%% Description
% This function constructs a Bezier curve based on the script mybez.m by
% Sagay Aiya. Changed input and output format. Output coordinates are in
% the same units as the input coordinates.
%
% Input:
%   - P: [3 x m] array containing the x/y/z coordinates (one per row) of
%       the m control points points defining the Bezier curve. 
%   - nPoints: number of points on the output curve; more points creates
%       a smoother curve.
%   - plotOption: show figure with created Bezier curve in 3D space
%
%  Output:
%   - B: [3 x nPoints] array containing the x/y/z coordinates of the 
%       nPoints along the curve
%
%%% Reynard de Vries
%%% TU Delft
%%% First created: 18-03-22
%%% Last modified: 18-03-22

% Preallocate
A = NaN(3,nPoints);

% Number of segments of the curve
nSegments = nPoints - 1;

% Number of input points
n = size(P,2);

% Create curve
count = 1;
for u = 0:(1/nSegments):1
    sum = [0 0 0]';
    for i = 1:n
        
        % Bernstein polynomial value, B
        B = nchoosek(n,i-1)*(u^(i-1))*((1-u)^(n-i+1)); 
        sum = sum + B*P(:,i);
    end
    B = nchoosek(n,n)*(u^(n));
    sum = sum + B*P(:,n);
    A(:,count) = sum; 
    count = count+1;  
end

% Plot curve if requested
if plotOption == 1
    fig = figure;
    fig.Name = 'Bezier curve';
    hold on; grid on; box on
    plot3(P(1,:),P(2,:),P(3,:),'o-r');
    plot3(A(1,:),A(2,:),A(3,:),'.-b');
    axis equal; view(3);
    xlabel('x'); ylabel('y');zlabel('z');
    legend('Control points','Bezier curve')    
end







