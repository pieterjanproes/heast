function [geom,P] = CreateLiftingSurfaceGeometry(geom_in,settings)
%%% Description
% This function computes the geometry of a lifting surface (e.g. a
% semi-wing). Only applicable to single-taper surfaces. 
%
% Input:
%   - geom_in: structure containing geometrical info of the planform:
%       - S: planform reference area [m2] (as viewed normal to the wing,
%           not viewed from top)
%       - AR: aspect ratio, b/c_mean [-]. Note that this referes to the
%           aspect ratio of one trapezoidal semi-wing, so for a full wing,
%           it is half the true aspect ratio of the complete wing.
%       - TR: taper ratio, c_tip/c_root [-]
%       - sweep_c025: quarter-chord sweep angle [deg]. 
%       - dihedral: dihedral angle [deg] (0 = horizontal, 90 = vertical)
%       - root.tc: root thickness-to-chord ratio [-]
%       - root.incidence: root incidence angle, positive nose-up [deg].
%           Rotation performed around quarter-chord
%       - tip.tc: tip thickness-to-chord ratio [-]
%       - tip.incidence: tip incidence angle, positive nose-up [deg].
%           Rotation performed around quarter-chord
%       - xc_spar_front: axial location (as fraction of chord) of front
%           spar [-]
%       - xc_spar_rear: axial location (as fraction of chord) of rear
%           spar [-]
%       - d_skin: vertical distance between wing box and outer airfoil
%           surface, as a fraction of max airfoil thickness at that
%           location [-]
%       - sFraction: fraction of external surface area covered because it's
%           "inside" another component, i.e. ratio of non-exposed external
%           area to total external area.
%       - fn_airfoil_root: filename to a file containing x/y coordinates of
%           root airfoil section, given as a [n x 2] array. Coordinates must be
%           specified from TE to LE via bottom, and back to TE via top. The
%           file should not contain headers. Values separated by tabs.
%       - fn_airfoil_tip: filename to a file containing x/y coordinates of
%           root airfoil section, given as a [n x 2] array. Coordinates must be
%           specified from TE to LE via bottom, and back to TE via top. The
%           file should not contain headers. Values separated by tabs.
%   - settings: program/plotting/discretization settings:
%       - nPoints: number of points used to define each side of the airfoil
%       - plotOption: show figure with geometry if = 1
%
% Outout:
%   - geom: "geom_in" input structure with additional info added:
%       - b: span [m] (of semi-wing)
%       - root.c: root chord [m]
%       - tip.c: tip chord [m]
%       - root.t: root thickness [m]
%       - tip.t: tip thickness [m]
%       - c_mean: average chord [m]
%       - c_MAC: mean aerodynamic chord [m]
%       - y_MAC: spanwise coordinate of MAC [m] (NB: along span, not along
%           Y-direction!)
%       - x_MAC_LE: x-coordinate of MAC leading-edge [m]
%       - x_MAC_c025: x-coordinate of MAC quarter-chord [m]
%       - sweep_LE: leading-edge sweep angle [deg]
%       - sweep_TE: trailing-edge sweep angle [deg]
%       - S_ext: surface area of the wing segment including tip cap, but
%           excluding root cap [m2]
%       - S_wet: wetted surface area [m2]; same as S_ext but corrected by a
%           factor accounting for the exposed area
%   - P: Cell array, containing the x/y/z coordinates 
%       (1st dim) of the points defining the airfoil (2nd dim), for the
%       root and tip sections (3rd dim). Points are ordered from TE to LE
%       via bottom and back to TE via top. 
%       - P{1}: Wing surface, of size [3 x (nPoints - 1) x 3], where
%           section (:,:,1) is the root airfoil, section (:,:,2) is the MAC
%           airfoil, and section (:,:,3) is the tip airfoil.
%       - P{2}: Wing box, of size [3 x n x 2] (with n approximately equal
%           to nPoints), where section (:,:,1) is the root section, and
%           section (:,:,2) is the tip section.
%
% Note regarding coordinate system: the origin is located at the root LE.
% The starboard surface is created (for dihedral < 90 deg)
% x = zero at root leading edge, positive downstream
% y = zero through root chord line, postive starboard
% z = zero through root chord line, positive upwards
%
%%% Reynard de Vries
%%% First created: 18-03-22
%%% Last modified: 13-04-22

% For testing 
%{
clear
close all
clc
tic

% Geometrical characteristics
geom_in.S = 50;                 % planform area [m2] 
geom_in.AR = 6;                 % Aspect ratio, b/c_mean [-]
geom_in.TR = 0.3;               % Taper ratio, c_tip/c_root [-]
geom_in.sweep_c025 = 15;        % Quarter-chord sweep angle [deg]
geom_in.dihedral = 35;          % Dihedral angle [deg] (0 = horizontal, 90 = vertical)
geom_in.root.tc = 0.18;         % Root thickness-to-chord ratio [-]
geom_in.root.incidence = 45;    % Root incidence angle [deg]
geom_in.tip.tc = 0.12;          % Tip thickness-to-chord ratio [-]
geom_in.tip.incidence = -15;    % Tip incidence angle [deg]
geom_in.xc_spar_front = 0.2;    % Axial location (as fraction of chord) of front spar [-]
geom_in.xc_spar_rear = 0.7;     % Axial location (as fraction of chord) of rear spar [-]
geom_in.d_skin = 0.05;          % Vertical distance between wing box and outer airfoil surface,
                                % as a fraction of max airfoil thickness at that location [-]
geom_in.sFraction = 0.1;        % Fraction of external surface area covered because it's "inside" another component

% Airfoil shapes                                
geom_in.fn_airfoil_root = 'Airfoil_NACA0012.txt';
geom_in.fn_airfoil_tip = 'Airfoil_NACA0012.txt';

% Sesttings
settings.nPoints = 50;          % Number of points used to define each side of the airfoil
settings.plotOption = 1;        % Show figure with geometry if = 1
%}


%% Load and prepare airfoil data

% Copy geom to output structure
geom = geom_in;
plotOption = settings.plotOption;
nPoints = settings.nPoints;

% Pre-open figure showing airfoil info
if plotOption == 1
    fig = figure; fig.Name = 'Lifting surface airfoil sections';
end

% Operate on root and tip
fn_airfoils = {geom.fn_airfoil_root,geom.fn_airfoil_tip};
varNames = {'root','tip'};
for k = 1:2
    
    % Extract arrays from file
    fid = fopen(fn_airfoils{k});
    coordsIn = textscan(fid,'%f\t%f');
    fclose(fid);

    % Normalize to chord-length = 1, in case that is not the case, and make
    % sure LE is at (0,0)
    xLE_in = min(coordsIn{1});
    xTE_in = max(coordsIn{1});
    cIn = (xTE_in - xLE_in);
    [~,idxLE] = min(coordsIn{1});
    zLE_in = coordsIn{2}(idxLE);
    coordsIn{1} = (coordsIn{1} - xLE_in)/cIn;
    coordsIn{2} = (coordsIn{2} - zLE_in)/cIn;
    
    % Distinguish between upper and lower sides
    xLoIn = coordsIn{1}(1:idxLE);
    xUpIn = coordsIn{1}(idxLE:end);
    zLoIn = coordsIn{2}(1:idxLE);
    zUpIn = coordsIn{2}(idxLE:end);
            
    % Flip such that all coordinates now go from LE to TE
    xLoIn = flip(xLoIn);
    zLoIn = flip(zLoIn);
    
	% If middle of TE is not at Z = 0, adjust height so that chord line is
	% at z = 0 (note that x goes from 0 to 1)
    zTE_in = mean([zLoIn(end) zUpIn(end)]);
    zLoIn = zLoIn - zTE_in*xLoIn;
    zUpIn = zUpIn - zTE_in*xUpIn;
    
    % If trailing edge is closed, interpolate to cosine distribution
    thetaMax = 2/3*pi;
    if zLoIn(end) == zUpIn(end)
        theta = linspace(0,thetaMax,nPoints);
        xIntp = (1-cos(theta))/(1-cos(thetaMax));
        zUp = interp1(xUpIn,zUpIn,xIntp,'linear');
        zLo = interp1(xLoIn,zLoIn,xIntp,'linear');
    
    % If trailing edge is open, add additional point at z = 0. 
    else 
        theta = linspace(0,thetaMax,nPoints-1);
        xIntp = (1-cos(theta))/(1-cos(thetaMax));
        zUp = interp1(xUpIn,zUpIn,xIntp,'linear');
        zLo = interp1(xLoIn,zLoIn,xIntp,'linear');
        xIntp = [xIntp 1];
        zUp = [zUp 0];
        zLo = [zLo 0];
    end
    
    % Re-order from TE to LE via bottom, and back via top 
    geom.(varNames{k}).x_airfoil = [flip(xIntp) xIntp(2:end)];
    geom.(varNames{k}).z_airfoil = [flip(zLo) zUp(2:end)];
    
    % Compute max thickness (coordinates now defined at same x-locations
    % on upper and lower sides) to scale later, store x/c location of max
    % thickness
    [t_max.(varNames{k}),idxMax] = max(zUp - zLo);
    geom.(varNames{k}).xc_max_t = xIntp(idxMax);
    
    % For checking
    if plotOption == 1
        subplot(2,2,k); title([varNames{k} ' airfoil'])
        hold on; grid on; axis equal; box on
        plot(coordsIn{1},coordsIn{2},'.-r')
        plot(xIntp,zLo,'ob')
        plot(xIntp,zUp,'og')
        plot(geom.(varNames{k}).x_airfoil,geom.(varNames{k}).z_airfoil,'.-k')
        legend('Input','Lower side','Upper side','Output')
        xlabel('x/c'); ylabel('z/c')
    end
end


%% Calculate dimensions

% Span
geom.b = (geom.S*geom.AR)^0.5;

% Chord lengths
geom.c_mean = geom.S/geom.b;
geom.root.c = 2*geom.c_mean/(1 + geom.TR);
geom.tip.c = geom.TR*geom.root.c;

% Thicknesses
geom.root.t = geom.root.tc*geom.root.c;
geom.tip.t = geom.tip.tc*geom.tip.c;

% Projected chord lengths
geom.root.c_projected = geom.root.c*cosd(geom.root.incidence);
geom.tip.c_projected = geom.tip.c*cosd(geom.tip.incidence);

% Axial, lateral and vertical locations of root and tip airfoils
geom.root.x_LE = 0;
geom.root.x_c025 = 0.25*geom.root.c_projected;
geom.root.z_c025 = 0;
geom.root.y_c025 = 0;
geom.tip.x_c025 = 0.25*geom.root.c_projected + geom.b*tand(geom.sweep_c025);
geom.tip.y_c025 = geom.b*cosd(geom.dihedral);
geom.tip.z_c025 = geom.b*sind(geom.dihedral);
geom.tip.x_LE = geom.tip.x_c025 - 0.25*geom.tip.c_projected;

% Calculate sweep angles (incl. max thickness location for CD0 estimation)
geom.sweep_LE = atand((geom.tip.x_LE - geom.root.x_LE)/geom.b);
geom.sweep_TE = atand((geom.tip.x_LE + geom.tip.c_projected - ...
                       geom.root.x_LE - geom.root.c_projected)/geom.b);
geom.sweep_c050 = atand((geom.tip.x_LE + 0.5*geom.tip.c_projected - ...
                         geom.root.x_LE - 0.5*geom.root.c_projected)/geom.b);
geom.sweep_t_max = atand((geom.tip.x_LE + geom.tip.xc_max_t*geom.tip.c_projected - ...
                         geom.root.x_LE - geom.root.xc_max_t*geom.root.c_projected)/geom.b);
                     
% Determine mean aerodynamic chord & location. Assuming linear twist
% distribution
geom.MAC.c = 2/3*geom.root.c*(1 + geom.TR + geom.TR^2)/(1 + geom.TR);
geom.MAC.b = (geom.b*2)/6*(1 + 2*geom.TR)/(1 + geom.TR); % Spanwise location
geom.MAC.incidence = geom.root.incidence + geom.MAC.b/geom.b*...
                     (geom.tip.incidence - geom.root.incidence);
geom.MAC.c_projected = geom.MAC.c * cosd(geom.MAC.incidence);                 
geom.MAC.x_c025 = geom.root.x_c025 + (geom.tip.x_c025 - geom.root.x_c025)*geom.MAC.b/geom.b;
geom.MAC.y_c025 = geom.tip.y_c025*geom.MAC.b/geom.b;
geom.MAC.z_c025 = geom.tip.z_c025*geom.MAC.b/geom.b;
geom.MAC.x_LE = geom.MAC.x_c025 - 0.25*geom.MAC.c_projected;

% Scale airfoils
geom.root.x_scaled = geom.root.x_airfoil*geom.root.c;
geom.root.z_scaled = geom.root.z_airfoil*geom.root.t/t_max.root;
geom.tip.x_scaled = geom.tip.x_airfoil*geom.tip.c;
geom.tip.z_scaled = geom.tip.z_airfoil*geom.tip.t/t_max.tip;

% Surface area of tip section
S_tip = trapz(xIntp*geom.tip.c,(zUp-zLo)*geom.tip.t/t_max.tip);


%% Create geometry for plot

% Rotate airfoils around quarter-chord and shift in X-direction to align
% most upstream point with X = 0
x_root_airfoil = 0.25*geom.root.c_projected + ...
    (geom.root.x_scaled - 0.25*geom.root.c)*cosd(geom.root.incidence) +...
    (geom.root.z_scaled)*sind(geom.root.incidence);
z_root_airfoil = (geom.root.z_scaled)*cosd(geom.root.incidence) - ...
    (geom.root.x_scaled - 0.25*geom.root.c)*sind(geom.root.incidence);
x_tip_airfoil = 0.25*geom.tip.c_projected + ...
    (geom.tip.x_scaled - 0.25*geom.tip.c)*cosd(geom.tip.incidence) +...
    (geom.tip.z_scaled)*sind(geom.tip.incidence);
z_tip_airfoil = (geom.tip.z_scaled)*cosd(geom.tip.incidence) - ...
    (geom.tip.x_scaled - 0.25*geom.tip.c)*sind(geom.tip.incidence);

% Store in [3 x n x 2] matrix, for horizontal wing
n = length(x_root_airfoil);
P_hor(:,:,1) = [x_root_airfoil;
                zeros(1,n);
                z_root_airfoil];
P_hor(:,:,2) = [x_tip_airfoil + geom.tip.x_LE;
                geom.b*ones(1,n);
                z_tip_airfoil];
            
% Rotate around X-axis to account for dihedral, shift in X-direction so
% that LE is at x = 0 after applying twist
P_wing(1,:,:) = P_hor(1,:,:); 
P_wing(2,:,:) = P_hor(2,:,:)*cosd(geom.dihedral) - P_hor(3,:,:)*sind(geom.dihedral);
P_wing(3,:,:) = P_hor(3,:,:)*cosd(geom.dihedral) + P_hor(2,:,:)*sind(geom.dihedral);

% Compute external area (excl. root and tip caps) 
dS = NaN(1,n-1);
for i = 1:(n-1)
    
    % Length of airfoil segment between points i and i+1, root section
    dl1 = ((P_wing(1,i,1) - P_wing(1,i+1,1))^2 + ...
           (P_wing(2,i,1) - P_wing(2,i+1,1))^2 + ...
           (P_wing(3,i,1) - P_wing(3,i+1,1))^2)^0.5;
       
    % Length of airfoil segment between points i and i+1, tip section
    dl2 = ((P_wing(1,i,2) - P_wing(1,i+1,2))^2 + ...
           (P_wing(2,i,2) - P_wing(2,i+1,2))^2 + ...
           (P_wing(3,i,2) - P_wing(3,i+1,2))^2)^0.5;
       
    % Spanwise distance between the center of the two segments
    db = (((P_wing(1,i,1) + P_wing(1,i+1,1))/2 - (P_wing(1,i,2) + P_wing(1,i+1,2))/2)^2 + ...
          ((P_wing(2,i,1) + P_wing(2,i+1,1))/2 - (P_wing(2,i,2) + P_wing(2,i+1,2))/2)^2 + ...
          ((P_wing(3,i,1) + P_wing(3,i+1,1))/2 - (P_wing(3,i,2) + P_wing(3,i+1,2))/2)^2)^0.5;
      
    % Area of twisted trapezoid
    dS(i) = db*(dl1 + dl2)/2;
end
S_ext_exclTip = sum(dS);

% Add contribution of tip cap and correct wetted area with exposed fraction
geom.S_ext = S_ext_exclTip + S_tip;
geom.S_wet = geom.S_ext*(1- geom_in.sFraction);

% Calculate MAC location through interpolation
spanFraction = geom.MAC.b/geom.b;
P_MAC = NaN(size(P_wing(:,:,1)));
for i = 1:n
    P_MAC(1,i) = interp1([0 1],[P_wing(1,i,1) P_wing(1,i,2)],spanFraction,'linear');
    P_MAC(2,i) = interp1([0 1],[P_wing(2,i,1) P_wing(2,i,2)],spanFraction,'linear');
    P_MAC(3,i) = interp1([0 1],[P_wing(3,i,1) P_wing(3,i,2)],spanFraction,'linear');
end

% Insert in original P-structure as intermediate section, so P(:,:,1) =
% root, P(:,:,2) = MAC, P(:,:,3) = Tip.
P_wing(:,:,3) = P_wing(:,:,2);
P_wing(:,:,2) = P_MAC;


%% Create geometry for plot: wing box

% Root airfoil: Interpolate top and bottom sides on x-coordinates between 
% spars. Note that we do not take first and last point of airfoil for
% interpolation, in case TE thickness is finite, and there are two points
% at the TE. This only works if we have enough points! There must be at
% least one point dowstream of xc_spar_rear. To avoid issues, make sure
% that nPoints >> 1
if nPoints < 10 % For example
    error('Please use more than 10 points per side to define the airfoils')
end
x_root_box = linspace(geom.xc_spar_front,geom.xc_spar_rear,ceil(nPoints/2))*geom.root.c;
[~,idxLE] = min(geom.root.x_scaled);
z_root_box_Up = interp1(geom.root.x_scaled(idxLE:(end-1)),geom.root.z_scaled(idxLE:(end-1)),x_root_box);
z_root_box_Lo = interp1(geom.root.x_scaled(2:idxLE),geom.root.z_scaled(2:idxLE),x_root_box);

% Offset from skin
z_root_box_Up = z_root_box_Up - geom.d_skin*geom.root.t;
z_root_box_Lo = z_root_box_Lo + geom.d_skin*geom.root.t;

% Rearrange into single array
geom.root.x_box = [flip(x_root_box) x_root_box x_root_box(end)];
geom.root.z_box = [flip(z_root_box_Lo) z_root_box_Up z_root_box_Lo(end)];

% Same steps for tip airfoil
x_tip_box = linspace(geom.xc_spar_front,geom.xc_spar_rear,ceil(nPoints/2))*geom.tip.c;
[~,idxLE] = min(geom.tip.x_scaled);
z_tip_box_Up = interp1(geom.tip.x_scaled(idxLE:end-1),geom.tip.z_scaled(idxLE:end-1),x_tip_box);
z_tip_box_Lo = interp1(geom.tip.x_scaled(2:idxLE),geom.tip.z_scaled(2:idxLE),x_tip_box);
z_tip_box_Up = z_tip_box_Up - geom.d_skin*geom.tip.t;
z_tip_box_Lo = z_tip_box_Lo + geom.d_skin*geom.tip.t;
geom.tip.x_box = [flip(x_tip_box) x_tip_box x_tip_box(end)];
geom.tip.z_box = [flip(z_tip_box_Lo) z_tip_box_Up z_tip_box_Lo(end)];

% For checking
if plotOption == 1
    subplot(2,2,3); title('Root airfoil, scaled')
    plot(geom.root.x_scaled,geom.root.z_scaled,'.-r')
    hold on; box on; axis equal; grid on
    plot(geom.root.x_box,geom.root.z_box,'.-k')
    xlabel('x [m]'); ylabel('z [m]')
    subplot(2,2,4); title('Tip airfoil, scaled')
    plot(geom.tip.x_scaled,geom.tip.z_scaled,'.-r')
    hold on; box on; axis equal; grid on
    plot(geom.tip.x_box,geom.tip.z_box,'.-k')
    xlabel('x [m]'); ylabel('z [m]')
    legend('Airfoil','Wing box')
end

% Create P-structure to plot in 3D space
% First Rotate airfoils around quarter-chord and shift in X-direction to align
% most upstream point with X = 0
x_root_box = 0.25*geom.root.c_projected + ...
    (geom.root.x_box - 0.25*geom.root.c)*cosd(geom.root.incidence) +...
    (geom.root.z_box)*sind(geom.root.incidence);
z_root_box = (geom.root.z_box)*cosd(geom.root.incidence) - ...
    (geom.root.x_box - 0.25*geom.root.c)*sind(geom.root.incidence);
x_tip_box = 0.25*geom.tip.c_projected + ...
    (geom.tip.x_box - 0.25*geom.tip.c)*cosd(geom.tip.incidence) +...
    (geom.tip.z_box)*sind(geom.tip.incidence);
z_tip_box = (geom.tip.z_box)*cosd(geom.tip.incidence) - ...
    (geom.tip.x_box - 0.25*geom.tip.c)*sind(geom.tip.incidence);

% Store in [3 x n x 2] matrix, for horizontal box
n_box = length(x_root_box);
P_box_hor(:,:,1) = [x_root_box;
                    zeros(1,n_box);
                    z_root_box];
P_box_hor(:,:,2) = [x_tip_box + geom.tip.x_LE;
                    geom.b*ones(1,n_box);
                    z_tip_box];
            
% Rotate around X-axis to account for dihedral, shift in X-direction so
% that LE is at x = 0 after applying twist
P_box(1,:,:) = P_box_hor(1,:,:); 
P_box(2,:,:) = P_box_hor(2,:,:)*cosd(geom.dihedral) - P_box_hor(3,:,:)*sind(geom.dihedral);
P_box(3,:,:) = P_box_hor(3,:,:)*cosd(geom.dihedral) + P_box_hor(2,:,:)*sind(geom.dihedral);

% Save output in cell aray
P{1} = P_wing;
P{2} = P_box;


%% Plotting 

% Create 3D figure
if plotOption == 1
    fig = figure;
    fig.Name = 'Lifting surface geometry';
    hold on; grid on; box on;
    xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]');
    axis equal; view(3);
    
    % Wing surface
    xx = squeeze(P_wing(1,:,:));
    yy = squeeze(P_wing(2,:,:));
    zz = squeeze(P_wing(3,:,:));
    surf(xx,yy,zz,'facecolor',[0.5 0.5 0.5],'edgecolor','none','facealpha',0.5)
    
    % Wing box
    xx = squeeze(P_box(1,:,:));
    yy = squeeze(P_box(2,:,:));
    zz = squeeze(P_box(3,:,:));
    surf(xx,yy,zz,'facecolor','k','edgecolor','none','facealpha',0.5)
    
    % Create root & tip caps
    fill3(P_wing(1,:,1),P_wing(2,:,1),P_wing(3,:,1),[0.5 0.5 0.5],'edgecolor','r','facealpha',0.5)
    fill3(P_wing(1,:,3),P_wing(2,:,3),P_wing(3,:,3),[0.5 0.5 0.5],'edgecolor','b','facealpha',0.5)
    
    % Plot MAC
    plot3(P_MAC(1,:),P_MAC(2,:),P_MAC(3,:),'-m')
    
    % Plot LE
    plot3(squeeze(P_wing(1,ceil(n/2),:)),...
          squeeze(P_wing(2,ceil(n/2),:)),...
          squeeze(P_wing(3,ceil(n/2),:)),'.-g')
      
    % Plot TE
    plot3(squeeze(P_wing(1,1,:)),squeeze(P_wing(2,1,:)),squeeze(P_wing(3,1,:)),'.-c')      
      
    % Plot quarter-chord
    plot3([geom.root.x_c025 geom.MAC.x_c025 geom.tip.x_c025],...
          [geom.root.y_c025 geom.MAC.y_c025 geom.tip.y_c025],...
          [geom.root.z_c025 geom.MAC.z_c025 geom.tip.z_c025],'.-y')
      
    % Add legend
    legend('Wing surface','Wing box','Root airfoil','Tip airfoil','MAC airfoil',...
           'Leading edge','Trailing edge','Quarter chord')
end















