%%% Description
% This script is a wrapper for ConfigurationAndGeometry.m, which is called
% in the Class-II convergence loop. It assigns input for
% ConfigurationAndGeometry.m, and extracts the output values that have to
% be updated for the next iteration - as well as computing the relative
% error of the updated estimates relative to the previous iteration.
%
%%% Reynard de Vries
%%% First created: 06-04-22
%%% Last modified: 04-05-22


%% Assign input parameters

% Fuselage structural mass [kg] 
C2.fuselage.weights.m = M.airframeStruct.fuselage;  

% Battery mass incl cooling [kg]
C2.battery.weights.m = M.bat + M.propGroup.coolingBatteries;           

% Wing structural + controls mass [kg]
C2.wing.weights.m = M.airframeStruct.wing + M.airframeStruct.surfaceControls; 

 % HT structural mass [kg] 
C2.tail.HT.weights.m = M.airframeStruct.HT;        

 % VT structural mass [kg] 
C2.tail.VT.weights.m = M.airframeStruct.VT;       

% Primary: Mass of each propeller [kg] (scalars or [1 x N/2] arrays)
C2.propulsionGroup1.weights.m_per_prop = M.propGroup.propellers1/p.N1; 

% Primary: Mass of each nacelle [kg] (scalars or [1 x N/2] arrays)
C2.propulsionGroup1.weights.m_per_nac = M.airframeStruct.nacelles1/p.N1;

% Mass of each primary motor including attached components [kg]. 
% (scalars or [1 x N/2] arrays)
C2.propulsionGroup1.weights.m_per_mot = ...             
    (M.propGroup.GT + M.propGroup.gearbox + M.propGroup.oilSystem + ...
     M.propGroup.accessories + M.propGroup.EM1 + M.propGroup.coolingEM1)/p.N1;     
 
% Secondary: Mass of each propeller [kg] (scalars or [1 x N/2] arrays) 
C2.propulsionGroup2.weights.m_per_prop = M.propGroup.propellers2/p.N2; 

% Secondary: Mass of each nacelle [kg] (scalars or [1 x N/2] arrays)
C2.propulsionGroup2.weights.m_per_nac = M.airframeStruct.nacelles2/p.N2;

% Mass of each EM2 including cooling [kg]. (scalars or [1 x N/2] arrays)
C2.propulsionGroup2.weights.m_per_mot = ...            
    (M.propGroup.EM2 + M.propGroup.coolingEM2)/p.N2;         
     
% Fuel & fuel system mass [kg]
C2.fuelSystem.weights.m = M.f + M.propGroup.fuelSystem; 

% Services & equipment mass [kg]
C2.servAndEquip.weights.m = M.servAndEquipMass;

% Operational items mass [kg]
C2.operItems.weights.m = M.operItemsMass;

% PMAD system, incl cooling [kg]
C2.PMAD.weights.m = M.propGroup.PMAD + M.propGroup.coolingPMAD;

% Landing gear [kg]
C2.NLG.weights.m = M.airframeStruct.NLG;
C2.MLG.weights.m = M.airframeStruct.MLG;

% Wing reference area
C2.wing.geom.S = aircraft.Sw;

% Electromotor diameter [m] and length [m]. Rough estimate! More refined
% methods required. Only applied to electromotors of secondary powertrain
% for now, while GT size is specified in input.
C2.propulsionGroup2.geom.D_mot = f.l_EM(aircraft.Pdes.EM2M/p.N2);
C2.propulsionGroup2.geom.l_mot = f.l_EM(aircraft.Pdes.EM2M/p.N2);

                                                    
%% Perform geometrical sizing 

% Dont show plots in convergence loop
C2_in = C2;
C2_in.settings.plotAircraft = 0;

% Call function
aircraft = ConfigurationAndGeometry_v2(p,C2_in,aircraft);

% Calcuate errors w.r.t. previous iteration for C2 convergence loop.
% Take-off mass
errC2_TOM = (M.TOM - C2.settings.TOM_0)/C2.settings.TOM_0;

% Axial CG location
errC2_xCG = (aircraft.weights.coords(1)/aircraft.comps.fuselageGroup.comps.fuselage.geom.l_fus - ...
                C2.settings.xCG_over_lFus_0)/C2.settings.xCG_over_lFus_0;
            
% Tail reference areas
if C2.tail.HT.geom.S == 0 && aircraft.comps.tailGroup.comps.HT.geom.S == 0
    errC2_sHT = 0;
else
    errC2_sHT = (aircraft.comps.tailGroup.comps.HT.geom.S - C2.tail.HT.geom.S)/C2.tail.HT.geom.S;
end
if C2.tail.VT.geom.S == 0 && aircraft.comps.tailGroup.comps.VT.geom.S == 0
    errC2_sVT = 0;
else
    errC2_sVT = (aircraft.comps.tailGroup.comps.VT.geom.S - C2.tail.VT.geom.S)/C2.tail.VT.geom.S;
end


%% Extract output for next iteration in C2 convergence loop

% Parameters required for Class-II weight estimation
% Tail reference areas [m2] 
C2.tail.HT.geom.S = aircraft.comps.tailGroup.comps.HT.geom.S;
C2.tail.VT.geom.S = aircraft.comps.tailGroup.comps.VT.geom.S;

% Fuselage wetted area [m2]
C2.fuselage.geom.S_fus = aircraft.comps.fuselageGroup.comps.fuselage.geom.S_wet;       

% Fuselage max diameter [m]
C2.fuselage.geom.D_fus = aircraft.comps.fuselageGroup.comps.fuselage.geom.D_fus;            

% Distance between wing 25% root chord and HT 25% root chord [m]. If HT is
% installed above or below the fuselage, it is assumed to be installed on
% the VT. In that case use VT as reference
if abs(C2.tail.zHT_over_DFus) > 0.5
    C2.fuselage.geom.l_tailArm = (aircraft.comps.tailGroup.RefSystem(1) + ....
                                  aircraft.comps.tailGroup.comps.VT.geom.root.x_c025 - ...
                                  aircraft.comps.tailGroup.comps.VT.geom.root.c*C2.tail.xHT_over_cVT) - ...
                                 (aircraft.comps.wingGroup.RefSystem(1) + ...
                                  aircraft.comps.wingGroup.comps.wing.geom.root.x_c025);
else
	C2.fuselage.geom.l_tailArm = (aircraft.comps.tailGroup.RefSystem(1) + ....
                                  aircraft.comps.tailGroup.comps.HT.geom.root.x_c025) - ...
                                 (aircraft.comps.wingGroup.RefSystem(1) + ...
                                  aircraft.comps.wingGroup.comps.wing.geom.root.x_c025);
end

% Cabin length and diameter [m]
C2.fuselage.geom.l_cabin = aircraft.comps.fuselageGroup.comps.fuselage.geom.l_cabin;   
C2.fuselage.geom.D_cabin = aircraft.comps.fuselageGroup.comps.fuselage.geom.D_cabin;   

% Update initial guesses on TOM and CG position
C2.settings.TOM_0 = M.TOM;
C2.settings.xCG_over_lFus_0 = aircraft.weights.coords(1)/...
                              aircraft.comps.fuselageGroup.comps.fuselage.geom.l_fus;

% Update initial guesses for MA in next C2 iteration
MA_in.OEM = M.OEM;   
if electric == 1
    MA_in.FF_tot0 = MA.BF_tot;
    MA_in.FF_miss0 = MA.BF_miss;
else
    MA_in.FF_tot0 = MA.FF_tot;
    MA_in.FF_miss0 = MA.FF_miss;
end
MA_in.DOH_tot0 = MA.DOH_tot;            
MA_in.DOH_miss0 = MA.DOH_miss;          




