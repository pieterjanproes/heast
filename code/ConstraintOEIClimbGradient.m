function [WS1,WS2,WP_path1,WP_comp1,WP_loss1,WP_path2,WP_comp2,WP_loss2,TW,a,m,p] = ConstraintOEIClimbGradient(a,m,p,f,s,c,con)
%% Operating conditions

% No horizontal acceleration
dvdt = 0;  

% Given climb gradient
climb = [NaN m.(con).G];       

% No bank angle/load factor req./turn radius
maneuver = [0 NaN NaN];

% Ambient conditions
[T_inf,aa,~,m.(con).rho] = atmosisa(m.(con).h);


%% Compute power loading

% Initialize variables
WS = linspace(s.WSmin,s.WSmax,s.n);
TW = NaN(size(WS));
WP2 = NaN(size(WS));
a.(con).CL = NaN(size(WS));
a.(con).dCL = NaN(size(WS));
a.(con).dCDi = NaN(size(WS));
p.(con).Tc = NaN(size(WS));

% Loop over WS values
for i = 1:length(WS)
    
    % Wing loading in flight condition
    WS_in = WS(i)*m.(con).f;
    
    % Initial guess for flight speed
    m.(con).vr = m.(con).vMargin*(WS_in*2/m.(con).rho/a.(con).CLmax)^0.5;
    
    % Initial guess for dynamic pressure
    q = 0.5*m.(con).rho*m.(con).vr^2;
         
    % Initial guess to speed up convergence
    TW_in = q*a.(con).CD0./WS_in + WS_in/pi/a.AR/a.(con).e/q;

    % Compute power loading and flight speed
    [WP_out,TW_out,CL,dCL,dCDi,dCD0,v,Tc,chi,etap,etap_conv,detap] = ...
        ComputeThrustLoading_vMargin(con,TW_in,WS_in,climb,...
                                     maneuver,dvdt,a,m,p,f,s,c);
    
    % Correct to MTOW
    TW(i) = TW_out*m.(con).f;
    WP2(i) = WP_out/m.(con).f;
    
    % Save aerodynamic variables
    a.(con).CL(i) = CL;
    a.(con).dCL(i) = dCL;
    a.(con).dCDi(i) = dCDi;
    a.(con).dCD0(i) = dCD0;
    p.(con).Tc(i) = Tc;
    m.(con).v(i) = v;
    m.(con).M(i) = v/aa;
    m.(con).Re(i) = v*m.(con).rho*a.c_ref/f.mu(T_inf);
    p.(con).etap(i) = etap;
    p.(con).detap(i) = detap;
    p.(con).chi(i) = chi;
    p.(con).etap_conv(i) = etap_conv;
end


%% Compute HEP component power loading: OEI in secondary powertrain

% Assign same required power for both cases
WP1 = WP2;

% Secondary powertrain failure
if p.N2 ~= 1
    OEI = 2;
else
    OEI = 0;
end

% Replace NaNs with Inf's so that the code understands that WP is an input
indices = isnan(WP2);
WP2(indices) = Inf;

% Evaluate powertrain model
[WP_path2,WP_comp2,WP_loss2] = SizePowertrain(WP2,con,OEI,m,p,f,s,c);

% Switch -Infs
pathnames = fieldnames(WP_path2);
for i = 1:size(pathnames,1)
    WP_path2.(pathnames{i})(WP_path2.(pathnames{i})==-Inf) = Inf;
    WP_path2.(pathnames{i})(indices) = NaN;
end
compnames = fieldnames(WP_comp2);
for i = 1:size(compnames,1)
    WP_loss2.(compnames{i})(WP_loss2.(compnames{i})==-Inf) = Inf;
    WP_comp2.(compnames{i})(WP_comp2.(compnames{i})==-Inf) = Inf;
    WP_loss2.(compnames{i})(indices) = NaN;
    WP_comp2.(compnames{i})(indices) = NaN;
end

% Primary powertrain failure
if p.N1 ~= 1
    OEI = 1;
else
    OEI = 0;
end

% Replace NaNs with Inf's so that the code understands that WP is an input
indices = isnan(WP1);
WP1(indices) = Inf;

% Evaluate powertrain model
[WP_path1,WP_comp1,WP_loss1] = SizePowertrain(WP1,con,OEI,m,p,f,s,c);

% Switch -Infs
pathnames = fieldnames(WP_path1);
for i = 1:size(pathnames,1)
    WP_path1.(pathnames{i})(WP_path1.(pathnames{i})==-Inf) = Inf;
    WP_path1.(pathnames{i})(indices) = NaN;
end
compnames = fieldnames(WP_comp2);
for i = 1:size(compnames,1)
    WP_loss1.(compnames{i})(WP_loss1.(compnames{i})==-Inf) = Inf;
    WP_comp1.(compnames{i})(WP_comp1.(compnames{i})==-Inf) = Inf;
    WP_loss1.(compnames{i})(indices) = NaN;
    WP_comp1.(compnames{i})(indices) = NaN;
end

% Duplicate wing loading output
WS1 = WS;
WS2 = WS;




