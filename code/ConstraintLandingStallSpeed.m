function [WS,WP_path,WP_comp,WP_loss,TW,a,m,p] = ConstraintLandingStallSpeed(a,m,p,f,s,c,con)
%% Landing operating conditions

% Atmospheric conditions
[T_inf,aa,~,m.(con).rho] = atmosisa(m.(con).h);

% Velocity/dynamic pressure
m.(con).v = m.(con).vs;
m.(con).M = m.(con).v/aa;
m.(con).Re = m.(con).v*m.(con).rho*a.c_ref/f.mu(T_inf);
q = 0.5*m.(con).rho*m.(con).v^2;  
           
% No horizontal acceleration
dvdt = 0;               

% No climb rate, no climb gradient
climb = [0 NaN];        

% No bank angle/load factor req./turn radius
maneuver = [0 NaN NaN]; 


%% Start loop to compute power required for landing speed with DP enabled

% Initialize variables
WS = linspace(s.WSmin,s.WSmax,s.n);
TW = NaN(size(WS));
WP = NaN(size(WS));
a.(con).CL = NaN(size(WS));
a.(con).dCL = NaN(size(WS));
a.(con).dCDi = NaN(size(WS));
p.(con).Tc = NaN(size(WS));

% Loop over wing-loading range
for i = 1:length(WS)
    
    % Wing loading in flight condition
    WS_in = WS(i)*m.(con).f;
    
    % Initial guess to speed up convergence
    TW_in = q*a.(con).CD0./WS_in + WS_in/pi/a.AR/a.(con).e/q;
    
    % Compute thrust and power loading
    [WP_out,TW_out,CL,dCL,dCDi,dCD0,Tc,chi,etap,etap_conv,detap] = ComputeThrustLoading_vKnown(...
                        con,TW_in,WS_in,climb,maneuver,dvdt,a,m,p,f,s,c);

    % Correct to MTOW
    TW(i) = TW_out*m.(con).f;
    WP(i) = WP_out/m.(con).f;
       
    % If the wing loading is unrealistically high and it is starting to
    % diverge, stop
    if TW(i) > 10e2 
        TW(i) = NaN;
        WP(i) = NaN;
        break
    end
    
    % Save aerodynamic variables
    a.(con).CL(i) = CL;
    a.(con).dCL(i) = dCL;
    a.(con).dCDi(i) = dCDi;
    a.(con).dCD0(i) = dCD0;
    p.(con).Tc(i) = Tc;
    p.(con).chi(i) = chi;
    p.(con).etap(i) = etap;
    p.(con).detap(i) = detap;
    p.(con).etap_conv(i) = etap_conv;
end

% For unrealistically high WS values the solution will not converge,
% remove indexes where any of the results has NaN value
% NaNarray = sum([TW; WP; a.(con).CL; a.(con).dCL; a.(con).dCDi; p.(con).Tc],1); 
NaNarray = sum([TW; WP; a.(con).CL; a.(con).dCL; a.(con).dCDi],1);% Tc might be NaN if N = 0 or b_dp = 0
TW = TW(~isnan(NaNarray));
WP = WP(~isnan(NaNarray));
WS = WS(~isnan(NaNarray));
a.(con).CL = a.(con).CL(~isnan(NaNarray));
a.(con).dCL = a.(con).dCL(~isnan(NaNarray));
a.(con).dCDi = a.(con).dCDi(~isnan(NaNarray));
a.(con).dCD0 = a.(con).dCD0(~isnan(NaNarray));
p.(con).Tc = p.(con).Tc(~isnan(NaNarray));
p.(con).chi = p.(con).chi(~isnan(NaNarray));
p.(con).etap = p.(con).etap(~isnan(NaNarray));
p.(con).etap_conv = p.(con).etap_conv(~isnan(NaNarray));
p.(con).detap = p.(con).detap(~isnan(NaNarray));

% Interpolate at isolated wing CLmax value to obtain max wing loading.
WS = interp1(a.(con).CL,WS,a.(con).CLmax,'linear');
TW = interp1(a.(con).CL,TW,a.(con).CLmax,'linear');
WP = interp1(a.(con).CL,WP,a.(con).CLmax,'linear');
a.(con).dCL = interp1(a.(con).CL,a.(con).dCL,a.(con).CLmax,'linear');
a.(con).dCDi = interp1(a.(con).CL,a.(con).dCDi,a.(con).CLmax,'linear');
a.(con).dCD0 = interp1(a.(con).CL,a.(con).dCD0,a.(con).CLmax,'linear');
p.(con).Tc = interp1(a.(con).CL,p.(con).Tc,a.(con).CLmax,'linear');
p.(con).chi = interp1(a.(con).CL,p.(con).chi,a.(con).CLmax,'linear');
p.(con).etap = interp1(a.(con).CL,p.(con).etap,a.(con).CLmax,'linear');
p.(con).detap = interp1(a.(con).CL,p.(con).detap,a.(con).CLmax,'linear');
p.(con).etap_conv = interp1(a.(con).CL,p.(con).etap_conv,a.(con).CLmax,'linear');
a.(con).CL = a.(con).CLmax;


%%  Compute HEP component power loading

% All engines operative
OEI = 0;

% Replace NaNs with Inf's so that the code understands that WP is an input,
% but save indices to revert later
indices = isnan(WP);
WP(indices) = Inf;

% Call sizing function
[WP_path,WP_comp,WP_loss] = SizePowertrain(WP,'L',OEI,m,p,f,s,c);

% Change -Inf to +Inf to avoid warnings; Inf is a possible solution for
% zero power flow. Also revert back to NaN if necessary
pathnames = fieldnames(WP_path);
for i = 1:size(pathnames,1)
    WP_path.(pathnames{i})(WP_path.(pathnames{i})==-Inf) = Inf;
    WP_path.(pathnames{i})(indices) = NaN;
end
compnames = fieldnames(WP_comp);
for i = 1:size(compnames,1)
    WP_loss.(compnames{i})(WP_loss.(compnames{i})==-Inf) = Inf;
    WP_comp.(compnames{i})(WP_comp.(compnames{i})==-Inf) = Inf;
    WP_loss.(compnames{i})(indices) = NaN;
    WP_comp.(compnames{i})(indices) = NaN;
end







