function [WP,TW,CL,dCL,dCDi,dCD0,v,Tc,chi1,etap,etap_conv,detap] = ComputeThrustLoading_vMargin(con,TW_in,WS,climb,maneuver,dvdt,a,m,p,f,s,c)
% This function computes the thrust (and power) loading for a given wing
% loading and flight conditions. It calculates useful (NOT shaft) power. 
% Powers and thrusts are NOT corrected to MTOW, SL or max throttle
% settings. 
%
% It differs from the ComputeThrustLoading.m function in the sense that the
% actual flight speed is not given. Instead a stall speed margin and
% maximum lift coefficient are given. Since the stall speed depends on both
% the isolated wing maximum lift coefficient and on the thrust setting, a
% convergence loop is required on both CL,TW and CL_stall,TW_stall, the
% last to required in order to obtain the stall speed, and therewith, the
% actual flight speed (v = vMargin*vs).
%
% Input:
%   - Flight condition being evaluated. String which is used as name for
%       the field within the structure variables (e.g. "TO", "cr")
%   - TW_in: initial guess for thrust loading (to speed up convergence)
%   - WS: wing loading [N/m2]
%   - climb: array of two elements, (1) climb rate [m/s] and (2) climb
%       gradient [-]. Only one can be specified, the other has to be NaN.
%       If both are NaN, zero climb is assumed (c = ROC = 0). If both have
%       values, an error is returned.
%   - maneuver: array of three elements, (1) bank angle [deg], (2) load
%       factor [-] and (3) turn radius [m]. Only one can be specified, the 
%       other two have to be NaN. If all are NaN, zero bank angle is 
%       assumed (mu = 0, n = 1, r = Inf). If two or more are numbers, an
%       error is returned.
%   - dvdt: horizontal acceleration of aircraft [m/s2]
%   - a,m,p,s,f,c: structures containing aicraft parameters, mission
%       parameters, powertrain parameters, programme settings, anonymous
%       functions, and constants, respectively. See input of
%       WP_WS_diagram_DP for more info.
%
% Output:
%   - WP: required flight power loading [N/W]
%   - TW: required flight thrust loading [-]
%   - CL: isolated wing lift coefficient [-]
%   - dCL: wing lift coefficient increase due to DP [-]
%   - dCDi: wing (thrust-induced) drag coefficient increase due to DP [-]
%   - dCD0: wing parasite drag coefficient increase due to DP [-]
%   - v: velocity at which the maneuver is performed [m/s]
%   - Tc: propulsors' thrust coefficient [-]
%   - chi: thrust share of DP propuslors [-]
%   - etap: propulsive efficiency of isolated DP propulsors [-]
%   - etap_conv: propulsive eff. of isolated non-DP propulsors [-]
%   - detap: change in propulsive efficiency due to DP [-]
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 23-08-17
%%% Last modified: 09-09-20


%% Input 

% Retrieve input from structures
CD0 = a.(con).CD0; AR = a.AR; e = a.(con).e; 
CLmax = a.(con).CLmax; CL_minD = a.(con).CL_minD;
rho = m.(con).rho; vMargin = m.(con).vMargin; h = m.(con).h;
Gamma = p.(con).Gamma;  b_dp = p.geom.b_dp; 
dy = p.geom.dy; N = p.N; b_conv = p.geom.b_conv; N_conv = p.N_conv;
itermax = s.itermax; errmax = s.errmax; rf = s.rf; g = c.g;

% Initial values for propulsive efficiency and thrust share (these will be 
% the values that are ued if ComputeEtap = 0). 
if p.DP == 1
    etap = p.(con).etap1;
    etap_conv = p.(con).etap2;
elseif p.DP == 2
    etap = p.(con).etap2;
    etap_conv = p.(con).etap1;
elseif p.DP == 0
    if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
       strcmp(p.config,'e-1') 
        etap = p.(con).etap2; 
        etap_conv = p.(con).etap1;
    elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
       strcmp(p.config,'e-2') 
        etap = p.(con).etap1; 
        etap_conv = p.(con).etap2;
    else
        etap = p.(con).etap2; 
        etap_conv = p.(con).etap1;
    end
end
chi0 = p.(con).chi(end);
detap = 0;

% Use same initial values for stall conditions
etaps = etap;
detaps = detap;
chi0s = chi0;

% Disk loading [m2/N]
D2W = f.D2W(WS,AR,p);
D2W_conv = f.D2W_conv(WS,AR,p);

% Compute prop radius/wing chord ratio of DP system
Rc = 0.5*(D2W*WS*AR)^0.5;

% Check input values for climb rate/gradient
if length(climb)~=2
    error(['The inpute array for "climb" has to have two elements,'...
           'one of which has to be NaN. Specify either climb rate '...
           'or climb gradient.'])
else
    % If both elements are NaNs
    if sum(isnan(climb))==2
        c = 0;
        
    % If only one element is specified    
    elseif sum(isnan(climb))==1
        
        % If ROC is specified
        if ~isnan(climb(1))
            computec = 1;
            c = climb(1);
            cs = climb(1);
            
        % If climb gradient is specified
        elseif ~isnan(climb(2))
            computec = 2;
            G = climb(2);
        end
    
    % If both elements have values
    else
        error(['The inpute array for "climb" has to have two elements,'...
               'one of which has to be NaN. Specify either climb rate '...
               'or climb gradient.'])
    end
end

% Check input values for maneuvers
if length(maneuver)~=3
    error(['The inpute array for "maneuver" has to have three elements,'...
           'two of which have to be NaN. Specify either bank angle, '...
           'load factor or turn radius.'])
else
    % If all elements are NaNs
    if sum(isnan(maneuver))==3
        mu = 0;
    
    % If only one element is specified    
    elseif sum(isnan(maneuver))==2
        
        % If bank angle is specified
        if ~isnan(maneuver(1))
            computeMu = 1;
            mu = maneuver(1);
            
        % If load factor is specified
        elseif ~isnan(maneuver(2))
            computeMu = 2;
            n = maneuver(2);
            
        % If turn radius is specified
        elseif ~isnan(maneuver(3))
            computeMu = 3;
            r = maneuver(3);
        end
    
    % If two or more elements have values
    else
        error(['The inpute array for "maneuver" has to have three '...
               'elements, one of which has to be NaN. Specify either '...
               'bank angle, load factor or turn radius.'])
    end
end


%% Compute equilibrium flight point

% Loop parameters
err = 1;                                           
iter = 0;                            

% Initial values in flight conditions
CL0 = CLmax/vMargin^2;
TW0 = TW_in;                    

% Initial values in stall conditions
CL0s = CLmax;
TW0s = TW_in;

% Start convergence loop on TW, CL
while err > errmax
    iter = iter+1;
    
    % Estimate reference stall speed
    vs = (WS*2/rho/CL0s).^0.5;
    [T_inf,aa,~,~] = atmosisa(h);
    Ms = vs/aa;
    qs = 0.5*rho*vs^2;
    mu_inf = f.mu(T_inf);
    Res = vs*rho*a.c_ref/mu_inf;
    
    % Compute climb rate at stall speed if necessary
    if computec == 2
       cs = G*vs; 
    end
    
    % Compute thrust coefficients, defined as Tc = T/(rho*v^2*D^2)
    if TW0==Inf
        Tcs = chi0s*1e9;
        Tcs_conv = (1-chi0s)*1e9;
    else
        Tcs = chi0s*TW0s/N/rho/vs^2/D2W;
        Tcs_conv = (1-chi0s)*TW0s/N_conv/rho/vs^2/D2W_conv;        
    end
    
    % Update propulsive efficiency and thrust share
    if s.ComputeEtap == 1
        
        % Compute propulsive efficiency based on assumed thrust coefficient 
        etaps = f.etap(Tcs);
        etap_convs = f.etap(Tcs_conv);
        
        % For architectures with two propulsion systems, re-compute
        % thrust share accordingly. Note that if no DP is used, but
        % eta_p has to be computed, that etap_conv is assumed to be on
        % the primary branch
        if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
                strcmp(p.config,'dual-e')
            if p.DP == 1
                chi1s = 1/( m.(con).Phi / (1-m.(con).Phi) ...
                    * etap_convs/(etaps+detaps) + 1);
            else
                chi1s = 1/((1-m.(con).Phi)/ m.(con).Phi   ...
                    * etap_convs/(etaps+detaps) + 1);
            end
        else
            chi1s = p.(con).chi(end);
        end
        
    % If eta_p does not have to be computed, use the thrust share
    % previously computed in CheckInput.m    
    else
        chi1s = p.(con).chi(end);
    end
     
    % Update delta terms if DP is used
    %     if p.DP == 1 || p.DP == 2
    oper.e = e;
    oper.CD0 = CD0;
    oper.Gamma = Gamma;
    oper.CL = CLmax;
    oper.M = Ms;
    oper.etap = etaps;
    oper.Tc = Tcs;
    oper.Tc_conv = Tcs_conv;
    oper.Rc = Rc;
    oper.Re = Res;
    [dCLs,dCD0s,dCDis,detaps] = WingPropDeltas_v5(oper,a,p,f,s,c,0);
    %     else
    %         dCLs = 0;
    %         dCD0s = 0;
    %         dCDis = 0;
    %         detaps = 0;
    %     end
                                         
    % Compute updated thrust loading in stall conditions
    TW1s = 1/(1-chi1s+chi1s*cosd(Gamma))*(cs/vs + dvdt/g + qs/WS*...
                (f.CD(CD0,CLmax,CL_minD,AR,e) + dCD0s + dCDis));
    
    % Update total CLmax estimation in stall conditions, excluding TV (?)
    CL1s = CLmax + dCLs;
    
    % Actual speed during maneuver
    v = vMargin*vs;
    M = v/aa;
    q = 0.5*rho*v^2;
    Re = v*rho*a.c_ref/mu_inf;
    
    % Compute bank angle if necessary
    if computeMu == 2
        mu = rad2deg(acos((1-(c/v)^2)^0.5 / (n + T*TW0*sind(Gamma))));
    elseif computeMu == 3
        mu = rad2deg(asin(v^2/r/g/...
            (q/WS*(CL0 + dCL) + T*TW0*sind(Gamma))));
    end
    
    % Compute climb rate if necessary
    if computec == 2
       c = G*v; 
    end
   
    % Recompute thrust coefficients in actual flight conditions
    if TW0==Inf
        Tc = chi0*1e9;
        Tc_conv = (1-chi0)*1e9;
    else
        Tc = chi0*TW0/N/rho/v^2/D2W;
        Tc_conv = (1-chi0)*TW0/N_conv/rho/v^2/D2W_conv;
    end
    
    % Recompute propulsive efficiency and thrust share in actual flight
    % conditions
    if s.ComputeEtap == 1
        
        % Compute propulsive efficiency based on assumed thrust coefficient 
        etap = f.etap(Tc);
        etap_conv = f.etap(Tc_conv);
        
        % For architectures with two propulsion systems, re-compute
        % thrust share accordingly. Note that if no DP is used, but
        % eta_p has to be computed, that etap_conv is assumed to be on
        % the primary branch
        if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
                strcmp(p.config,'dual-e')
            if p.DP == 1
                chi1 = 1/( m.(con).Phi / (1-m.(con).Phi) ...
                    * etap_conv/(etap+detap) + 1);
            else
                chi1 = 1/((1-m.(con).Phi)/ m.(con).Phi   ...
                    * etap_conv/(etap+detap) + 1);
            end
        else
            chi1 = p.(con).chi(end);
        end
        
    % If eta_p does not have to be computed, use the thrust share
    % previously computed in CheckInput.m    
    else
        chi1 = p.(con).chi(end);
    end
    
    % Update delta terms if DP is used
    %     if p.DP == 1 || p.DP == 2
    oper.e = e;
    oper.CD0 = CD0;
    oper.Gamma = Gamma;
    oper.CL = CL0;
    oper.M = M;
    oper.etap = etap;
    oper.Tc = Tc;
    oper.Rc = Rc;
    oper.Tc_conv = Tc_conv;
    oper.Re = Re;
    [dCL,dCD0,dCDi,detap] = WingPropDeltas_v5(oper,a,p,f,s,c,0);
    %     else
    %         dCL = 0;
    %         dCD0 = 0;
    %         dCDi = 0;
    %         detap = 0;
    %     end
    
	% Isolated wing lift coefficient in actual CL and V conditions. If c>v,
	% (e.g. as W/S -> 0), this will give a complex number. In that case,
	% set output to NaN. Note that loop will continue until itermax has
	% been reached
    if c/v > 1
        CL1 = NaN;
    else
        CL1 = WS/q*(1/cosd(mu)*(1-(c/v)^2)^0.5 - chi1*TW0*sind(Gamma)) - dCL;
    end
    
    % Compute updated thrust loading in actual CL and V conditions
    TW1 = 1/(1-chi1+chi1*cosd(Gamma))*(c/v + dvdt/g + q/WS*...
                (f.CD(CD0,CL1,CL_minD,AR,e) + dCD0 + dCDi));
    
    % Update total wing lift coefficient in actual CL and V conditions
    CL_tot = CL1 + dCL;
    
    % Unrealistic input values the model may return negative,
    %   and afterwards complex, values, which will cause
    %   erroneous solutions. In this case provide NaN as answer
    if CL_tot < 0                                   
        CL1 = NaN;                               
    end                                             
    
    % Calculate error
    err1 = abs(TW1-TW0)/abs(TW0);
    err2 = abs(CL1-CL0)/abs(CL0);
    err3 = abs(TW1s-TW0s)/abs(TW0s);
    err4 = abs(CL1s-CL0s)/abs(CL0s);
    if chi1s==0 && chi0s==0
        err5 = 0;
    else
        err5 = abs(chi1s-chi0s)/abs(chi0s);
    end
    if chi1==0 && chi0==0
        err6 = 0;
    else
        err6 = abs(chi1-chi0)/abs(chi0);
    end
    err = err1 + err2 + err3 + err4 + err5 + err6;
            
    % If the error becomes a NaN anyway, the code
    % will crash or provide an unconverged solution. Note that err will
    % never be complex because of the abs function
    if isnan(err)
%         warning('Convergence error attained NaN value')
        TW1 = NaN;
        CL1 = NaN;
        break
    end
    
    % Update values, add small offset to avoid reiterating NaN values. Use
    % relaxation factor to avoid divergence
    if isnan(TW1) || isnan(CL1) || isnan(TW1s) || isnan(CL1s)
        TW0 = 1.1*TW_in;
        CL0 = 1.1*WS/q;
        TW0s = 1.1*TW_in;
        CL0s = 1.1*WS/q;
        chi0s = chi1s;
        chi0 = chi1;
    else
        TW0 = TW0+rf*(TW1-TW0);
        CL0 = CL0+rf*(CL1-CL0);
        TW0s = TW0s+rf*(TW1s-TW0s);
        CL0s = CL0s+rf*(CL1s-CL0s);
        chi0s = chi1s;
        chi0 = chi1;
    end
    
    % Limit number of iterations
    if iter >= itermax
        TW1 = NaN;
        CL1 = NaN;
        break
    end
    
end

% Compute required (flight, not shaft) power loading
WP = 1/TW1/v;

% Assign output
TW = TW1;
CL = CL1;


