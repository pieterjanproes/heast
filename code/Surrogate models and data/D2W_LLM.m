function D2W = D2W_LLM(WS,AR,p)
% This function computes the weight-oriented disk loading for the
% geometrical parametrization used in the LLM surrogate model. The D2W is
% computed for the DP array.

 % Constants used in the LLM
 Rfus = 0.05;            
 dy_dp = 0.05;           
 dy_main = 0.025;        

% Ratio between single main-prop diameter and wing span (N.B. model only
% works for two main props, even though a generic N_prop is used here)
D_main = p.geom.b_conv/p.N_conv;

% K-factor
K = 0.5 - Rfus - dy_main - D_main;
        
% Ratio between DP-space-used and available DP space, b_used/b_free
dY = p.geom.b_dp*(0.5 - (0.5 + dy_dp)/p.N/(1 + dy_dp))/ ...
    (K - p.geom.b_dp/p.N*dy_dp/(1+dy_dp)) + p.geom.dy_tip;
      
% Disk loading parameter
D2W = AR/WS*(K*(dY - p.geom.dy_tip) / ...
    ((p.N/2) - 0.5 +dy_dp*((p.N/2) - 1 + dY - p.geom.dy_tip)))^2;