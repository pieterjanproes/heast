function [s] = PlotMissionAnalysis(p,m,s,c,aircraft,M,MA,WSdes,WPdes,WS,WP_path)
%% Settings

% Select only mission segments of MA array
names = {'cl','cr','de','Dcl','Dcr','Dde','Loiter'};
Ncon = size(names,2);

% Colors for weight pie chart (Class 1.5)
piecolors = [1 1 0          % Payload
             0 1 0          % OEM'
             0 0.7 0        % Wing
             1 0 0          % Fuel
             0 1 1          % Battery
             0 0.2 1        % GT
             0 0 0.9        % EM1
             0 0 0.6];      % EM2

% Colors for weight pie chart (Class 2)
piecolors2 = [  1 1 0       % Payload (yellow)
                1 0 0       % Fuel (red)
                0 1 1       % Battery (cyan)
                0 1 0       % Airframe struct (green)
                0 0 1       % Propulsion group (blue)
                1 0.55 0    % Services and equipment (orange)
                0.8 0 0.8]; % Operational items (magenta)
                

%% I: Mission parameters

% Generate figure
fig = figure(s.figStart + size(s.figs,2));
fig.Name = 'MA: mission';
s.figs(size(s.figs,2)+1) = fig;
fig.Color = [1 1 1];
set(fig,'defaultAxesColorOrder',[[0 0 0]; [0 0 0]]);

% Altitude (if any component limit has been exceeded, show mission segment 
% in dotted lines) 
subplot(2,2,1)
grid on; hold on; box on
for i = 1:Ncon
    plot(MA.(names{i}).t/60,MA.(names{i}).h,'-b')
    if any(MA.(names{i}).limits==1)==1
        plot(MA.(names{i}).t(MA.(names{i}).limits==1)/60,...
            MA.(names{i}).h(MA.(names{i}).limits==1),'-','color',[1 1 1])
        H = plot(MA.(names{i}).t(MA.(names{i}).limits==1)/60,...
            MA.(names{i}).h(MA.(names{i}).limits==1),'--r');
    end
end
xlabel('Time [min]')
ylabel('Altitude [m]')

% Mach number
subplot(2,2,2)
grid on; hold on; box on
for i = 1:Ncon
    plot(MA.(names{i}).t/60,MA.(names{i}).M,'-b')
    if any(MA.(names{i}).limits==1)==1
        plot(MA.(names{i}).t(MA.(names{i}).limits==1)/60,...
            MA.(names{i}).M(MA.(names{i}).limits==1),'-','color',[1 1 1])
        H = plot(MA.(names{i}).t(MA.(names{i}).limits==1)/60,...
            MA.(names{i}).M(MA.(names{i}).limits==1),'--r');
    end
end
plot([0 max(MA.cl.t)]/60,[m.cr.M m.cr.M],':k')
xlabel('Time [min]')
ylabel('Mach [-]')

% Aircraft mass
subplot(2,2,3)
grid on; hold on; box on
for i = 1:Ncon
    plot(MA.(names{i}).t/60,MA.(names{i}).W/c.g/1000,'-b')
end
xlabel('Time [min]')
ylabel('Aircraft mass [t]')

% Energies remaining (including taxi/TO/L)
subplot(2,2,4)
grid on; hold on; box on
tArray = [];
EfArray = [];
EbatArray = [];
for i = 1:Ncon
    h1 = plot(MA.(names{i}).t/60,MA.(names{i}).Ef/1e9,'-r');
    h2 = plot(MA.(names{i}).t/60,(MA.(names{i}).Ebat)/1e9,'-b');
    tArray = [tArray MA.(names{i}).t/60];
    EfArray = [EfArray MA.(names{i}).Ef];
    EbatArray = [EbatArray MA.(names{i}).Ebat];
end
[tMin,idxMin] = min(tArray);
[tMax,idxMax] = max(tArray);
plot(tMin+[-10 -5 0],(EfArray(idxMin)+[MA.EF.E_f_TakeOff+MA.EF.E_f_TaxiOut...
                                       MA.EF.E_f_TakeOff 0])/1e9,'.:r');
plot(tMax+[+10 +5 0],(EfArray(idxMax)-[MA.EF.E_f_Landing+MA.EF.E_f_TaxiIn...
                                       MA.EF.E_f_Landing 0])/1e9,'.:r');
plot(tMin+[-10 -5 0],(EbatArray(idxMin)+[MA.EF.E_bat_TakeOff+MA.EF.E_bat_TaxiOut...
                                         MA.EF.E_bat_TakeOff 0])/1e9,'.:b');
plot(tMax+[+10 +5 0],(EbatArray(idxMax)-[MA.EF.E_bat_Landing+MA.EF.E_bat_TaxiIn...
                                         MA.EF.E_bat_Landing 0])/1e9,'.:b');
xlabel('Time [min]')
ylabel('Energy remaining [GJ]')

% Add battery SOC on right axis (SOC based on total installed energy, also
% the extra used for power)
Emax = M.bat*p.SE.bat;
yyaxis right
ylabel('Battery SOC [%]')
ylim([0 100])
for i = 1:Ncon
    h3 = plot(MA.(names{i}).t/60,100*MA.(names{i}).Ebat/Emax,'-g');
end
plot(tMin+[-10 -5 0],100*(EbatArray(idxMin)+[MA.EF.E_bat_TakeOff+MA.EF.E_bat_TaxiOut...
                                                    MA.EF.E_bat_TakeOff 0])/Emax,'.:g');
plot(tMax+[+10 +5 0],100*(EbatArray(idxMax)-[MA.EF.E_bat_Landing+MA.EF.E_bat_TaxiIn...
                                                    MA.EF.E_bat_Landing 0])/Emax,'.:g');

% Add division lines of the different segments
for j = 1:4
    for i= 1:Ncon
        subplot(2,2,j)
        ax=gca;
        plot([max(MA.(names{i}).t/60) max(MA.(names{i}).t/60)],...
        ax.YLim,':k')
    end
end

% Add legends
for i = 1:Ncon
    if any(MA.(names{i}).limits==1)==1
        subplot(2,2,1); legend(H,'Warning: P_{required}>P_{installed}')
        subplot(2,2,2); legend(H,'Warning: P_{required}>P_{installed}')
        break
    end
end
subplot(2,2,4); legend([h1 h2 h3],'Fuel','Battery',...
            'Battery SOC','Location','northeast')


%% II: Performance parameters

% Generate figure
fig = figure(s.figStart + size(s.figs,2));
fig.Name = 'MA: performance';
s.figs(size(s.figs,2)+1) = fig;
fig.Color = [1 1 1];

% Climb rate
subplot(2,2,1)
grid on; hold on; box on
for i = 1:Ncon
    plot(MA.(names{i}).t/60,MA.(names{i}).dhdt,'-b')
end
xlabel('Time [min]')
ylabel('Climb rate [m/s]')

% Acceleration
subplot(2,2,2)
grid on; hold on; box on
for i = 1:Ncon
    plot(MA.(names{i}).t/60,MA.(names{i}).dVdt,'-b')
end
xlabel('Time [min]')
ylabel('Acceleration [m/s^2]')

% Lift coefficients
subplot(2,2,3)
grid on; hold on; box on
for i = 1:Ncon
    h1 = plot(MA.(names{i}).t/60,MA.(names{i}).aero.CL,'-b');
    h2 = plot(MA.(names{i}).t/60,MA.(names{i}).aero.CLiso,'--b');
end
xlabel('Time [min]')
ylabel('Lift coefficient [-]')

% Lift-to-drag ratios & efficiency
subplot(2,2,4)
grid on; hold on; box on
for i = 1:Ncon
    h3 = plot(MA.(names{i}).t/60,MA.(names{i}).aero.LD,'-b');
    h4 = plot(MA.(names{i}).t/60,MA.(names{i}).aero.CLiso./...
        MA.(names{i}).aero.CDiso,'--b');
end
xlabel('Time [min]')
ylabel('Lift-to-drag ratio [-]')

% Add division lines of the different segments
for j = 1:4
    for i= 1:Ncon
        subplot(2,2,j)
        ax=gca;
        plot([max(MA.(names{i}).t/60) max(MA.(names{i}).t/60)],...
        ax.YLim,':k')
    end
end

% Legends
subplot(2,2,3); legend([h1,h2],'Total','Airframe only','Location','northeast')
subplot(2,2,4); legend([h3 h4],'Total','Airframe only','Location','northeast')


%% III: Powertrain parameters

% Generate figure
fig = figure(s.figStart + size(s.figs,2));
fig.Name = 'MA: powertrain';
s.figs(size(s.figs,2)+1) = fig;
fig.Color = [1 1 1];

% Power-control parameters
subplot(2,2,1)
grid on; hold on; box on
GTthrottleUsed = 0;
EM1throttleUsed = 0;
EM2throttleUsed = 0;
h11 = [];
for i = 1:Ncon
    
    % Make distinction between throttle referring to EM1/EM2/GT. Note that
    % generally xi_flag is constant throughout a mission segment, though in
    % theory it's possible that phi crosses phi = 1 at one point along the
    % segment, and at that point throttle suddenly refers to something
    % else... so make separate plotting lines
    t_GT = MA.(names{i}).t(MA.(names{i}).P.xi_flag == 1)/60;
    xi_GT = MA.(names{i}).P.xi(MA.(names{i}).P.xi_flag == 1);
    t_EM1 = MA.(names{i}).t(MA.(names{i}).P.xi_flag == 2)/60;
    xi_EM1 = MA.(names{i}).P.xi(MA.(names{i}).P.xi_flag == 2);
    t_EM2 = MA.(names{i}).t(MA.(names{i}).P.xi_flag == 3)/60;
    xi_EM2 = MA.(names{i}).P.xi(MA.(names{i}).P.xi_flag == 3);
    if ~isempty(t_GT)
        if GTthrottleUsed == 0
            h11(1,end+1) = plot(t_GT,xi_GT,'-b');
            h11String{length(h11)} = 'GT throttle';
        else
            plot(t_GT,xi_GT,'-b');
        end
        GTthrottleUsed = 1;
    end
    if ~isempty(t_EM1)
        if EM1throttleUsed == 0
            h11(1,end+1) = plot(t_EM1,xi_EM1,'-.b');
            h11String{length(h11)} = 'EM1 throttle';
        else
            plot(t_EM1,xi_EM1,'-.b');
        end
        EM1throttleUsed = 1;
    end
    if ~isempty(t_EM2)
        if EM2throttleUsed == 0
            h11(1,end+1) = plot(t_EM2,xi_EM2,'--b');
            h11String{length(h11)} = 'EM2 throttle';
        else
            plot(t_EM2,xi_EM2,'--b');
        end
        EM2throttleUsed = 1;
    end
        
    % Add phi, Phi, chi
    h12 = plot(MA.(names{i}).t/60,MA.(names{i}).P.phi,'-r');
    h13 = plot(MA.(names{i}).t/60,MA.(names{i}).P.Phi,'-g');
    h14 = plot(MA.(names{i}).t/60,MA.(names{i}).P.chi,'-m');
end
xlabel('Time [min]')
ylabel('Power-control parameters [-]')

% Propulsive/thermal efficiencies
subplot(2,2,2)
grid on; hold on; box on
for i = 1:Ncon
    if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ...
            || strcmp(p.config,'e-1') 
        etap1plot = MA.(names{i}).aero.etap1;
        etap1isoplot = MA.(names{i}).aero.etap1iso;
        etap2plot = NaN(size(MA.(names{i}).aero.etap2));
        etap2isoplot = NaN(size(MA.(names{i}).aero.etap2iso));
    elseif strcmp(p.config,'turboelectric') || strcmp(p.config,'serial')...
           || strcmp(p.config,'e-2') 
        etap2plot = MA.(names{i}).aero.etap2;
        etap2isoplot = MA.(names{i}).aero.etap2iso;
        etap1plot = NaN(size(MA.(names{i}).aero.etap1));
        etap1isoplot = NaN(size(MA.(names{i}).aero.etap1iso));
    else
        etap1plot = MA.(names{i}).aero.etap1;
        etap1isoplot = MA.(names{i}).aero.etap1iso;
        etap2plot = MA.(names{i}).aero.etap2;
        etap2isoplot = MA.(names{i}).aero.etap2iso;
    end
    h21 = plot(MA.(names{i}).t/60,etap1plot,'-b');
    h22 = plot(MA.(names{i}).t/60,etap1isoplot,'--b');
    h23 = plot(MA.(names{i}).t/60,etap2plot,'-r');
    h24 = plot(MA.(names{i}).t/60,etap2isoplot,'--r');
    h25 = plot(MA.(names{i}).t/60,MA.(names{i}).aero.etapAvg,':g');
    h26 = plot(MA.(names{i}).t/60,MA.(names{i}).P.etagt,'-m');
end
xlabel('Time [min]')
ylabel('Efficiency [-]')


% Fuel and battery power
subplot(2,2,3)
grid on; hold on; box on
for i = 1:Ncon
    h31 = plot(MA.(names{i}).t/60,MA.(names{i}).P.f/1e6,'-r');
    h32 = plot(MA.(names{i}).t/60,MA.(names{i}).P.bat/1e6,'-b');
end
xlabel('Time [min]')
ylabel('Source power [MW]')

% Include C-rate, if architecture can have batteries
if ~strcmp(p.config,'conventional') && ~strcmp(p.config,'turboelectric') ...
                                    && ~strcmp(p.config,'PTE')
    yyaxis right
    ylabel('C-rate [1/h]')
    for i = 1:Ncon
        C_rate = MA.(names{i}).P.bat/(M.bat*p.SE.bat/3600);
        yyaxis right
        h33 = plot(MA.(names{i}).t/60,C_rate,'-g');
    end
    ax = gca; ax.YAxis(1).Color = 'k'; ax.YAxis(2).Color = 'k';
    WP_TO = interp1(WS.TO,WP_path.TO.bat,WSdes.(s.SelDes));
    C_rate_TO = (M.TOM*c.g/WP_TO)/(M.bat*p.SE.bat/3600);
    C_rate_max = p.SP.bat/(p.SE.bat/3600);
    h34 = plot(0,C_rate_TO,'xg');
    h35 = plot(ax.XLim,[C_rate_max C_rate_max],'--g');
end

% Propulsive power breakdown
subplot(2,2,4)
grid on; hold on; box on
for i = 1:Ncon
    h41 = plot(MA.(names{i}).t/60,MA.(names{i}).P.p/1e6,'-r','linewidth',2);
    h42 = plot(MA.(names{i}).t/60,MA.(names{i}).P.drag/1e6,'-b');
    h43 = plot(MA.(names{i}).t/60,MA.(names{i}).P.climb/1e6,'-m');
    h44 = plot(MA.(names{i}).t/60,MA.(names{i}).P.acceleration/1e6,'-g');
    h45 = plot(MA.(names{i}).t/60,MA.(names{i}).P.offtake/1e6,'--c');
    h46 = plot(MA.(names{i}).t/60,MA.(names{i}).P.cooling/1e6,'--','color',[0.3 0.3 0.3]);
end
xlabel('Time [min]')
ylabel('Power [MW]')

% Add division lines of the different segments
for j = 1:4
    for i= 1:Ncon
        subplot(2,2,j)
        ax=gca;
        plot([max(MA.(names{i}).t/60) max(MA.(names{i}).t/60)],...
        ax.YLim,':k')
    end
end

% Add legends
subplot(2,2,1) 
h1String = h11String;
h1String{end+1} = 'Supplied power ratio';
h1String{end+1} = 'Shaft power ratio';
h1String{end+1} = 'DP thrust share';
legend([h11 h12 h13 h14],h1String,'Location','northeast')
subplot(2,2,2)
legend([h21 h22 h23 h24 h25 h26],'\eta_{p1}','\eta_{p1,iso}',...
                     '\eta_{p2}','\eta_{p2,iso}','\eta_{p,avg}','\eta_{gt}')
subplot(2,2,3)                 
if ~strcmp(p.config,'conventional') && ~strcmp(p.config,'turboelectric') ...
                                    && ~strcmp(p.config,'PTE')
    legend([h31 h32 h33 h34 h35],'Fuel','Battery','C-rate','C-rate (TO)',...
            'max C-rate','Location','northeast')
else
    legend([h31 h32],'Fuel','Battery','Location','northeast')
end
subplot(2,2,4)
legend([h41 h42 h43 h44 h45 h46],'Total propulsive','Drag (propulsive)',...
    'Climb (propulsive)','Accelerate (propulsive)','Offtake (source)',...
    'Cooling (source)','Location','northeast')


%% IV: Convergence history
% Note: the DOH plotted in the convergence history is the one used in the
% MA, which assumes zero remaining energy. If the batteries are
% posteriorly sized by power, then this value will not coincide with the
% final DOH result, since there will be energy left in the batteries after
% the mission.

% Deactivated
%{
% Generate figure
fig = figure(s.figStart + size(s.figs,2));
fig.Name = 'MA: convergence';
s.figs(size(s.figs,2)+1) = fig;
fig.Color = [1 1 1];

% Fuel fraction convergence
subplot(2,2,1)
hold on; grid on; box on
plot(0:length(MA.conv.FF)-1,MA.conv.FF,'.k')
xlabel('Iteration number [-]')
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') ||...
        strcmp(p.config,'dual-e')
    ylabel('Total battery mass fraction [-]')
else
    ylabel('Total fuel mass fraction [-]')
end

% DOH convergence
subplot(2,2,2)
hold on; grid on; box on
plot(0:length(MA.conv.DOH)-1,MA.conv.DOH,'.k')
xlabel('Iteration number [-]')
ylabel('Total degree of hybridization [-]')

% TOM convergence
subplot(2,2,3)
hold on; grid on; box on
plot(0:length(MA.conv.TOM)-1,MA.conv.TOM/1000,'.k')
xlabel('Iteration number [-]')
ylabel('Take-off mass [t]')

% Convergence error
subplot(2,2,4)
hold on; grid on; box on
plot(0:length(MA.conv.err)-1,MA.conv.err,'.k')
xlabel('Iteration number [-]')
ylabel('Error [-]')
%}


%% V: Weight breakdown

% Generate figure
fig = figure(s.figStart + size(s.figs,2));
fig.Name = 'Weight breakdown';
s.figs(size(s.figs,2)+1) = fig;
fig.Color = [1 1 1];

% Original Class 1.5 breakdown
if s.WeightEstimationClass == 1
    
    % Prepare divisions and labels of pie chart
    X = [M.PL M.OEM M.w M.f M.bat M.GT M.EM1 M.EM2]/1000;
    Y = 100*X/M.TOM*1000;
    Z = {['    Payload\newline' num2str(X(1),'%.2f') ' t (' num2str(Y(1),'%.1f') '%)'],...
        ['       OEM''\newline' num2str(X(2),'%.2f') ' t (' num2str(Y(2),'%.1f') '%)'],...
        ['     Wing\newline' num2str(X(3),'%.2f') ' t (' num2str(Y(3),'%.1f') '%)'],...
        ['     Fuel\newline' num2str(X(4),'%.2f') ' t (' num2str(Y(4),'%.1f') '%)'],...
        ['  Batteries \newline' num2str(X(5),'%.2f') ' t (' num2str(Y(5),'%.1f') '%)'],...
        ['Gas turbines\newline ' num2str(X(6),'%.2f') ' t (' num2str(Y(6),'%.1f') '%)'],...
        ['Primary EM''s\newline  ' num2str(X(7),'%.2f') ' t (' num2str(Y(7),'%.1f') '%)'],...
        ['Secondary EM''s\newline    ' num2str(X(8),'%.2f') ' t (' num2str(Y(8),'%.1f') '%)']};
    explode = [0 0 0 0 0 0 0 0];
    
    % Remove divisions which are zero/not used
    if sum(X==0)>0
        Z(X==0) = []; explode(X==0) = []; piecolors(X==0,:)=[]; X(X==0) = [];
    end
    
    % Generate chart
    H = pie(X,explode,Z);
    if M.bat == 0
        hT = title(['MTOM = ' num2str(M.TOM/1000,'%.1f') 't, S_w = ' num2str(aircraft.Sw,'%.0f') ' m^2']);
    else
        if M.bat == M.bat_E
            hT = title({['MTOM = ' num2str(M.TOM/1000,'%.1f') 't, S_w = ' num2str(aircraft.Sw,'%.0f') ' m^2'],...
                '(Battery sized by energy requirements)'});
        elseif M.bat == M.bat_P
            hT = title({['MTOM = ' num2str(M.TOM/1000,'%.1f') 't, S_w = ' num2str(aircraft.Sw,'%.0f') ' m^2'],...
                '(Battery sized by power requirements)'});
        end
    end
    hT.Position = [0 -1.5 0];
    if length(H) == 2*length(X)
        for i = 1:length(X)
            hAux = H(2*i-1);
            hAux.FaceColor = piecolors(i,:);
        end
    end

% Class-II weight breakdown    
elseif  s.WeightEstimationClass == 2
    
    % Start with payload, fuel, battery weight
    X = [M.PL M.f M.bat]/1000;
    Y = 100*X/M.TOM*1000;
    Z = {['Payload, ' num2str(X(1),'%.2f') ' t (' num2str(Y(1),'%.1f') '%)'],...
        ['Fuel, ' num2str(X(2),'%.2f') ' t (' num2str(Y(2),'%.1f') '%)'],...
        ['Batteries, ' num2str(X(3),'%.2f') ' t (' num2str(Y(3),'%.1f') '%)']};
    pieColorsUsed = piecolors2(1:3,:);
    
    % Loop over the four groups comprising OEM
    groupNames = {'airframeStruct','propGroup','servAndEquip','operItems'};
    for i = 1:length(groupNames)
        
        % Loop over sub-fields
        fNames = fieldnames(M.(groupNames{i}));
        for j = 1:length(fNames)
            X(end+1) = M.(groupNames{i}).(fNames{j})/1000;
            Y(end+1) = 100*X(end)/M.TOM*1000;
            Z{end+1} = [fNames{j} ', ' num2str(X(end),'%.2f') ...
                        ' t (' num2str(Y(end),'%.1f') '%)'];
            col = piecolors2(3+i,:)*(1-0.8*(j-1)/length(fNames));
            pieColorsUsed(end+1,:) = col; 
        end
    end

    % Do not explode slices
    explode = zeros(size(Y));  
    
    % Remove divisions which are zero/not used
    if sum(X==0)>0
        Z(X==0) = []; explode(X==0) = []; pieColorsUsed(X==0,:)=[]; X(X==0) = [];
    end
    
    % Generate chart
    H = pie(X,explode,Z);
    if M.bat == 0
        hT = title(['MTOM = ' num2str(M.TOM/1000,'%.1f') 't, S_w = ' num2str(aircraft.Sw,'%.0f') ' m^2']);
    else
        if M.bat == M.bat_E
            hT = title({['MTOM = ' num2str(M.TOM/1000,'%.1f') 't, S_w = ' num2str(aircraft.Sw,'%.0f') ' m^2'],...
                '(Battery sized by energy requirements)'});
        elseif M.bat == M.bat_P
            hT = title({['MTOM = ' num2str(M.TOM/1000,'%.1f') 't, S_w = ' num2str(aircraft.Sw,'%.0f') ' m^2'],...
                '(Battery sized by power requirements)'});
        end
    end
    hT.Position = [0 -1.5 0];
    if length(H) == 2*length(X)
        for i = 1:length(X)
            hAux = H(2*i-1);
            hAux.FaceColor = pieColorsUsed(i,:);
        end
    end
    
    % Add group weights in separate boxes on the left
    str1 = ['Airframe structures group = ' num2str(M.airframeStructMass/1000,'%.2f') ...
            ' t (' num2str(M.airframeStructMass/M.TOM*100,'%.1f') '%)'];
    annotation('textbox', [0.05, 0.90, 0.3, 0.1],'String',str1,...
            'FitBoxToText','on','color',piecolors2(3+1,:)*0.8,'edgecolor','none')
    str2 = ['Propulsion group = ' num2str(M.propGroupMass/1000,'%.2f') ...
            ' t (' num2str(M.propGroupMass/M.TOM*100,'%.1f') '%)'];
    annotation('textbox', [0.05, 0.85, 0.3, 0.1],'String',str2,...
            'FitBoxToText','on','color',piecolors2(3+2,:)*0.8,'edgecolor','none')
    str3 = ['Services & equipment = ' num2str(M.servAndEquipMass/1000,'%.2f') ...
            ' t (' num2str(M.servAndEquipMass/M.TOM*100,'%.1f') '%)'];
    annotation('textbox', [0.05, 0.80, 0.3, 0.1],'String',str3,...
            'FitBoxToText','on','color',piecolors2(3+3,:)*0.8,'edgecolor','none')
    str4 = ['Operational items = ' num2str(M.operItemsMass/1000,'%.2f') ...
            ' t (' num2str(M.operItemsMass/M.TOM*100,'%.1f') '%)'];
    annotation('textbox', [0.05, 0.75, 0.3, 0.1],'String',str4,...
            'FitBoxToText','on','color',piecolors2(3+4,:)*0.8,'edgecolor','none')
    str5 = ['Operating empty weight = ' num2str(M.OEM/1000,'%.2f') ...
            ' t (' num2str(M.OEM/M.TOM*100,'%.1f') '%)'];
    annotation('textbox', [0.05, 0.70, 0.3, 0.1],'String',str5,...
            'FitBoxToText','on','color','k','edgecolor','none') 
        
    % Rotate labels to position them radially
    ax = gca;
    for i = 1:length(ax.Children)
        if strcmp(ax.Children(i).Type,'text')
            hh = ax.Children(i);
            rotation = rad2deg(atan2(hh.Position(2),hh.Position(1)));
            hh.VerticalAlignment = 'middle';
            if rotation > 90
                hh.Rotation = rotation-180;
                hh.HorizontalAlignment = 'right';
            elseif rotation < -90
                hh.Rotation = rotation+180;
                hh.HorizontalAlignment = 'right';
            else
                hh.Rotation = rotation;
                hh.HorizontalAlignment = 'left';
            end
        end
    end
    ax.Position = [0.2 0.2 0.6 0.6];
end


%% VI: Add corrected design point to WP plots
if s.MAResize == 1
    
    % Corrected GT WP-WS plot
    if ~isnan(WPdes.(s.SelDes).GTM)
        index = 0;
        figName = 'Corrected gas turbine comp. wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
                % Do nothing if an error is returned, which is normally due to
                % empty graphics holders in the graphics array (e.g. if a figure
                % has been closed)
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No corrected-GT power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPdes.(s.SelDes).GTM,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPdes.(s.SelDes).GTM,'.k','markersize',10);
        end
    end
    
    % EM1 plot
    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e')
        WPplot = WPdes.(s.SelDes).EM1M;
    else
        WPplot = WPdes.(s.SelDes).EM1;
    end
    if ~isnan(WPplot)
        index = 0;
        figName = 'Primary elec. machine comp. wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No EM1 power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPplot,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPplot,'.k','markersize',10);
        end
    end
    
    % EM2 plot
    if strcmp(p.config,'e-2') 
        WPplot = WPdes.(s.SelDes).EM2M;
    else
        WPplot = WPdes.(s.SelDes).EM2;
    end
    if ~isnan(WPplot)
        index = 0;
        figName = 'Secondary elec. machine comp. wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No EM2 power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPplot,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPplot,'.k','markersize',10);
        end
    end
    
    % Fuel WP-WS plot
    if ~isnan(WPdes.(s.SelDes).f)
        index = 0;
        figName = 'Fuel wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No fuel power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPdes.(s.SelDes).f,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPdes.(s.SelDes).f,'.k','markersize',10);
        end
    end
    
    % Battery WP-WS plot
    if ~isnan(WPdes.(s.SelDes).bat)
        index = 0;
        figName = 'Battery wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No battery power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPdes.(s.SelDes).bat,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPdes.(s.SelDes).bat,'.k','markersize',10);
        end
    end
    
    % Primary shaft WP-WS plot
    if ~isnan(WPdes.(s.SelDes).s1)
        index = 0;
        figName = 'Primary shaft wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No primary shaft power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPdes.(s.SelDes).s1,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPdes.(s.SelDes).s1,'.k','markersize',10);
        end
    end
    
    % Secondary shaft WP-WS plot
    if ~isnan(WPdes.(s.SelDes).s2)
        index = 0;
        figName = 'Secondary shaft wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No secondary shaft power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPdes.(s.SelDes).s2,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPdes.(s.SelDes).s2,'.k','markersize',10);
        end
    end
    
    % Propulsive WP-WS plot
    if ~isnan(WPdes.(s.SelDes).p)
        index = 0;
        figName = 'Total propulsive wing-loading/power-loading diagram';
        for k = 1:size(s.figs,2)
            figHandle = s.figs(k);
            try
                if strcmp(figHandle.Name,figName)
                    index = k;
                    break
                end
            catch
            end
        end
        if index == 0
            if s.printMessages == 1
                disp([s.levelString '  > No propulsive power-loading plot was '...
                    'found to show the corrected design point'])
            end
        else
            figure(s.figs(index)); hold on
            text(WSdes.(s.SelDes),WPdes.(s.SelDes).p,'{  }Re-sized design point')
            plot(WSdes.(s.SelDes),WPdes.(s.SelDes).p,'.k','markersize',10);
        end
    end
end



