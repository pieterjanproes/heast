% This script is an extract of Main.m, called in MainLoop_Sweep3D_v2.
% Placed in separate script here for readability.

% Check input is consistent
if s.printMessages == 1
    disp([s.levelString '> Checking input settings'])
end
CheckInput;

% Start Class-II convergence loop 
errC2 = 1;
iterC2 = 0;
xManual0 = s.xManual;
while errC2 > s.errmax
    
    % Set iteration conter
    iterC2 = iterC2 + 1;
    
    % Assign/update aerodynamic characteristics
    AssignAeroProperties;
    
    % Generate wing-loading power-loading diagrams
    if s.printMessages == 1
        disp([s.levelString '> Evaluating W/S-W/P diagram'])
    end
    s0 = s.printMessages; s.printMessages = 0;
    WP_WS_diagram;
    s.printMessages = s0;

    % Compute WS & WP of reference aircraft for weight estimation (Class I)
    if s.WeightEstimationClass ~= 2 || iterC2 == 1
        if s.printMessages == 1
            disp([s.levelString '> Computing reference aircraft WS/WP'])
        end
        [WSdes_ref,WPdes_ref] = ComputeReferencePowerLoading(a,m,p,s,c,f);
    end
    
    % Run mission analysis (incl weight estimation)
    if s.printMessages == 1
        disp([s.levelString '> Starting Mission Analysis'])
    end
    s0 = s.printMessages; s.printMessages = 0;
    MissionAnalysis;
    s.printMessages = s0;

    % Steps not performed in Class-I analysis
    if s.WeightEstimationClass == 2
        
        % Determine geometry, CG, & tail size
        if s.printMessages == 1
            disp([s.levelString '> Determining geometry, CG, & tail size'])
        end
        CallConfigurationAndGeometry; 
        
        % Perform Class-II aero
        if s.AeroEstimationClass == 2
            if s.printMessages == 1
                disp([s.levelString '> Performing Class-II aero estimation'])
            end
            C2_in = C2;
            C2_in.settings.plotAero = 0;
            [a,errC2_aero,~] = ComputeClassIIAero(a,m,p,f,s,C2_in,aircraft,MA);
        else
            errC2_aero = 0;
        end
    end
    
    % Check convergence: for Class-I analysis, set error to zero and stop here
    if s.WeightEstimationClass ~= 2
        errC2 = 0;
        
    % For Class-II analysis, add error contributions. Note that individual
    % error contributions are computed inside CallConfigurationAndGeometry.
    % Initial values for next iteration are also updated there.
    else
        
        % Total error
        errC2 = abs(errC2_TOM) + abs(errC2_xCG) + ...
                abs(errC2_sHT) + abs(errC2_sVT) + abs(errC2_aero);  
           
        % Display progress
        if s.printMessages == 1
            disp([s.levelString 'Completed Class-II iteration ' num2str(iterC2) ...
                  '. Error = ' num2str(errC2)])
        end
    end
    
    % Break if not converging
    if iterC2 > s.itermax/4
        error('Class-II sizing did not converge');
    end    
    if isnan(errC2)
        error('Class-II convergence criterion attained NaN value')
    end
    
    % Checking/debugging: show progress in selection of design point
    %{
    cols = {'r','b','g','m','c','k','y'};
    figure(1000)
    subplot(2,2,1); hold on; grid on; box on
    plot3(counter,iterC2,WSdes.(s.SelDes),'xr')
    xlabel('Outer loop iter');ylabel('C2 convergence iter');
    zlabel('Wing loading [N/m^2]'); view(3)
    subplot(2,2,2); hold on; grid on;
    plot3(counter,iterC2,s.xManual,'xr')
    xlabel('Outer loop iter');ylabel('C2 convergence iter');
    zlabel('s.xManual [-]'); view(3)
    subplot(2,2,3); hold on; grid on;
    plot3(counter,iterC2,aircraft.b,'xr')
    xlabel('Outer loop iter');ylabel('C2 convergence iter');
    zlabel('Wing span [m]'); view(3)
    subplot(2,2,4); hold on; grid on;
    if exist('idx','var')   % Variable altitude
        plot3(counter,iterC2,m.cr.h,'+','markeredgecolor',cols{idx})
    else                    % Fixed altitude
        plot3(counter,iterC2,m.cr.h,'xr')
    end    
    xlabel('Outer loop iter');ylabel('C2 convergence iter');
    zlabel('Altitude [m]'); view(3)
    drawnow; 
    %}
    
    % If fixed span, update wing loading selection for next iteration
    if errC2 > s.errmax && strcmp(s.SelDes,'manual') && ...
       s.WeightEstimationClass == 2 && C2.settings.FixedSpan ~= 0
        WSlimit = a.AR*c.g*M.TOM/C2.settings.SpanLimit^2;
        xLimit = WSlimit/WSdes.minWS;
            
        % Equality constraint (set b = SpanLimit)
        if C2.settings.FixedSpan == 1  
            s.xManual = xLimit;
            
        % Inquality constraint (set b = SpanLimit only if currently b > SpanLimit)
        elseif C2.settings.FixedSpan == 2 
            if xLimit > xManual0 
                s.xManual = xLimit;
            else
                s.xManual = xManual0;   % Reset to original input value,
            end                         % in case modified in previous iter
        end
        
        % Limit to max wing loading at most
        if s.xManual > 1
            if s.printMessages == 1
                disp([s.levelString ...
                    '> Span obtained at max wing loading exceeds span limit'])
            end
            s.xManual = 1;
        end
    end

    % Checking/debugging
    %{
    if s.WeightEstimationClass == 2
        disp([errC2_TOM errC2_xCG errC2_sHT errC2_sVT])
        [~] = PlotClassIIPieChart_temp(M,aircraft,s);
        drawnow
    end
    %}
end
