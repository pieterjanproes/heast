function [AFlipped] = MissionAnalysisLoiter(a,p,f,s,c,con,aircraft,MA_in,t_target,vLimits)
%% Initialize variables

% Select corresponding flight segment
A = MA_in.(con);

% Initial guess for delta eta_p
detap = 0;

% Aircraft characteristics
D2 = aircraft.D2;
D2_conv = aircraft.D2_conv;
Sw = aircraft.Sw;
Rc = aircraft.Rc;

% Powertrain component efficiencies
etas.GT = p.eta_GT;     % Gas turbine
etas.GB = p.eta_GB;     % Gearbox
etas.EM1 = p.eta_EM1;   % Primary electrical machine
etas.PM = p.eta_PM;     % PMAD (power management and distribution)
etas.EM2 = p.eta_EM2;   % Secondary electrical machine

% Gravity
g = c.g;

% Initial guess for required propulsive power 
[~,~,~,rho] = atmosisa(A.h(1));
v = A.v(1);
Pp = 0.5*rho*v^3*Sw*(a.(con).CD0 + 0.5^2/pi/a.AR/a.(con).e);

% Time step array
nTimeSteps = round(t_target/s.dt.(con));
t0 = A.t(1);
tArray = linspace(t0,t0 - t_target,nTimeSteps);
dt = tArray(1)-tArray(2);

% Velocities to be evaluated in order to determine optimum in terms of LD
velEval = linspace(vLimits(1),vLimits(2),s.LoiterVelEvals);
velRefined = linspace(vLimits(1),vLimits(2),1000*s.LoiterVelEvals);


%% Loop backwards over all time steps
for k = 1:length(tArray)
    
    % Operating conditions
    h = A.h(k);
    [T_inf,aa,~,rho] = atmosisa(h);
    G = A.G(k);
    W = A.W(k);
    dvdt = A.dVdt(k);
    
    % Select power-control parameters
    xi = interp1([t0 t0-t_target],A.xi,tArray(k),'linear');
    phi = interp1([t0 t0-t_target],A.phi,tArray(k),'linear');
    Phi = interp1([t0 t0-t_target],A.Phi,tArray(k),'linear');
      
    % Loop over multiple velocities
    deltaETot = NaN(size(velEval));
    for kk = 1:s.LoiterVelEvals
        
        % Select velocity
        vEval = velEval(kk);
        qEval = 0.5*rho*vEval^2;
        MEval = vEval/aa;
        ReEval = vEval*a.c_ref*rho/f.mu(T_inf);
        
        % Compute available power at altitude
        if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e')
            Pa = aircraft.Pdes.EM1M;
        elseif strcmp(p.config,'e-2')
            Pa = aircraft.Pdes.EM2M;
        else
            Pa = aircraft.Pdes.GTM.*f.Alpha(rho,MEval,c);
        end
        
        % Use previous solution for initial guess
        if k == 1
            CL_iso0 = 0.5;
            Tc0 = 0.1;
            Tc0_conv = 0.1;
        else
            CL_iso0 = A.aero.CLiso(k-1);
            Tc0 = A.aero.Tc(k-1);
            Tc0_conv = A.aero.Tc_conv(k-1);
        end
        err = 1;
        while err > s.errmax
            
            % Estimate isolated prop efficiency based on Tc, if requested
            if s.ComputeEtap == 1
                etap = f.etap(Tc0);
                etap_conv = f.etap(Tc0_conv);
            else
                if p.DP == 2
                    etap = p.(con).etap2;
                    etap_conv = p.(con).etap1;
                elseif  p.DP == 1
                    etap = p.(con).etap1;
                    etap_conv = p.(con).etap2;
                else
                    if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
                            strcmp(p.config,'e-1')
                        etap = p.(con).etap2;
                        etap_conv = p.(con).etap1;
                    elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
                            strcmp(p.config,'e-2')
                        etap = p.(con).etap1;
                        etap_conv = p.(con).etap2;
                    else
                        etap = p.(con).etap2;
                        etap_conv = p.(con).etap1;
                    end
                end
            end
            
            % Recompute thrust share
            if p.DP == 1 && ~isnan(Phi)
                chi = 1/(Phi/(1-Phi) * etap_conv/(etap+detap) + 1);
            elseif (p.DP == 2 || p.DP == 0) && ~isnan(Phi)
                chi = 1/((1-Phi)/Phi * etap_conv/(etap+detap) + 1);
            else
                chi = p.(con).chi(end);
            end
            
            % Compute thrust
            T = Pp/vEval;
            Tc1 = chi*T/p.N/rho/vEval^2/D2;
            Tc1_conv = (1-chi)*T/p.N_conv/rho/vEval^2/D2_conv;
            
            % Calculate required total lift coefficient
            CL = W/Sw/qEval*((1-G^2)^0.5-chi*sind(p.(con).Gamma)*T/W);
            
            % Obtain deltas
            oper.e = a.(con).e;
            oper.CD0 = a.(con).CD0;
            oper.Gamma = p.(con).Gamma;
            oper.CL = CL_iso0;
            oper.M = MEval;
            oper.etap = etap;
            oper.Tc = Tc1;
            oper.Tc_conv = Tc1_conv;
            oper.Rc = Rc;
            oper.Re = ReEval;
            [dCL,dCD0,dCDi,detap] = WingPropDeltas_v5(oper,a,p,f,s,c,0);
            
            % Update lift coefficient
            CL_iso = CL-dCL;
            
            % Compute error. For 0/0, error will return NaN. In that case,
            % error should be zero
            err1 = abs((CL_iso-CL_iso0)/CL_iso0);
            if any([(Tc1 ==0 && Tc0 == 0),(Tc1 == Inf && Tc0 == Inf),isnan(Tc1),isnan(Tc0)])
                err2 = 0;
            else
                err2 = abs((Tc1-Tc0)/Tc0);
            end
            if any([(Tc1_conv ==0 && Tc0_conv == 0),(Tc1_conv == Inf && Tc0_conv == Inf),...
                isnan(Tc1_conv),isnan(Tc0_conv)])
                err3 = 0;
            else
                err3 = abs((Tc1_conv-Tc0_conv)/Tc0_conv);
            end
            err = err1 + err2 + err3;

            % If the error becomes a NaN, the code
            % will crash or provide an unconverged solution. Note that
            % err will never be complex because of the abs function
            if isnan(err)
                warning('Convergence error attained NaN value')
                CL_iso = NaN;
                Tc1 = NaN;
                Tc1_conv = NaN;
                break
            end
            
            % Update values for next iteration
            CL_iso0 = CL_iso;
            Tc0 = Tc1;
            Tc0_conv = Tc1_conv;  
                        
            % Update drag 
            CD_iso = f.CD(a.(con).CD0,CL_iso,a.(con).CL_minD,a.AR,a.(con).e);
            CD = (CD_iso + dCD0 + dCDi);

            % Update propulsive power
            % Ignore propulsive power required to accelerate: will be small
            % in practice, and leads to divergence (stability issues). Use
            % quasi-steady approach instead
            Pp = vEval*W*(qEval/W*Sw*CD + G + 0/g)/...
                (1 - chi*(1-cosd(p.(con).Gamma)));
        end
                
        % Update propulsive efficiency
        if p.DP == 2
            etas.P1 = etap_conv;
            etas.P2 = etap + detap;
        elseif p.DP == 1
            etas.P1 = etap + detap;
            etas.P2 = etap_conv;
        else
            if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
                    strcmp(p.config,'e-1')
                etas.P1 = etap_conv;
                etas.P2 = etap;
            elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
                    strcmp(p.config,'e-2')
                etas.P1 = etap;
                etas.P2 = etap_conv;
            else
                etas.P1 = etap_conv;
                etas.P2 = etap;
            end
        end
        
        % Calculate how much power is required from each component
        [P_out,xi_out,~,~,~,~] = PowerTransmissionComputation_v3...
            (p.config,etas,phi,Phi,xi,Pp,[],[],Pa);   
        
        % Correct fuel power for part-throttle efficiency lapse
        if s.ComputeEtaGt == 1
            xi_In = xi_out;
            xi_In(isnan(xi_In)) = 1;
            P_out.f = P_out.f./f.etagt(xi_In,MEval);
        end
        
        % Hypothetical cooling power required
        if s.WeightEstimationClass == 2
            P_EM1 = min([P_out.e1 P_out.gb])*(1 - p.eta_EM1);
            P_EM2 = min([P_out.e2 P_out.s2])*(1 - p.eta_EM2);
            P_PM = max([P_out.bat P_out.e1 P_out.e2])*(1 - p.eta_PM);
            P_bat = P_out.bat*(1 - p.eta_bat);
            Ps = sum([P_EM1 P_EM2 P_PM P_bat],'omitnan');
            A.P.cooling(k) = Ps*p.cooling_power;
        else
            A.P.cooling(k) = 0;
        end

        % Hypothetical source power required for non-propulsive uses 
        if strcmp(p.config,'conventional') || strcmp(p.config,'turboelectric') || ...
                strcmp(p.config,'PTE')
            A.P.offtake(k) = MA_in.P_offtake*MA_in.N_pax/p.eta_GT; 
            P_offtake_fuel = A.P.offtake(k);
            P_offtake_bat = 0;
            P_cooling_fuel = A.P.cooling(k);
            P_cooling_bat = 0;
        else
            A.P.offtake(k) = MA_in.P_offtake*MA_in.N_pax;          
            P_offtake_fuel = 0;
            P_offtake_bat =  A.P.offtake(k);
            P_cooling_fuel = 0;
            P_cooling_bat = A.P.cooling(k);
        end

        % Hypothetical energy consumption in this timestep
        deltaEbat = -(P_out.bat + P_offtake_bat + P_cooling_bat)*dt;
        if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e') ||...
                strcmp(p.config,'e-2')
            deltaEf = 0;
        else
            deltaEf = -(P_out.f + P_offtake_fuel + P_cooling_fuel)*dt;
        end
        
        % Total energy consumption (positive)
        if isnan(deltaEbat) && ~isnan(deltaEf)
            deltaETot(kk) = - deltaEf;
        elseif ~isnan(deltaEbat) && isnan(deltaEf)
            deltaETot(kk) = - deltaEbat;
        else
            deltaETot(kk) = -(deltaEbat + deltaEf);
        end
    end
    
    % Fit polynomial to APE
    vEvalScaled = (velEval-nanmean(velEval))/(max(velEval)-min(velEval));
    vRefinedScaled = (velRefined-nanmean(velEval))/(max(velEval)-min(velEval));
    deltaETotScaled = (deltaETot-nanmean(deltaETot))/(max(deltaETot)-min(deltaETot));
    POLY = polyfit(vEvalScaled,deltaETotScaled,s.LoiterVelEvals-1);
    deltaETotRefinedScaled = polyval(POLY,vRefinedScaled);
    deltaETotRefined = deltaETotRefinedScaled*(max(deltaETot)-min(deltaETot)) + nanmean(deltaETot);
    
    % Select optimal velocity
    [~,idxMin] = min(deltaETotRefined);
    velOpt = velRefined(idxMin);

    % Figure for checking optima
    %{
    [deltaEMin,~] = min(deltaETotRefined);
    figure(4964)
    hold on; grid on; box on;
    H1 = plot3(velEval,tArray(k)*ones(size(deltaETot)),deltaETot,'or');
    H2 = plot3(velRefined,tArray(k)*ones(size(deltaETotRefined)),deltaETotRefined,'-b');
    H3 = plot3(velOpt,tArray(k)*ones(size(deltaEMin)),deltaEMin,'og');
    xlabel('Velocity [m/s]')
    ylabel('Time [s]')
    zlabel('Energy consumed in timestep [J]')
    if k == 1
        legend([H1 H2 H3],'Evaluated','Poly fit','Optimum')
    end
    %}
    
    % EVALUATE AT OPTIMAL VELOCITY
    v = velOpt;
    M = v/aa;
    q = 0.5*rho*v^2;
    Re = v*a.c_ref*rho/f.mu(T_inf);
    
    % Calculate lift required and break down into delta CL and CLiso
    if k == 1
        CL_iso0 = 0.5;
        Tc0 = 0.1;
        Tc0_conv = 0.1;
    else
        CL_iso0 = A.aero.CLiso(k-1);
        Tc0 = A.aero.Tc(k-1);
        Tc0_conv = A.aero.Tc_conv(k-1);
    end
    err = 1;
    while err > s.errmax
        
        % Estimate isolated prop efficiency based on Tc, if requested
        if s.ComputeEtap == 1 
            etap = f.etap(Tc0); 
            etap_conv = f.etap(Tc0_conv); 
        else  
            if p.DP == 2
                etap = p.(con).etap2; 
                etap_conv = p.(con).etap1;
            elseif  p.DP == 1
                etap = p.(con).etap1; 
                etap_conv = p.(con).etap2;
            else
                if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
                        strcmp(p.config,'e-1')
                    etap = p.(con).etap2;
                    etap_conv = p.(con).etap1;
                elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
                        strcmp(p.config,'e-2')
                    etap = p.(con).etap1;
                    etap_conv = p.(con).etap2;
                else
                    etap = p.(con).etap2;
                    etap_conv = p.(con).etap1;
                end
            end
        end
        
        % Recompute thrust share
        if p.DP == 1 && ~isnan(Phi)
            chi = 1/(Phi/(1-Phi) * etap_conv/(etap+detap) + 1);
        elseif (p.DP == 2 || p.DP == 0) && ~isnan(Phi)
            chi = 1/((1-Phi)/Phi * etap_conv/(etap+detap) + 1);
        else
            chi = p.(con).chi(end);
        end
        
        % Compute thrust (note: in first time step, the thrust requirement
        % is computed without A-P effects!)
        T = Pp/v;
        Tc1 = chi*T/p.N/rho/v^2/D2;
        Tc1_conv = (1-chi)*T/p.N_conv/rho/v^2/D2_conv;
        
        % Calculate required total lift coefficient
        CL = W/Sw/q*((1-G^2)^0.5-chi*sind(p.(con).Gamma)*T/W);
        
        % Obtain deltas
        oper.e = a.(con).e;
        oper.CD0 = a.(con).CD0;
        oper.Gamma = p.(con).Gamma;
        oper.CL = CL_iso0;
        oper.M = M;
        oper.etap = etap;
        oper.Tc = Tc1;
        oper.Tc_conv = Tc1_conv;
        oper.Rc = Rc;
        oper.Re = Re;
        [dCL,dCD0,dCDi,detap] = WingPropDeltas_v5(oper,a,p,f,s,c,0);
        
        % Check convergence of lift coefficient and propulsive efficiency
        % and update
        CL_iso = CL-dCL;
        err1 = abs((CL_iso-CL_iso0)/CL_iso0);
        if any([(Tc1 ==0 && Tc0 == 0),(Tc1 == Inf && Tc0 == Inf),isnan(Tc1),isnan(Tc0)])
            err2 = 0;
        else
            err2 = abs((Tc1-Tc0)/Tc0);
        end
        if any([(Tc1_conv ==0 && Tc0_conv == 0),(Tc1_conv == Inf && Tc0_conv == Inf),...
                isnan(Tc1_conv),isnan(Tc0_conv)])
            err3 = 0;
        else
            err3 = abs((Tc1_conv-Tc0_conv)/Tc0_conv);
        end
        err = err1 + err2 + err3;
        
        % If the error becomes a NaN, the code
        % will crash or provide an unconverged solution. Note that
        % err will never be complex because of the abs function
        if isnan(err)
            warning('Convergence error attained NaN value')
            CL_iso = NaN;
            Tc1 = NaN;
            Tc1_conv = NaN;
            break
        end
        
        % Update for next iteration
        CL_iso0 = CL_iso;
        Tc0 = Tc1;
        Tc0_conv = Tc1_conv;
        
        % Update drag and required flight power
        CD_iso = f.CD(a.(con).CD0,CL_iso,a.(con).CL_minD,a.AR,a.(con).e);
        CD = (CD_iso + dCD0 + dCDi);
        D = CD*q*Sw;
        Pp = v*W*(q/W*Sw*CD + G + dvdt/g)/(1 - chi*(1-cosd(p.(con).Gamma)));
    end
    
    % Compute lift-to-drag ratio in actual cruise condition
    LD = CL/CD;
          
    % Update propulsive efficiency
    if p.DP == 2
        etas.P1 = etap_conv;
        etas.P2 = etap + detap;
    elseif p.DP == 1
        etas.P1 = etap + detap;
        etas.P2 = etap_conv;
    else
        if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
                strcmp(p.config,'e-1')
            etas.P1 = etap_conv;
            etas.P2 = etap;
        elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
                strcmp(p.config,'e-2')
            etas.P1 = etap;
            etas.P2 = etap_conv;
        else
            etas.P1 = etap_conv;
            etas.P2 = etap;
        end
    end
    
    % Compute available power at altitude
    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e')
        Pa = aircraft.Pdes.EM1;
    elseif strcmp(p.config,'e-2')
        Pa = aircraft.Pdes.EM2;
    else
        Pa = aircraft.Pdes.GTM.*f.Alpha(rho,M,c);
    end
    
    % Calculate how much power is required from each component
    [P_out,xi_out,phi_out,Phi_out,~,xi_flag] = PowerTransmissionComputation_v3...
        (p.config,etas,phi,Phi,xi,Pp,[],[],Pa);

    % Correct fuel power for part-throttle efficiency lapse
    if s.ComputeEtaGt == 1
        xi_In = xi_out;
        xi_In(isnan(xi_In)) = 1;
        eta_GT = p.eta_GT * f.etagt(xi_In,M);
        P_out.f = P_out.f./f.etagt(xi_In,M);
    else
        eta_GT = p.eta_GT;
    end
    
    % Cooling power required
    if s.WeightEstimationClass == 2
        P_EM1 = min([P_out.e1 P_out.gb])*(1 - p.eta_EM1);
        P_EM2 = min([P_out.e2 P_out.s2])*(1 - p.eta_EM2);
        P_PM = max([P_out.bat P_out.e1 P_out.e2])*(1 - p.eta_PM);
        P_bat = P_out.bat*(1 - p.eta_bat);
        Ps = sum([P_EM1 P_EM2 P_PM P_bat],'omitnan');
        A.P.cooling(k) = Ps*p.cooling_power;
    else
        A.P.cooling(k) = 0;
    end

    % Source power required for non-propulsive uses (currently assumed
    % constant and independent of flight condition). Note that this power
    % requirement is NOT included in power sizing of constraint diagram.
    if strcmp(p.config,'conventional') || strcmp(p.config,'turboelectric') || ...
            strcmp(p.config,'PTE')
        A.P.offtake(k) = MA_in.P_offtake*MA_in.N_pax/p.eta_GT;  % Extracted from shaft
        P_offtake_fuel = A.P.offtake(k);
        P_offtake_bat = 0;
        P_cooling_fuel = A.P.cooling(k);
        P_cooling_bat = 0;
    else
        A.P.offtake(k) = MA_in.P_offtake*MA_in.N_pax;           % Extracted from battery
        P_offtake_fuel = 0;
        P_offtake_bat =  A.P.offtake(k);
        P_cooling_fuel = 0;
        P_cooling_bat = A.P.cooling(k);
    end
    
    % Delta weight corresponding to this timestep
    deltaEbat = -(P_out.bat + P_offtake_bat + P_cooling_bat)*dt;
    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e') ||...
            strcmp(p.config,'e-2')
        deltaEf = 0;
    else
        deltaEf = -(P_out.f + P_offtake_fuel + P_cooling_fuel)*dt;
    end
    deltaW = g*deltaEf/p.SE.f;
    
    % Compute change in range & altitude for next step
    deltah = 0;
    deltaR = 0;
            
    % Update data for next iteration
    A.t(k+1) = A.t(k) - dt;
    A.h(k+1) = A.h(k) - deltah;
    A.W(k+1) = A.W(k) - deltaW;
    A.R(k+1) = A.R(k) - deltaR;
    A.Ebat(k+1) = A.Ebat(k) - deltaEbat;
    A.Ef(k+1) = A.Ef(k) - deltaEf;
    A.dhdt(k+1) = deltah/dt;
    A.G(k+1) = A.dhdt(k+1)/v;
    
    % Overwrite velocity for current timestep, prescribe for next
    % timestep as initial guess
    A.v(k+1) = v;       A.v(k) = v;
    A.M(k+1) = v/aa;    A.M(k) = v/aa;
    if k == 1
        A.dVdt(k+1) = 0;
        A.dVdt(k) = 0;
    else
        A.dVdt(k+1) = (A.v(k)-A.v(k-1))/dt;
        A.dVdt(k) = (A.v(k)-A.v(k-1))/dt;
    end  
    
    % Save aerodynamic variables
    A.aero.CLiso(k) = CL_iso;
    A.aero.CL(k) = CL;
    A.aero.CDiso(k) = CD_iso;
    A.aero.CD(k) = CD;
    A.aero.LD(k) = LD;
    A.aero.Tc(k) = Tc1;
    A.aero.Tc_conv(k) = Tc1_conv;
    if p.DP == 1
        A.aero.etap1(k) = etap + detap;
        A.aero.etap2(k) = etap_conv;
        A.aero.etap1iso(k) = etap;
        A.aero.etap2iso(k) = etap_conv;
    elseif p.DP == 2
        A.aero.etap1(k) = etap_conv;
        A.aero.etap2(k) = etap + detap;
        A.aero.etap1iso(k) = etap_conv;
        A.aero.etap2iso(k) = etap;
    else
        if strcmp(p.config,'conventional') || ...
                strcmp(p.config,'parallel') ||...
                strcmp(p.config,'e-1')
            A.aero.etap1(k) = etap_conv;
            A.aero.etap2(k) = etap;
            A.aero.etap1iso(k) = etap_conv;
            A.aero.etap2iso(k) = etap;
        elseif strcmp(p.config,'serial') || ...
                strcmp(p.config,'turboelectric') ||...
                strcmp(p.config,'e-2')
            A.aero.etap1(k) = etap;
            A.aero.etap2(k) = etap_conv;
            A.aero.etap1iso(k) = etap;
            A.aero.etap2iso(k) = etap_conv;
        else
            A.aero.etap1(k) = etap_conv;
            A.aero.etap2(k) = etap;
            A.aero.etap1iso(k) = etap_conv;
            A.aero.etap2iso(k) = etap;
        end
    end
    
    % Save powers
    A.P.x(k) = NaN;
    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e') ||...
            strcmp(p.config,'e-2')
        A.P.availableGT(k) = NaN;
    else
        A.P.availableGT(k) = Pa;
    end
    A.P.drag(k) = D*v;        
    A.P.acceleration(k) = W*A.dVdt(k)*v/g;  
    A.P.climb(k) = W*A.dhdt(k);        
    if k == 1; names = fieldnames(P_out); end
    for j = 1:size(names,1)
        A.P.(names{j})(k) = P_out.(names{j});
    end
    A.P.etagt(k) = eta_GT;
    
    % Save power-control variables
    A.P.xi(k) = xi_out;
    A.P.xi_flag(k) = xi_flag;
    if strcmp(p.config,'serial') || strcmp(p.config,'parallel') ||...
            strcmp(p.config,'SPPH')
        A.P.phi(k) = phi_out;
    else
        A.P.phi(k) = NaN;
    end
    if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
            strcmp(p.config,'dual-e')
        A.P.Phi(k) = Phi_out;
    else
        A.P.Phi(k) = NaN;
    end
    A.P.chi(k) = chi;
    
    % Store effective propulsive efficiency
    if A.P.Phi(k) == 1 || strcmp(p.config,'turboelectric') || ...
            strcmp(p.config,'serial') || strcmp(p.config,'e-2')
        A.aero.etapAvg(k) = A.aero.etap2(k); 
    elseif A.P.Phi(k) == 0 || strcmp(p.config,'conventional') || ...
            strcmp(p.config,'parallel') || strcmp(p.config,'e-1')
        A.aero.etapAvg(k) = A.aero.etap1(k); 
    else
        A.aero.etapAvg(k) = (A.aero.etap1(k) + A.aero.etap2(k)*...
                             (A.P.Phi(k)/(1-A.P.Phi(k)))) / ...
                             (1 + (A.P.Phi(k)/(1-A.P.Phi(k)))); 
    end    
end

% Add last element to aerodynamic/power variables to make plotting arrays 
% same length
aeronames = fieldnames(A.aero);
for i = 1:size(aeronames,1)
    A.aero.(aeronames{i})(k+1) = NaN;
end
Pnames = fieldnames(A.P);
for i = 1:size(Pnames,1)
    A.P.(Pnames{i})(k+1) = NaN;
end

% Flip all output arrays so that first element is first interval of flight
% segment
aux = rmfield(A,{'xi','phi','Phi','aero','P'});
AFlipped = structfun(@(x) flip(x),aux,'UniformOutput',0);
PFlipped = structfun(@(x) flip(x),A.P,'UniformOutput',0);
aeroFlipped = structfun(@(x) flip(x),A.aero,'UniformOutput',0);
AFlipped.P = PFlipped;
AFlipped.aero = aeroFlipped;


