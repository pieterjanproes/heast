function [propulsionGroup] = CreatePropulsionGroup(propulsionGroup,wing,fuselage)
%%% Description
% This function estimates the geometry of the primary or secondary 
% propulsion system for a given set of input parameters.
% It performs similar tasks to the wing and fuselage groups in
% ConfigurationAndGeometry.m, but is gathered in a function because it's
% called at different location. 
% 
% The script creates N copies of the three sub components: propeller,
% motor, and nacelle (optional). The input design parameters can be
% specified as scalars, or as arrays of length = N/2. If scalar, then the
% same value is applied to all sub-component instances. If an array, then 
% the values must be given moving from inboard to starboard. These
% sub-components are Level-3 components in the overall aircraft tree.
%
% Note that component CG's (nacelle, motor, and prop) are currently assumed 
% to be at the center of the object.
%
%
%%% Reynard de Vries
%%% First created: 22-03-22
%%% Last modified: 04-05-22


%% Prepare input

% Outside loop: check selected P1/P2 is ocmpatible with powertrain
% architecture

% Check N has even values (if applicable)
if rem(propulsionGroup.geom.N,2) && strcmp(propulsionGroup.geom.type,'LEDP')
    error('An even number of propulsors is required for the LEDP system')
end

% Convert length of geometrical inputs to [1 x N] arrays, from port to
% starboard. Skip "y_over_b2", which must be computed separately
geomIn = propulsionGroup.geom;
varNames = {'D','x_over_c','z_over_c','incidence','D_mot','Dnac_over_Dmot',...
            'l_mot','xmot_over_Dprop','xnac_over_lnac','lnac_over_Dnac','sFraction'};
for i = 1:length(varNames)
    
    % For variables specified as scalar, repeat N times
    if length(propulsionGroup.geom.(varNames{i})) == 1
        geomIn.(varNames{i}) = propulsionGroup.geom.(varNames{i})*...
                                   ones(1,propulsionGroup.geom.N);
        
    % For variables specified correctly as an array, mirror to apply to
    % port and starboard side
    elseif all(size(propulsionGroup.geom.(varNames{i})) == [1, (propulsionGroup.geom.N/2)])
        geomIn.(varNames{i}) = [flip(propulsionGroup.geom.(varNames{i})) ...
                                     propulsionGroup.geom.(varNames{i})];
        
    % Return error if array is not the correct length
    else
        error(['The propulsion-group input variable ' varNames{i} ...
               ' must be a scalar or an array of length N/2'])
    end
end

% Convert length of weight inputs to [1 x N] arrays, from port to
% starboard. 
weightsIn = propulsionGroup.weights;
varNames = {'m_per_prop','m_per_nac','m_per_mot'};
for i = 1:length(varNames)
    
    % For variables specified as scalar, repeat N times
    if length(propulsionGroup.weights.(varNames{i})) == 1
        weightsIn.(varNames{i}) = propulsionGroup.weights.(varNames{i})*...
                                   ones(1,propulsionGroup.geom.N);
        
    % For variables specified correctly as an array, mirror to apply to
    % port and starboard side
    elseif all(size(propulsionGroup.weights.(varNames{i})) == [1, (propulsionGroup.geom.N/2)])
        weightsIn.(varNames{i}) = [flip(propulsionGroup.weights.(varNames{i})) ...
                                        propulsionGroup.weights.(varNames{i})];
        
    % Return error if array is not the correct length
    else
        error(['The propulsion-group input variable ' varNames{i} ...
               ' must be a scalar or an array of length N/2'])
    end
end

% Number of propulsors
N = geomIn.N;


%% Leading-edge distributed propulsion (wing mounted)
if strcmp(propulsionGroup.geom.type,'LEDP')

    % Compute spanwise locations. NB: along Y-axis, not in span direction!
    if length(geomIn.y_over_b2) == 1
        ys_port = NaN(1,geomIn.N/2);
        ys_stbd = NaN(1,geomIn.N/2);
        for i = 1:(N/2)
            if i == 1
                ys_port(i) = -geomIn.y_over_b2*wing.geom.b/2;
                ys_stbd(i) = +geomIn.y_over_b2*wing.geom.b/2;
            else
                ys_port(i) = ys_port(i-1) - cosd(wing.geom.dihedral)*...
                    ((1 + geomIn.dy)*(geomIn.D(i-1) + geomIn.D(i))/2);
                ys_stbd(i) = ys_stbd(i-1) + cosd(wing.geom.dihedral)*...
                    ((1 + geomIn.dy)*(geomIn.D(i-1) + geomIn.D(i))/2);
            end
        end
        geomIn.y_over_b2 = [flip(ys_port) ys_stbd]/(wing.geom.b/2);
    else
        geomIn.y_over_b2 = [-flip(geomIn.y_over_b2) geomIn.y_over_b2];
    end

    % Azimuthal coordinate for geometry
    theta = linspace(0,2*pi,propulsionGroup.settings.nAzimuth);
    
    % Loop over component instances
    coordsLE = NaN(3,N);
    propellers = cell(1,N);
    motors = cell(1,N);
    if propulsionGroup.geom.hasNacelles == 1
        nacelles = cell(1,N);
    end
    for i = 1:N
        
        % Extract wing LE line from wing surface
        if i <= (N/2)    % Port
            idxLE = ceil(size(wing.plotting.P{3},2)/2);
            xLE = squeeze(wing.plotting.P{3}(1,idxLE,:));
            yLE = squeeze(wing.plotting.P{3}(2,idxLE,:));
            zLE = squeeze(wing.plotting.P{3}(3,idxLE,:));
        else             % Starboard
            idxLE = ceil(size(wing.plotting.P{1},2)/2);
            xLE = squeeze(wing.plotting.P{1}(1,idxLE,:));
            yLE = squeeze(wing.plotting.P{1}(2,idxLE,:));
            zLE = squeeze(wing.plotting.P{1}(3,idxLE,:));
        end
        
        % Compute wing LE coordinates & chord length at spanwise stations
        coordsLE(2,i) = geomIn.y_over_b2(i)*wing.geom.b/2;
        if all(xLE == xLE(1)) % Unswept
            coordsLE(1,i) = xLE(1);
        else
            coordsLE(1,i) = interp1(yLE,xLE,coordsLE(2,i),'linear','extrap');
        end
        if all(zLE == zLE(1)) % Horizontal LE
            coordsLE(3,i) = zLE(1);
        else
            coordsLE(3,i) = interp1(yLE,zLE,coordsLE(2,i),'linear','extrap');
        end
        c_wing = interp1([0 1],...
                         [wing.geom.root.c_projected, wing.geom.tip.c_projected],...
                         abs(geomIn.y_over_b2(i)),'linear','extrap');

        %%% 1. Propellers
        % Compute propeller disk center (= CG) location (in wing coordinate
        % sytem)
        coordsCenterProp = [coordsLE(1,i) + geomIn.x_over_c(i)*c_wing;
                            coordsLE(2,i);
                            coordsLE(3,i) + geomIn.z_over_c(i)*c_wing];
                       
        % Coordinates of propeller disk in prop reference frame (azimuthal 
        % coordinate along second dimension)
        coordsProp = [zeros(1,length(theta));
                      	  cos(theta)*geomIn.D(i)/2;
                          sin(theta)*geomIn.D(i)/2];
                  
        % Coordinates of propeller disk, corrected for incidence angle and
        % shifted to correct position
        coordsPropRot(1,:) = coordsCenterProp(1) + ...
                          coordsProp(1,:)*cosd(geomIn.incidence(i)) + ...
                          coordsProp(3,:)*sind(geomIn.incidence(i));
        coordsPropRot(2,:) = coordsCenterProp(2) + coordsProp(2,:);
        coordsPropRot(3,:) = coordsCenterProp(3) - ...
                            coordsProp(1,:)*sind(geomIn.incidence(i)) + ...
                            coordsProp(3,:)*cosd(geomIn.incidence(i)); 
                        
        % Store in output cell array
        propellers{i}.weights.m = weightsIn.m_per_prop(i);
        propellers{i}.weights.coords_loc = coordsCenterProp;
        propellers{i}.geom.D = geomIn.D(i);
        propellers{i}.geom.incidence = geomIn.incidence(i);
        propellers{i}.plotting.P = coordsPropRot;
        
        %%% 2. Motors
        % Distance along propeller axis from prop center to front of motor
        d_mot = geomIn.xmot_over_Dprop(i)*geomIn.D(i);
        
        % CG location in propeller reference frame
        coordsCenterMot = [d_mot + geomIn.l_mot(i)/2, 0, 0]';
        
        % CG: Rotate and translate coordinates to account for incidence 
        % angle and propeller location
        coordsCenterMotRot(1) = coordsCenterProp(1) + ...
                            coordsCenterMot(1)*cosd(geomIn.incidence(i)) + ...
                            coordsCenterMot(3)*sind(geomIn.incidence(i));
        coordsCenterMotRot(2) = coordsCenterProp(2) + coordsCenterMot(2);
        coordsCenterMotRot(3) = coordsCenterProp(3) - ...
                            coordsCenterMot(1)*sind(geomIn.incidence(i)) + ...
                            coordsCenterMot(3)*cosd(geomIn.incidence(i));
        
        % Cylinder representing motor in propeller reference frame (:,:,1)
        % = front cover, (:,:,2) = back cover
        coordsMot(:,:,1) = [d_mot*ones(1,length(theta));
                            cos(theta)*geomIn.D_mot(i)/2;
                            sin(theta)*geomIn.D_mot(i)/2];
        coordsMot(:,:,2) = [(d_mot+geomIn.l_mot(i))*ones(1,length(theta));
                            cos(theta)*geomIn.D_mot(i)/2;
                            sin(theta)*geomIn.D_mot(i)/2];                     
                                             
        % Cylindrical surface: rotate and translate coordinates to account 
        % for incidence angle and propeller location:               
        coordsMotRot(1,:,:) = coordsCenterProp(1) + ...
                            coordsMot(1,:,:)*cosd(geomIn.incidence(i)) + ...
                            coordsMot(3,:,:)*sind(geomIn.incidence(i));
        coordsMotRot(2,:,:) = coordsCenterProp(2) + coordsMot(2,:,:);
        coordsMotRot(3,:,:) = coordsCenterProp(3) - ...
                            coordsMot(1,:,:)*sind(geomIn.incidence(i)) + ...
                            coordsMot(3,:,:)*cosd(geomIn.incidence(i));
                        
        % Store in output cell array
        motors{i}.weights.m = weightsIn.m_per_mot(i);
        motors{i}.weights.coords_loc = coordsCenterMotRot';
        motors{i}.geom.D_mot = geomIn.D_mot(i);
        motors{i}.geom.l_mot = geomIn.l_mot(i);
        motors{i}.geom.incidence = geomIn.incidence(i);
        motors{i}.plotting.P = coordsMotRot;
        
        %%% 3. Nacelles
        if propulsionGroup.geom.hasNacelles == 1
                      
            % Get nacelle contour. Nacelle assumed axisymmetric
            controlPoints = [0      0       +0.3    1       1;    % x/l
                             0      +0.02   +0.3    +0.02   0;   % y/D
                             0      0       0       0       0];     % z/D
                         
            % Create bezier curve
            curve = CreateBezierCurve(controlPoints,propulsionGroup.settings.nPoints,0);
            
            % Scale to nacelle size
            D_nac = geomIn.Dnac_over_Dmot(i)*geomIn.D_mot(i);
            l_nac = geomIn.lnac_over_Dnac(i)*D_nac;
            curve(1,:) = curve(1,:)/max(curve(1,:))*l_nac;
            curve(2,:) = curve(2,:)/max(curve(2,:))*D_nac/2;
  
            % Distance along propeller axis from prop center to front of
            % nacelle
            d_nac = geomIn.xnac_over_lnac(i)*l_nac;
            
            % CG location in propeller reference frame
            coordsCenterNac = [d_nac + l_nac/2, 0, 0]';
            
            % CG: Rotate and translate coordinates to account for incidence
            % angle and propeller location
            coordsCenterNacRot(1) = coordsCenterProp(1) + ...
                                coordsCenterNac(1)*cosd(geomIn.incidence(i)) + ...
                                coordsCenterNac(3)*sind(geomIn.incidence(i));
            coordsCenterNacRot(2) = coordsCenterProp(2) + coordsCenterNac(2);
            coordsCenterNacRot(3) = coordsCenterProp(3) - ...
                                coordsCenterNac(1)*sind(geomIn.incidence(i)) + ...
                                coordsCenterNac(3)*cosd(geomIn.incidence(i));
            
            % Determine nacelle radius on x-grid with cosine spacing
            x_nac = l_nac*(1 - cos(linspace(0,1,propulsionGroup.settings.nPoints)*pi))/2;
            r_nac = interp1(curve(1,:),curve(2,:),x_nac,'linear');

            % Create axisymmetric nacelle surface (axial stations along 3rd
            % dimension)
            coordsNac = NaN(3,length(theta),length(x_nac));
            for j = 1:length(x_nac)
                coordsNac(1,:,j) = x_nac(j)*ones(size(theta)) + d_nac;
                coordsNac(2,:,j) = r_nac(j)*cos(theta);
                coordsNac(3,:,j) = r_nac(j)*sin(theta);
            end
            
            % Nacelle surface: Rotate and translate coordinates to account
            % for incidence angle and propeller location:
            coordsNacRot(1,:,:) = coordsCenterProp(1) + ...
                                coordsNac(1,:,:)*cosd(geomIn.incidence(i)) + ...
                                coordsNac(3,:,:)*sind(geomIn.incidence(i));
            coordsNacRot(2,:,:) = coordsCenterProp(2) + coordsNac(2,:,:);
            coordsNacRot(3,:,:) = coordsCenterProp(3) - ...
                                coordsNac(1,:,:)*sind(geomIn.incidence(i)) + ...
                                coordsNac(3,:,:)*cosd(geomIn.incidence(i));
            
            % Nacelle surface area
            nacelles{i}.geom.S_ext = trapz(x_nac,2*pi*r_nac);
            nacelles{i}.geom.S_wet = nacelles{i}.geom.S_ext*(1 - geomIn.sFraction(i));
            
            % Store in output cell array
            nacelles{i}.weights.m = weightsIn.m_per_nac(i);
            nacelles{i}.weights.coords_loc = coordsCenterNacRot';
            nacelles{i}.geom.D_nac = D_nac;
            nacelles{i}.geom.l_nac = l_nac;
            nacelles{i}.geom.incidence = geomIn.incidence(i);
            nacelles{i}.plotting.P = coordsNacRot;
        end
    end
        
    % Store results in propulsion-group structure
    propulsionGroup.comps.propellers = propellers;
    propulsionGroup.comps.motors = motors;
    propulsionGroup.geom.S_ext = 0;
    propulsionGroup.geom.S_wet = 0;
    if propulsionGroup.geom.hasNacelles == 1
        propulsionGroup.comps.nacelles = nacelles;
        
        % Add total external/wetted areas of nacelles
        for i = 1:N
            propulsionGroup.geom.S_ext = propulsionGroup.geom.S_ext + nacelles{i}.geom.S_ext;
            propulsionGroup.geom.S_wet = propulsionGroup.geom.S_wet + nacelles{i}.geom.S_wet;
        end
    end
   
       
%% Fuselage-mounted: create propeller geometry
% Note that for 'fus', the entire primary propulsion system (except 
% propeller) is represented by a single point mass, whos CG is specified in 
% the input C2 structure [to be improved]
elseif strcmp(propulsionGroup.geom.type,'fus')
    
    % Azimuthal coordinate for prop disk geometry
    theta = linspace(0,2*pi,propulsionGroup.settings.nAzimuth);
    
    % Compute propeller disk center (= CG) location (in fuselage coordinate
    % sytem)
    coordsCenterProp = [propulsionGroup.geom.x_over_l*fuselage.geom.l_fus;
                        0;
                        propulsionGroup.geom.z_over_D*fuselage.geom.D_fus];
    
    % Loop over component instances
    coordsLE = NaN(3,N);
    propellers = cell(1,N);
    motors = cell(1,N);
    if propulsionGroup.geom.hasNacelles == 1
        nacelles = cell(1,N);
    end
    for i = 1:N
        
        % Coordinates of propeller disk in prop reference frame (azimuthal
        % coordinate along second dimension)
        coordsProp = [zeros(1,length(theta));
            cos(theta)*geomIn.D(i)/2; 
            sin(theta)*geomIn.D(i)/2];
        
        % Coordinates of propeller disk, corrected for incidence angle and
        % shifted to correct position
        coordsPropRot(1,:) = coordsCenterProp(1) + ...
            coordsProp(1,:)*cosd(geomIn.incidence(i)) + ...
            coordsProp(3,:)*sind(geomIn.incidence(i));
        coordsPropRot(2,:) = coordsCenterProp(2) + coordsProp(2,:);
        coordsPropRot(3,:) = coordsCenterProp(3) - ...
            coordsProp(1,:)*sind(geomIn.incidence(i)) + ...
            coordsProp(3,:)*cosd(geomIn.incidence(i));
        
        % Component weights: nacelle also at prop location (though should be
        % zero mass!). Motors (includes GB, accessories, etc.) as specified in
        % C2 input field.
        propellers{i}.weights.m = weightsIn.m_per_prop(i);
        propellers{i}.weights.coords_loc = coordsCenterProp;
        nacelles{i}.weights.m = weightsIn.m_per_nac(i);
        nacelles{i}.weights.coords_loc = coordsCenterProp;
        motors{i}.weights.m = weightsIn.m_per_mot(i);
        motors{i}.weights.coords_loc = [weightsIn.x_over_l*fuselage.geom.l_fus;
            weightsIn.y_over_D*fuselage.geom.D_fus;
            weightsIn.z_over_D*fuselage.geom.D_fus];
        
        % To-do: create object to show motor size for fuselage-mounted case as
        % well
        
        % Store in output cell array
        if weightsIn.m_per_prop > 0
            propellers{i}.plotting.P = coordsPropRot;
        end
    end
    propulsionGroup.comps.propellers = propellers;
    propulsionGroup.comps.nacelles = nacelles;
    propulsionGroup.comps.motors = motors;
    propulsionGroup.geom.S_ext = 0;
    propulsionGroup.geom.S_wet = 0;


%% Other configurations
        
% Do nothing if no propulsion system present
elseif strcmp(propulsionGroup.geom.type,'none')
    error('Incorrect propulsionGroup type specified')    
    
% Other systems: not implemented
else
    error('Incorrect propulsionGroup type specified')    
end


%% Total propulsion group CG

% Compute CG location of each propulsor unit (copied from LEDP case)
PGnames = fieldnames(propulsionGroup.comps);
masses = NaN(1,N);
coords = NaN(3,N);
for i = 1:N
    mass_per_unit = NaN(1,length(PGnames));
    coords_per_unit = NaN(3,length(PGnames));
    
    % Loop over 2 or 3 sub-components per propulsion unit
    for k = 1:length(PGnames)
        mass_per_unit(1,k) = propulsionGroup.comps.(PGnames{k}){i}.weights.m;
        coords_per_unit(:,k) = propulsionGroup.comps.(PGnames{k}){i}.weights.coords_loc;
    end
    
    % CG of this unit
    masses(1,i) = sum(mass_per_unit);
    coords(:,i) = sum(mass_per_unit.*coords_per_unit,2)/sum(mass_per_unit);
end

% Compute group CG & mass
propulsionGroup.weights.m = sum(masses);
if sum(masses) ~= 0
    propulsionGroup.weights.coords_loc = sum(masses.*coords,2)/sum(masses);
else
    propulsionGroup.weights.coords_loc = [0 0 0]';
end


%% Plotting
if propulsionGroup.settings.plotOption == 1
    
    % Create 3D figure
    fig = figure;
    fig.Name = 'PropulsionGroup geometry';
    hold on; grid on; box on;
    xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]');
    axis equal; view(3);
    
%     % Fuselage surfaces (for reference) (add for fuselage-mounted propulsion systems)
%     for i = 1:length(fuselage.plotting.P)
%         xx = squeeze(fuselage.plotting.P{i}(1,:,:));
%         yy = squeeze(fuselage.plotting.P{i}(2,:,:));
%         zz = squeeze(fuselage.plotting.P{i}(3,:,:));
%         surf(xx,yy,zz,'facecolor',[0.5 0.5 0.5],'edgecolor','none','facealpha',0.3)
%     end
    
    % Wing surfaces (for reference)
    if strcmp(propulsionGroup.geom.type,'LEDP')
        for i = 1:length(wing.plotting.P)
            xx = squeeze(wing.plotting.P{i}(1,:,:));
            yy = squeeze(wing.plotting.P{i}(2,:,:));
            zz = squeeze(wing.plotting.P{i}(3,:,:));
            surf(xx,yy,zz,'facecolor',[0.5 0.5 0.5],'edgecolor','none','facealpha',0.3)
        end
        
        plot3(coordsLE(1,:),coordsLE(2,:),coordsLE(3,:),'.-r')
    end
    
    % Add propeller disks & CG location
    for i = 1:geomIn.N
        fill3(propellers{i}.plotting.P(1,:),propellers{i}.plotting.P(2,:),...
              propellers{i}.plotting.P(3,:),[0.5 0.5 0.5],'facealpha',0.2,'edgecolor','g');
        plot3(propellers{i}.weights.coords_loc(1),propellers{i}.weights.coords_loc(2),...
              propellers{i}.weights.coords_loc(3),'o','markersize',3,'markeredgecolor','g',...
              'markerfacecolor',[1 1 1])
    end
    
    % Add motors & CG location
    for i = 1:geomIn.N
        xx = squeeze(motors{i}.plotting.P(1,:,:));
        yy = squeeze(motors{i}.plotting.P(2,:,:));
        zz = squeeze(motors{i}.plotting.P(3,:,:));
        surf(xx,yy,zz,'facecolor',[0.2 0.2 0.8],'edgecolor','none','facealpha',0.5)
        fill3(motors{i}.plotting.P(1,:,1),motors{i}.plotting.P(2,:,1),...
              motors{i}.plotting.P(3,:,1),[0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');
        fill3(motors{i}.plotting.P(1,:,2),motors{i}.plotting.P(2,:,2),...
              motors{i}.plotting.P(3,:,2),[0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');          
        plot3(motors{i}.weights.coords_loc(1),motors{i}.weights.coords_loc(2),...
              motors{i}.weights.coords_loc(3),'o','markersize',3,'markeredgecolor','b',...
              'markerfacecolor',[1 1 1])
    end
    
    % Add nacelles & CG location
    if propulsionGroup.geom.hasNacelles == 1
        for i = 1:geomIn.N
            xx = squeeze(nacelles{i}.plotting.P(1,:,:));
            yy = squeeze(nacelles{i}.plotting.P(2,:,:));
            zz = squeeze(nacelles{i}.plotting.P(3,:,:));
            surf(xx,yy,zz,'facecolor',[0.8 0.2 0.2],'edgecolor','none','facealpha',0.3)
            plot3(nacelles{i}.weights.coords_loc(1),nacelles{i}.weights.coords_loc(2),...
                nacelles{i}.weights.coords_loc(3),'o','markersize',3,'markeredgecolor','r',...
                'markerfacecolor',[1 1 1])
        end
    end
    
    % Add propulsion group CG location
    plot3(propulsionGroup.weights.coords_loc(1),propulsionGroup.weights.coords_loc(2),...
            propulsionGroup.weights.coords_loc(3),'o','markersize',5,'markeredgecolor','k',...
            'markerfacecolor',[1 1 1])
end











