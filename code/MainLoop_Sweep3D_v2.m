%%% Description
%
% For the code description refer to Main.m. This script simply evaluates
% Main.m in a loop to obtain parameter sweeps. The evaluation can be done
% in a 3D space, i.e. it can evaluate a grid along one, two, or three
% dimensions. There is an option to optimize cruise altitude for each point
% (optimum = minimum mission energy consumption in that case).
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 16-04-18
%%% Last modified: 10-07-24


%% Input settings

% Initialize code
clc
clear 
close all
tic
warning on
addpath('Surrogate models and data') 
addpath('Input files') 

% Iterate to obtain optimum altitude?
OptimizeAltitude = 1;

% Min/max altitude bounds [m]
hBounds = [20000 30000]*0.3048;

% Max altitude reduction in case cruise altitude not reached [m]
dh = 1000*0.3048;

% Order of polynomial fit for altitude optimum 
polyOrder = 4;

% Number of altitudes sampled to generate fit
nFit = 5;

% Parameters to be swept
Para1Name = 'MA_in.N_pax';      Para1Values = [65 70 75];
Para2Name = 'MA_in.R';          Para2Values = [1400 1500]*1e3;
Para3Name = 'a.AR';             Para3Values = 12;
SaveFn = 'TEST';
saveData = 0;


%% Main loop

% Number of cases to be evaluated
n1 = length(Para1Values);
n2 = length(Para2Values);
n3 = length(Para3Values);
n = n1*n2*n3;

% Initialize loop counters
counter = 0;
Store.ErrorLog = cell(n1,n2,n3);

% Loop over array elements. i, j and k are the indices of sweep1, sweep2
% and sweep3 respectively
for ii = 1:n1
    for jj = 1:n2
        for kk = 1:n3
            
            % Update loop counter
            counter = counter + 1;
            
            % Display progress
            if counter == 1
                disp(['> Starting run ' num2str(counter) ' of '...
                    num2str(n) '. Run time: ' num2str(toc,'%.1f') ...
                    ' seconds. It is now ' datestr(clock)])
            else
                Tused = toc;
                deltaT = Tused/(counter-1);
                SecsLeft = deltaT*(n-counter+1);
                disp(['> Starting run ' num2str(counter) ' of ' num2str(n) ...
                    '. Run time: ' num2str(toc,'%.1f') ...
                    ' seconds. Estimated time remaining: '...
                    datestr(seconds(SecsLeft),'HH:MM:SS')])
            end
            
            % Clear previously generated data
            clearvars -except ii jj kk Store counter...
                            n1 n2 n3 n Para1Name Para2Name Para3Name...
                            Para1Values Para2Values Para3Values SaveFn...
                            polyOrder nFit OptimizeAltitude hBounds dh saveData
    
            % Reset default input [ADAPT ACCORDINGLY] 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            InputReferenceAircraft;
            InputReferenceAircraft_ClassII;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Update program settings values
            s.plotWPWS = 0;
            s.plotMA = 0;
            s.plotPowertrain = 0;
            s.plotTc = 0;
            s.Polar.plot = 0;
            s.Env.plot = 0;
            s.LandingCheck.plot = 0;
            s.levelString = ['  ' s.levelString];
            s.SaveFn = SaveFn;
            s.Save = 0;
            s.SavePath = 'Results'; 
            s.figs = gobjects(1);
            
            % Assign parameters: Parameter 1 
            % Manual modification of propulsive efficiency for all segments
            if strcmp(Para1Name,'etapFactor')    
                p.cr.etap1 = p.cr.etap1*Para1Values(ii);
                p.cr.etap2 = p.cr.etap2*Para1Values(ii);
                p.cI.etap1 = p.cI.etap1*Para1Values(ii);
                p.cI.etap2 = p.cI.etap2*Para1Values(ii);
                p.L.etap1 = p.L.etap1*Para1Values(ii);
                p.L.etap2 = p.L.etap2*Para1Values(ii);
                p.TO.etap1 = p.TO.etap1*Para1Values(ii);
                p.TO.etap2 = p.TO.etap2*Para1Values(ii);
                p.ss.etap1 = p.ss.etap1*Para1Values(ii);
                p.ss.etap2 = p.ss.etap2*Para1Values(ii);

            % Fractional change in L/D, through a reduction in drag (again,
            % effect during landing NOT considered
            elseif strcmp(Para1Name,'LDFactor')    
                a.de_L = a.de_L + a.e_clean*(1-Para1Values(ii));
                a.de_TO = a.de_TO*Para1Values(ii);
                a.e_clean = a.e_clean*Para1Values(ii);
                a.dCD0_L = a.dCD0_L + (a.CD0_clean + a.dCD0_LG)*(1-1/Para1Values(ii));
                a.dCD0_TO = a.dCD0_TO/Para1Values(ii);
                a.dCD0_LG = a.dCD0_LG/Para1Values(ii);
                a.CD0_clean = a.CD0_clean/Para1Values(ii);
                
            % OEM ratio: update anonymous function 
            elseif strcmp(Para1Name,'OEMratio')    
                f.W.OEM_TP = @(MTOM)MTOM*Para1Values(ii);
                f.W.OEM_TF = @(MTOM)MTOM*Para1Values(ii);
                
            % Landing distance requirement: update associated stall speed
            elseif strcmp(Para1Name,'m.sL.s')  
                m.sL.s = Para1Values(ii);
                m.sL.vs = (m.sL.s/0.584)^0.5;         
                
            % Number of pax: update payload mass (CHECK MASS PER PAX)!
            elseif strcmp(Para1Name,'MA_in.N_pax')  
                mass_per_pax = MA_in.PL/MA_in.N_pax;
                MA_in.N_pax = Para1Values(ii);
                MA_in.PL = MA_in.N_pax*mass_per_pax;  
                C2.payload.weights.m = MA_in.PL;                
                C2.fuselage.geom.NoPassengers = MA_in.N_pax;    
                
            % For a generic variable, evaluate normally
            else
                eval([Para1Name '= Para1Values(ii);'])
            end
            
            % Assign parameters: Parameter 2
            % For the "EtaCorrection" case (only applicable to Parameter2),
            % correct all electrical components by this factor. For 0, the
            % electrical components efficiency is equal to the input file;
            % for 1, it is equal to 100% efficiency.
            if strcmp(Para2Name,'EtaCorrection')
                p.eta_EM1 = p.eta_EM1 + Para2Values(jj)*(1-p.eta_EM1);
                p.eta_EM2 = p.eta_EM2 + Para2Values(jj)*(1-p.eta_EM2);
                p.eta_PM = p.eta_PM + Para2Values(jj)*(1-p.eta_PM);
                
            % For "SuppliedRatio" case, apply supplied power ratio to all mission
            % segments and constraints (except landing)
            elseif strcmp(Para2Name,'SuppliedRatio')
                m.cr.phi = Para2Values(jj);
                m.L.phi = Para2Values(jj);
                m.TO.phi = Para2Values(jj); 
                m.bLAEO.phi = Para2Values(jj); 
                m.ToClAEO.phi = Para2Values(jj); 
                m.ToClEx.phi = Para2Values(jj); 
                m.ToClRe.phi = Para2Values(jj); 
                m.erOEI.phi = Para2Values(jj); 
                m.da.phi = Para2Values(jj); 
                m.rocAEO.phi = Para2Values(jj); 
                m.rocOEI.phi = Para2Values(jj); 
                MA_in.cl.phi = [Para2Values(jj) Para2Values(jj)];
                MA_in.cr.phi = [Para2Values(jj) Para2Values(jj)];
                MA_in.de.phi = [Para2Values(jj) Para2Values(jj)];
                MA_in.Dcl.phi = [Para2Values(jj) Para2Values(jj)];
                MA_in.Dcr.phi = [Para2Values(jj) Para2Values(jj)];
                MA_in.Dde.phi = [Para2Values(jj) Para2Values(jj)];
                MA_in.Loiter.phi = [Para2Values(jj) Para2Values(jj)];
                
%             % For "take-off distance" case, recompute stall speed in
%             % landing conditions for same runway length
%             elseif strcmp(Para2Name,'m.TO.s')    
%                 eval([Para2Name '= Para2Values(jj);'])
%                 m.L.vs = (m.TO.s/0.584)^0.5; 
%                 
%             elseif strcmp(Para2Name,'p.SE.bat')
%                 eval([Para2Name '= Para2Values(jj);'])
%                 p.SP.bat = p.SE.bat/3600*2; % HARD CODED C-RATE OF 2!

            % For a generic variable, evaluate normally
            else
                eval([Para2Name '= Para2Values(jj);'])
            end
            
            % Assign parameters: Parameter 3
            eval([Para3Name '= Para3Values(kk);'])              
                        
            % Update electrical machine weight correlation function
            f.W.EM = @(P) P/p.SP.EM;

            % Change initial guess of MA to increase convergence speed
            if counter > 1
                if Store.OEM > 0; MA_in.OEM = Store.OEM; end
                if Store.FF_tot > 0; MA_in.FF_tot0 = Store.FF_tot; end
                if Store.FF_miss > 0; MA_in.FF_miss0 = Store.FF_miss; end
                if Store.DOH_tot > 0; MA_in.DOH_tot0 = Store.DOH_tot; end
                if Store.DOH_miss > 0; MA_in.DOH_miss0 = Store.DOH_miss;end
            end
    
            % If an error is returned, ignore and save NaN's + warning 
            try
                
                % Evaluate once if altitude is fixed
                if OptimizeAltitude ~= 1
                    MainCalledInLoop;
                    
                % Optimize cruise altitude if required 
                else
                    
                    % Display start of altitude optimization
                    disp([s.levelString '> Creating altitude fit'])
                    s.levelString = ['  ' s.levelString];
                    
                    % Evaluate different heights (in decreasing order)
                    hArray = linspace(hBounds(2),hBounds(1),nFit);
                    hPREE = NaN(size(hArray));
                    xManual0_outer = s.xManual;
                    idx = 1;
                    while idx <= nFit 
                        
                        % Assign altitude, reset xManual in case it changed
                        % in the last C2 convergence loop
                        m.cr.h = hArray(idx);
                        s.xManual = xManual0_outer;

                        % Evaluate this altitude; if it is too high for the
                        % AC to converge; reduce altitude 
                        try
                            
                            % Call sizing function
                            disp([s.levelString '> Evaluating altitude '...
                                num2str(idx) ' of ' num2str(nFit)])
                            MainCalledInLoop
                            
                            % Compute minimization criterion: PREE
                            if isnan(M.bat_miss)
                                hE_miss = 0 + M.f_miss*p.SE.f;
                            else
                                hE_miss = M.bat_miss*p.SE.bat + M.f_miss*p.SE.f;
                            end
                            hPREE(idx) = MA_in.R*MA_in.PL*9.81/hE_miss;
                            
                        % Check what sort of error was returned    
                        catch hError
                            
                            % If nominal climb/descent required more than
                            % nominal mission range, reduce maximum
                            % altitude evaluated. Also set this as the new
                            % "upper" limit for the optimum
                            if strcmp(hError.message,...
                                'Climb and descent require more than the specified nominal mission range')
                                    
                                % Reset altitude query array
                                hArray = linspace(hArray(1)-dh,hBounds(1),nFit);   
                                  
                                % Set counter to 1 for next loop
                                idx = 0;
                                disp([s.levelString '> Climb/descent take too long, reducing max altitude'])
                            
                                % Give error if new max altitude is lower
                                % than lower bound
                                if hArray(1) <= hBounds(1)
                                    error('Maximum altitude is lower than minimum altitude')
                                end
                                
                            % If a different error is captured, pass on to 
                            % outer try-catch and continue
                            else
                                error(hError.message)
                            end
                        end

                        % Update counter
                        idx = idx + 1;
                    end
                    
                    % Scale altitude array
                    xScaled = (hArray-mean(hArray))/std(hArray);
                    
                    % Create polyfit
                    pp = polyfit(xScaled,hPREE,polyOrder);
                    
                    % Evaluate fit on a fine grid 
                    hFit = linspace(min(hArray),max(hArray),1000);
                    xFitScaled = (hFit-mean(hArray))/std(hArray);
                    hPREEFit = polyval(pp,xFitScaled);
                    
                    % Determine optimum altitude
                    [PREEMax,idxMax] = max(hPREEFit);
                    hOpt = hFit(idxMax);
                    
                    % Save data related to optimum altitude
                    Store.hOpt(ii,jj,kk) = hOpt;
                    Store.hOptFit{ii,jj,kk}.hArray = hArray;
                    Store.hOptFit{ii,jj,kk}.hPREE = hPREE;
                    Store.hOptFit{ii,jj,kk}.hFit = hFit;
                    Store.hOptFit{ii,jj,kk}.hPREEFit = hPREEFit;
                    
                    % Assign new altitude, reset xManual again in case
                    % changed in C2 convergence loop
                    s.xManual = xManual0_outer;
                    m.cr.h = hOpt;
                    
                    % Call sizing function for optimum altitude
                    disp([s.levelString '> Evaluating optimum altitude'])
                    MainCalledInLoop

                    % Plot for checking altitude fit
                    %{
                    figure(1000+ii)
                    hold on; grid on; box on;
                    plot(hArray,hPREE,'or')
                    plot(hFit,hPREEFit,'-b')
                    if isnan(M.bat_miss); aux = 0 + M.f_miss*p.SE.f;
                    else; aux = M.bat_miss*p.SE.bat + M.f_miss*p.SE.f; end
                    PlotPREE = MA_in.R*MA_in.PL*9.81/aux;
                    plot(hOpt,PlotPREE,'xg')
                    legend('Evaluated','Fitted','Optimum')
                    drawnow
                    %}
                end
                
                % Generate additional plots if desired
                %{
                % 0. Show constraint diagrams
                if s.plotWPWS == 1
                    if s.printMessages == 1
                        disp([s.levelString '> Generating constraint-diagram plots'])
                    end
                    [~,~,~,~,~,~,~,~,s.figs] = ComputeDesignPoint(WS,WP_select,TW,a,m,p,f,s,c);
                end

                % I. Create mission-analysis profiles
                if s.plotMA == 1
                    if s.printMessages == 1
                        disp([s.levelString '> Generating mission-analysis plots'])
                    end
                    [s] = PlotMissionAnalysis(p,m,s,c,aircraft,M,MA,WSdes,WPdes,WS,WP_path);
                end
                
                % II. Powertrain plots
                if s.plotPowertrain == 1
                    disp([s.levelString '> Generating powertrain plots'])
                    P_path.cr = structfun(@(x) 1./x/m.cr.f*M.TOM*c.g/1e6,...
                        WP_path.cr,'UniformOutput',0);
                    P_path.TO = structfun(@(x) 1./x/m.TO.f*M.TOM*c.g/1e6,...
                        WP_path.TO,'UniformOutput',0);
                    P_path.L = structfun(@(x) 1./x/m.L.f*M.TOM*c.g/1e6,...
                        WP_path.L,'UniformOutput',0);
                    [s.figs(end+1)] = PlotPowertrain(P_path.cr,WS.cr,...
                        WSdes.(s.SelDes),s.figStart+size(s.figs,2),'%6.2f',...
                        'Cruise constraint [MW]');
                    [s.figs(end+1)] = PlotPowertrain(P_path.TO,WS.TO,...
                        WSdes.(s.SelDes),s.figStart+size(s.figs,2),'%6.2f',...
                        'Take-off constraint [MW]');
                    [s.figs(end+1)] = PlotPowertrain(P_path.L,NaN,...
                        NaN,s.figStart+size(s.figs,2),'%6.2f',...
                        'Landing constraint [MW]');
                    clear('P_path')
                end
                
                % III. Aerodynamic polar in cruise conditions
                if s.Polar.plot == 1
                    disp([s.levelString '> Generating cruise polar'])
                    [~,~,~,~,~] = CreateLiftDragPolarMap(WSdes.(s.SelDes),'cr',a,p,f,s,1);
                end
                
                % IV. Power-control envelopes
                if s.Env.plot == 1
                    disp([s.levelString '> Generating power-control envelope(s)'])
                    [s] = CreatePowerControlEnvelope(p,s,f,c,WPdes,AEROdes,MA,M);
                end
                
                % V. Add drag requirements to stall constraint
                if s.LandingCheck.plot == 1
                    disp([s.levelString '> Adding landing drag requirements'])
                    [s,AEROdes] = CreateLandingDragRequirements_v2(a,m,p,f,s,c,WSdes,WPdes,AEROdes);
                end

                % VI. Plot overall efficiencies
                if s.plotOverallEfficiencies == 1
                    if s.printMessages == 1
                        disp([s.levelString '> Plotting overall efficiencies'])
                    end
                    [s] = PlotOverallEfficiencies(a,m,p,f,s,c,MA);
                end
                
                % VII. Class-II aero
                if s.WeightEstimationClass == 2
                    [~,~,s] = ComputeClassIIAero(a,m,p,f,s,C2,aircraft,MA);
                end
                
                % VIII. Aircraft geometry
                if s.WeightEstimationClass == 2
                    if C2.settings.plotAircraft == 1
                        [~] = ConfigurationAndGeometry_v2(p,C2,aircraft);
                    end
                end
                %}
                        
                % Check warnings: Installed PT component power exceeded in
                % mission segment x. Mission segments given in the variable
                % "names".
                PavailExceeded = NaN(1,length(names));
                for x = 1:length(names)
                    if any(MA.(names{x}).limits > 0)
                        PavailExceeded(x) = 1;
                    else
                        PavailExceeded(x) = 0;
                    end
                end
                
            % If the code crashed, provide NaNs as output and issue a
            % warning
            OEM1 = M.OEM;
            errorEncountered = 0;
            catch ME
                M.f = NaN; M.f_miss = NaN; M.bat = NaN; M.bat_miss = NaN;
                M.bat_e = NaN; M.bat_p = NaN; M.EM1 = NaN; M.EM2 = NaN;
                M.GT = NaN; M.w = NaN; M.OEM = NaN; M.TOM = NaN; M.PL =NaN;
                PavailExceeded = NaN;
                MA.FF_tot = MA_in.FF_tot0;
                MA.FF_miss = MA_in.FF_miss0;
                MA.DOH_tot =MA_in.DOH_tot0;
                MA.DOH_miss = MA_in.DOH_miss0;
                OEM1 = MA_in.OEM;
                disp([s.levelString '> Code has encountered an error: ' ME.message])
                Store.ErrorLog{ii,jj,kk} = ME;
                errorEncountered = 1;
            end
                       
            % Store results
            Store.xManual(ii,jj,kk) = s.xManual;
            Store.M{ii,jj,kk} = M;
            Store.PavailExceeded{ii,jj,kk} = PavailExceeded;
            Store.MA_in{ii,jj,kk} = MA_in;
            Store.aircraft{ii,jj,kk} = aircraft;
            if errorEncountered == 1
                Store.WSdes(ii,jj,kk) = NaN;
                Store.WPdes{ii,jj,kk} = NaN;
                Store.hCr(ii,jj,kk) = NaN;
            else
                Store.WSdes(ii,jj,kk) = WSdes.(s.SelDes);
                Store.WPdes{ii,jj,kk} = WPdes.(s.SelDes);
                Store.hCr(ii,jj,kk) = m.cr.h;
            end
                                               
            % Store aerodynamic characteristics
            if errorEncountered == 1
                Store.AvgLDCruise(ii,jj,kk) = NaN;
                Store.AvgEtapCruise(ii,jj,kk) = NaN;
                Store.AvgCLCruise(ii,jj,kk) = NaN;
                Store.AvgLD(ii,jj,kk) = NaN;
                Store.AvgEtap(ii,jj,kk) = NaN;
                Store.AvgCL(ii,jj,kk) = NaN;
            else
                Store.AvgLDCruise(ii,jj,kk) = nanmean(MA.cr.aero.LD);
                Store.AvgEtapCruise(ii,jj,kk) = nanmean(MA.cr.aero.etapAvg);
                Store.AvgCLCruise(ii,jj,kk) = nanmean(MA.cr.aero.CL);
                Store.AvgLD(ii,jj,kk) = (nanmean(MA.cl.aero.LD)*(MA.cl.t(end)-MA.cl.t(1)) + ...
                    nanmean(MA.cr.aero.LD)*(MA.cr.t(end)-MA.cr.t(1)) + ...
                    nanmean(MA.de.aero.LD)*(MA.de.t(end)-MA.de.t(1))) /...
                    (MA.de.t(end)-MA.cl.t(1));
                Store.AvgEtap(ii,jj,kk) = (nanmean(MA.cl.aero.etapAvg)*(MA.cl.t(end)-MA.cl.t(1)) + ...
                    nanmean(MA.cr.aero.etapAvg)*(MA.cr.t(end)-MA.cr.t(1)) + ...
                    nanmean(MA.de.aero.etapAvg)*(MA.de.t(end)-MA.de.t(1))) /...
                    (MA.de.t(end)-MA.cl.t(1));
                Store.AvgCL(ii,jj,kk) = (nanmean(MA.cl.aero.CL)*(MA.cl.t(end)-MA.cl.t(1)) + ...
                    nanmean(MA.cr.aero.CL)*(MA.cr.t(end)-MA.cr.t(1)) + ...
                    nanmean(MA.de.aero.CL)*(MA.de.t(end)-MA.de.t(1))) /...
                    (MA.de.t(end)-MA.cl.t(1));
            end
            
            % Compute FoM's and store separately in array for ease. NB:
            % specific energy consumption calculated based on nominal
            % mission energy, not on total energy! PREE is also calculated
            % further up
            if errorEncountered == 1
                Store.MTOM(ii,jj,kk) = NaN;
                Store.Wh_per_pkm(ii,jj,kk) = NaN;
                Store.E_miss(ii,jj,kk) = NaN;
                Store.E_tot(ii,jj,kk) = NaN;
                Store.PREE(ii,jj,kk) = NaN;
                Store.DOH(ii,jj,kk) = NaN;
            else
                Store.MTOM(ii,jj,kk) = Store.M{ii,jj,kk}.TOM;
                Store.Wh_per_pkm(ii,jj,kk) = aircraft.Wh_per_pkm;
                if isnan(Store.M{ii,jj,kk}.bat_miss)
                    Store.E_miss(ii,jj,kk) = 0 + Store.M{ii,jj,kk}.f_miss*p.SE.f;
                    Store.E_tot(ii,jj,kk) = 0 + Store.M{ii,jj,kk}.f*p.SE.f;
                else
                    Store.E_miss(ii,jj,kk) = Store.M{ii,jj,kk}.bat_miss*p.SE.bat + ...
                        Store.M{ii,jj,kk}.f_miss*p.SE.f;
                    Store.E_tot(ii,jj,kk) = Store.M{ii,jj,kk}.bat_E*p.SE.bat*(1-p.minSOC) + ...
                        Store.M{ii,jj,kk}.f*p.SE.f; % EXCLUDES SOC MARGIN!
                end
                Store.PREE(ii,jj,kk) = Store.MA_in{ii,jj,kk}.R*...
                    Store.MA_in{ii,jj,kk}.PL*9.81/Store.E_miss(ii,jj,kk);
                Store.DOH(ii,jj,kk) = (p.SE.bat*Store.M{ii,jj,kk}.bat)/(p.SE.bat*Store.M{ii,jj,kk}.bat ...
                    + p.SE.f*Store.M{ii,jj,kk}.f);                
            end

            % Compute and store other extensive parameters
            if errorEncountered == 1
                Store.b(ii,jj,kk) = NaN;            % Wing span
                Store.Ps_tot(ii,jj,kk) = NaN;       % Total installed shaft power
                Store.OEMratioCorr(ii,jj,kk) = NaN; % OEM ratio (incl. wing/pt)
            else
                Store.b(ii,jj,kk) = (Store.MTOM(ii,jj,kk)*c.g/Store.WSdes(ii,jj,kk)*a.AR)^0.5;
                if isnan(Store.WPdes{ii,jj,kk}.s1); WPs1 = Inf; 
                else; WPs1 = Store.WPdes{ii,jj,kk}.s1; end
                if isnan(Store.WPdes{ii,jj,kk}.s2); WPs2 = Inf; 
                else; WPs2 = Store.WPdes{ii,jj,kk}.s2; end
                Store.Ps_tot(ii,jj,kk) = Store.MTOM(ii,jj,kk)*c.g*(1/WPs1 + 1/WPs2);
                Store.OEMratioCorr(ii,jj,kk) = (Store.M{ii,jj,kk}.OEM + ...
                    Store.M{ii,jj,kk}.w + Store.M{ii,jj,kk}.EM1 + ...
                    Store.M{ii,jj,kk}.EM2 + Store.M{ii,jj,kk}.GT)/...
                    Store.M{ii,jj,kk}.TOM;
            end
            
            % Keep initial guesses for next iteration in case of
            % fuel-aircraft; for battery-based aircraft this may lead to
            % invalid initial points (?)
            if electric == 1
                if errorEncountered ~= 1
                    Store.FF_tot = MA.BF_tot;
                    Store.FF_miss = MA.BF_miss;
                else
                    Store.FF_tot = MA_in.FF_tot0;
                    Store.FF_miss = MA_in.FF_miss0;
                end
            else
                if errorEncountered ~= 1
                    Store.FF_tot = MA.FF_tot;
                    Store.FF_miss = MA.FF_miss;
                else
                    Store.FF_tot = MA_in.FF_tot0;
                    Store.FF_miss = MA_in.FF_miss0;
                end
            end
            Store.DOH_tot = MA.DOH_tot;
            Store.DOH_miss = MA.DOH_miss;
            Store.OEM = OEM1;
        end
    end
end


%% Post-processing

% Display progress
disp(['> Plotting and/or saving results. Run time: ' num2str(toc/60,'%.1f') ' min.'])

% Store results in arrays for plotting. Update: these are also already
% stored in the "Store" array now.
TcExceeded = NaN(n1,n2,n3);
PavailExceeded = NaN(n1,n2,n3);
ErrorLog = NaN(n1,n2,n3);
for i = 1:n1
    for j = 1:n2
        for k = 1:n3
            if any(Store.PavailExceeded{i,j,k}>0)
                PavailExceeded(i,j,k) = 1;
            else
                PavailExceeded(i,j,k) = 0;
            end
            if isempty(Store.ErrorLog{i,j,k})
                ErrorLog(i,j,k) = 0;
            else
                ErrorLog(i,j,k) = 1;
            end
        end
    end
end

% Generate figure (2D/3D)
ns = [n1 n2 n3];
if sum(ns==1) ~= 2
    fig = figure(s.figStart + size(s.figs,2));
    fig.Name = 'Parameter sweep results';
    s.figs(size(s.figs,2)+1) = fig;
    fig.Color = [1 1 1];
    
    % Generate color array
    cols = [linspace(0,1,n3); zeros(1,n3); linspace(1,0,n3)]';
    
    % MTOM distribution
    subplot(2,2,1)
    hold on; grid on;
    xlabel(Para1Name); ylabel(Para2Name);
    for k = 1:n3
        if n1 > 1 && n2 > 1
            H1{k} = surf(Para1Values,Para2Values, Store.MTOM(:,:,k)','facecolor',...
                'interp','edgecolor',cols(k,:));
        else
            H1{k} = plot3(Para1Values,Para2Values,Store.MTOM(:,:,k)','.-',...
                'color',cols(k,:));
        end
        LString{k} = [Para3Name ' = ' num2str(Para3Values(k))];
    end
    zlabel('MTOM [kg]')
    colormap(flip(gray));
    legend([H1{:}],LString);
    
    % Total energy
    subplot(2,2,2)
    hold on; grid on;
    xlabel(Para1Name); ylabel(Para2Name);
    for k = 1:n3
        if n1 > 1 && n2 > 1
            H2{k} = surf(Para1Values,Para2Values,Store.E_miss(:,:,k)'/10^9,'facecolor',...
                'interp','edgecolor',cols(k,:));
        else
            H2{k} = plot3(Para1Values,Para2Values,Store.E_miss(:,:,k)'/10^9,'.-',...
                'color',cols(k,:));
        end
    end
    zlabel('Total Energy [GJ]')
    colormap(flip(gray));
    
    % Aircraft efficiency
    subplot(2,2,3)
    hold on; grid on;
    xlabel(Para1Name); ylabel(Para2Name);
    for k = 1:n3
        if n1 > 1 && n2 > 1
            H3{k} = surf(Para1Values,Para2Values,Store.PREE(:,:,k)','facecolor',...
                'interp','edgecolor',cols(k,:));
        else
            H3{k} = plot3(Para1Values,Para2Values,Store.PREE(:,:,k)','.-',...
                'color',cols(k,:));
        end
    end
    zlabel('PREE [-]')
    colormap(flip(gray));
    
    % Errors/warnings
    subplot(2,2,4)
    hold on; grid on;
    xlabel(Para1Name); ylabel(Para2Name); zlabel(Para3Name);
    for i = 1:n1
        for j = 1:n2
            for k = 1:n3
                if TcExceeded(i,j,k) == 1
                    plot3(Para1Values(i),Para2Values(j),Para3Values(k),'xb');
                end
                if PavailExceeded(i,j,k) == 1
                    plot3(Para1Values(i),Para2Values(j),Para3Values(k),'+g');
                end
                if ErrorLog(i,j,k) == 1
                    plot3(Para1Values(i),Para2Values(j),Para3Values(k),'or');
                end
            end
        end
    end
    h1 = plot3(NaN,NaN,NaN,'xb');
    h2 = plot3(NaN,NaN,NaN,'+g');
    h3 = plot3(NaN,NaN,NaN,'or');
    legend([h1 h2 h3],'T_c exceeded','P_{avail} exceeded','Program error')
    
% Generate figure (1D)    
else
    fig = figure(s.figStart + size(s.figs,2));
    fig.Name = 'Parameter sweep results';
    s.figs(size(s.figs,2)+1) = fig;
    fig.Color = [1 1 1];
    if n1>1
        subplot(2,2,1); hold on; grid on;
        xlabel(Para1Name); ylabel('MTOM [kg]');
        plot(Para1Values,squeeze(squeeze(Store.MTOM)),'.-r');
        subplot(2,2,2); hold on; grid on;
        xlabel(Para1Name); ylabel('Mission energy [GJ]');
        plot(Para1Values,squeeze(squeeze(Store.E_miss/10^9)),'.-r');
        subplot(2,2,3); hold on; grid on;
        xlabel(Para1Name); ylabel('PREE [-]');
        plot(Para1Values,squeeze(squeeze(Store.PREE)),'.-r');
        subplot(2,2,4); hold on; grid on;
        xlabel(Para1Name); ylabel('Error');
        for i = 1:n1
            if PavailExceeded(i,:,:) == 1; plot(Para1Values(i),1,'+g'); end
            if ErrorLog(i,:,:) == 1; plot(Para1Values(i),1,'or'); end
        end
    elseif n2>1
        subplot(2,2,1); hold on; grid on;
        xlabel(Para2Name); ylabel('MTOM [kg]');
        plot(Para2Values,squeeze(squeeze(Store.MTOM)),'.-r');
        subplot(2,2,2); hold on; grid on;
        xlabel(Para2Name); ylabel('Mission energy [GJ]');
        plot(Para2Values,squeeze(squeeze(Store.E_miss/10^9)),'.-r');
        subplot(2,2,3); hold on; grid on;
        xlabel(Para2Name); ylabel('PREE [-]');
        plot(Para2Values,squeeze(squeeze(Store.PREE)),'.-r');
        subplot(2,2,4); hold on; grid on;
        xlabel(Para2Name); ylabel('Error');
        for i = 1:n2
            if PavailExceeded(:,i,:) == 1; plot(Para2Values(i),1,'+g'); end
            if ErrorLog(:,i,:) == 1; plot(Para2Values(i),1,'or'); end
        end
    elseif n3>1
        subplot(2,2,1); hold on; grid on;
        xlabel(Para3Name); ylabel('MTOM [kg]');
        plot(Para3Values,squeeze(squeeze(Store.MTOM)),'.-r');
        subplot(2,2,2); hold on; grid on;
        xlabel(Para3Name); ylabel('Mission energy [GJ]');
        plot(Para3Values,squeeze(squeeze(Store.E_miss/10^9)),'.-r');
        subplot(2,2,3); hold on; grid on;
        xlabel(Para3Name); ylabel('PREE [-]');
        plot(Para3Values,squeeze(squeeze(Store.PREE)),'.-r');
        subplot(2,2,4); hold on; grid on;
        xlabel(Para3Name); ylabel('Error');
        for i = 1:n3
            if PavailExceeded(:,:,i) == 1; plot(Para3Values(i),1,'+g'); end
            if ErrorLog(:,:,i) == 1; plot(Para3Values(i),1,'or'); end
        end
    end
    h1 = plot3(NaN,NaN,NaN,'+g');
    h2 = plot3(NaN,NaN,NaN,'or');
    legend([h1 h2],'P_{avail} exceeded','Program error')
end


%% Save results

% Save data? Specified above, decision not take from input files
if saveData == 1
    
    % Clear loop variables (can be commented for debugging)
    clear('FF_tot0','FF_miss0','DOH_tot0','DOH_miss0','DOHmiss',...
        'FF_tot1','FF_miss1','DOH_tot1','DOH_miss1','DOHtot',...
        'electric','count','err','TOM0','TOM1','i','conv',...
        'Ebat_miss1','Ebat_tot1','EbatMiss','EbatTot',...
        'Ef_miss1','Ef_tot1','Mbat_div','Mbat_miss0','Mbat_miss1',...
        'Mbat_tot0','Mbat_tot1','Mf_div','Mf_miss0','Mf_miss1',...
        'Mf_tot0','Mf_tot1','Ncon','R_climb','R_cruise',...
        'R_Dclimb','R_Dcruise','R_descent','R_Ddescent','j','k',...
        'Ebat_div','Ebat_end','Ebat_miss0','Ebat_tot0','aa','phiStatus',...
        'PhiStatus','xiStatus','names','DOH_miss','warnings',...
        'DOH_missref','DOH_totref','Pavail','Pnames','Preq','Pstrings',...
        'rho','Sstring','Ratiobat','Ratiobat0','RatioEM1','RatioEM10',...
        'RatioEM2','RatioEM20','RatioGTM','RatioGTM0','Ratios1','Ratios10',...
        'ScalingError');
    
    % Clear additional outer-loop variables
    clear('counter','Etot','ii','jj','kk','n','n1','n2','n3','OEM1',...
          'PavailExceeded','TcExceeded','x','h1','h2','h3','cols',...
          'fig','H1','H2','H3','LString','MTOM','OverallEta','ErrorLog')


    % Hide figures
    for ii = 1:length(s.figs)
        if isgraphics(s.figs(ii))
            set(s.figs(ii),'Visible','Off')
        end
    end
    clear('ii')
    
    % Generate .mat file with figures hidden
    save([s.SavePath '\Sweep_' s.SaveFn])
    
    % Re-show figures
    for ii = 1:length(s.figs)
        if isgraphics(s.figs(ii))
            set(s.figs(ii),'Visible','On')
        end
    end
    clear('ii')
end

% End
disp([s.levelString '> Completed on ' datestr(clock) '. Total run time: ' ...
    num2str(toc,'%.1f') ' seconds.'])


