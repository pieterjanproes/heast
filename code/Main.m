%%% Description
%
% This script combines the wing-loading power-loading diagrams and mission
% analysis to carry out the complete "Class-1.5" sizing of an HEP aircraft
% including aero-propulsive interaction effects. A description of the
% method can be found in the paper by de Vries, Brown and Vos (2018). The
% way in which aero-propulsive interaction effects are accounted for is
% briefly described in "WP_WS_diagram.m". The aero-propulsive models are
% specified in "WingPropDeltas.m". The powertrain model is described in
% "PowerTransmissionComputation_v3". The input parameters of the code are 
% described and specified in "Input.m". 
%
% The user is strongly recommended to monitor the messages displayed in the
% command window, since important warnings may be shown and input data may
% be modified for consistency. For a "properly" sized aircraft, with the
% correct input settings, no warnings should appear. Only ignore these
% messages if you understand them and are sure they do not lead to 
% erroneous results. 
% 
% After running, the following variables (structures) should appear in the 
% workspace:
%
%   - a: contains aerodynamic properties such as the assumed CLmax or CD0
%       in the different flight conditions evaluated in the WS-WP diagram,
%       as well as wing geometries such as AR or TR. Defined by user in
%       "Input.m"
%   - AC: indicates which constraint is active/limiting for each powertrain
%       component's power loading, as well as wing loading, depending on
%       the design criteria chosen. For example, "AC.minGT.bat" gives the
%       name of the constraint which limits the minimum battery size when
%       selecting the design wing-loading corresponding to minimum gas
%       turbine size. AC is computed in "ComputeDesignPoint.m".
%   - AEROdes: provides the aerodynamic (such as CL, L/D,...) and
%       operational (such as v, M,...) properties of the aircraft in a
%       determined flight condition (= constraint) for a given design
%       point. For example, "AEROdes.minWS.cr.v" returns the velocity
%       required during the cruise constraint at the design wing loading
%       corresponding to minimum wing area (maximum W/S). AEROdes is
%       obtained in "ComputeDesignPoint.m".
%   - aircraft: structure summarizing basic top-level results. The
%       degree-of-hybridization included in "aircraft" is the total
%       installed DOH, so the available battery energy is considered, and
%       not the one used during the actual mission (e.g. if the battery is
%       sized by power constraint, part of the energy is never used). FOr
%       Class-II sizing, it also contains all the detailed geometrical
%       information and CG info per component.
%   - c: constants specified by the user in "Input.m", such as gravity or
%       sea-level atmospheric density.
%   - f: contains anonymous functions such as the power lapse of the
%       engine, atmospheric density lapse, or the weight of a powertrain
%       component as a function of installed power. Specified by user in
%       "Input.m".
%   - m: specifies operational/mission parameters, divided into flight
%       conditions (= constraints). For exmaple, "m.TO.h" is the altitude
%       at which take-off is performed. Some variables are specified by the
%       user in "Input.m"; others are computed along the way and added to
%       the structure.
%   - M: mass breakdown of the aircraft, as obtained from the mission
%       analysis. It is obtained from the "ComputeWeights.m" function. It
%       contains the following fields (all in [kg]):
%           - f: total fuel mass on board
%           - f_miss: fuel mass used during nominal mission (taxi + TO +
%               climb + cruise + descent + landing + taxi; no loiter or
%               diversion)
%           - bat: total battery mass on board. It is equal to the most
%               limiting case between bat_E and bat_P
%           - bat_miss: battery mass required to provide the energy
%               consumed during the nominal mission (taxi + TO + climb + 
%               cruise + descent + landing + taxi; no loiter or diversion,
%               no SOC margin.
%           - bat_E: battery mass that would be required to satisfy energy
%               requirements, including a SOC margin.
%           - bat_P: battery mass that would be required to satisfy the
%               power requirements
%           - EM1: mass of all primary electrical machines (combined) 
%           - EM2: mass of all secondary electrical machines (combined) 
%           - GT: mass of all gas turbines (combined) 
%           - w: wing mass
%           - OEM: operative empty mass EXCLUDING wing and powertrain (so
%               also no battery mass)
%           - TOM: total take-off mass
%           - PL: payload mass
%   - MA: collects results of the mission analysis ("MissionAnalysis.m",
%       unsuprisingly). One field exists per mission segment. For each
%       mission segment, several parameters are specified as an array, with
%       each element of the array corresponding to a timestep along the
%       mission segment. For example, "MA.Dcl.Ef" refers to the fuel energy
%       remaining on the aircraft (as a function of time) during the climb
%       phase of the diversion mission. MA also collects the resulting fuel
%       fractions and degree-of-hybridization of the aircraft.
%   - MA_in: input parameters for the mission analysis specified by the
%       user in "Input.m", such as Range, diversion altitude, and initial
%       guesses for OEM or DOH. Additional parameters are added per mission
%       segment throughout "MissionAnalysis.m".
%   - p: powertrain parameters specified by user in "Input.m", including
%       several which are flight-condition dependent. For example,
%       "p.L.etap1" refers to the propulsive efficiency of the primary
%       propulsors in landing conditions.
%   - s: program settings, such as convergence tolerances, figure numbers,
%       etc. Specified by user in "Input.m". The handles of the figures
%       generated are added to "s.figs".
%   - TW: Thrust-to-weight ratio of the different constraints evaluated in
%       the WP-WS diagram. Created in the different constraint functions
%       called in "WP_WS_diagram.m". The wing loading values at which the
%       TW arrays are specified are given by the structure "WS".
%   - TW_WSdes: Thrust-to-weight ratio evaluated at the different design
%       wing-loadings (specified in "WSdes") for each constraint. For
%       example, "TW_WSdes.minp.cr" refers to the thrust-to-weight ratio
%       obtained during cruise when selecting the wing loading
%       corresponding to minimum propulsive power as design point. TW_WSdes
%       is computed in "ComputeDesignPoint.m".
%   - WP_comp: contains the sizing power-loading values of each component 
%       of the powertrain, for each constraint and as a function of "WS".
%       For example, "WP_comp.cr.GB" gives, as a function of wing loading,
%       the sizing power-loading value (i.e. the maximum value the
%       component has to be able to produce/absorb) of the gearbox in
%       cruise conditions. This structure is generated as output of the
%       constraint functions in "WP_WS_diagram.m".
%   - WP_loss: contains the power-loading losses of each component 
%       of the powertrain, for each constraint and as a function of "WS".
%       For example, "WP_loss.cr.GB" gives, as a function of wing loading,
%       the amount of power lost due to e.g. heat dissipation (expressed
%       as a power-lodaing) in the gearbox in cruise conditions. This 
%       structure is generated as output of the constraint functions in 
%       "WP_WS_diagram.m".
%   - WP_path: contains the power-loading values of the different paths
%       that link the different components of the powertrain, for each 
%       constraint and as a function of "WS". For example, "WP_path.cr.s1" 
%       gives, as a function of wing loading, the power transmitted through
%       the primary shaft, from the gearbox to the primary propulsor 
%       (expressed as a power-loading). This structure is generated as 
%       output of the constraint functions in "WP_WS_diagram.m".
%   - WP_select: a series of WP-arrays obtained from WP_comp, WP_loss, or 
%       WP_path, which are manually selected at the end of 
%       "WP_WS_diagram.m" and passed on to "ComputeDesignPoint.m" in order
%       to evaluate the design points which lead to maximum power-loading
%       values of the fields specified (per constraint) in WP_select.
%   - WPdes: design power-loadings obtained from "ComputeDesignPoint.m" for
%       different design criteria. For example, "WPdes.minWS.GTM" refers to
%       the required power loading of the gas turbine (corrected to
%       TO/SL/max thrust conditions) when selecting the design point
%       corresponding to minimum wing area. The results in this structure
%       determine the installed power of the different powertrain 
%       components during the mission analysis, once the aircraft weight is 
%       known.
%   - WS: contains the wing-loading values at which TW and WP are sampled
%       for each constraint in the WP-WS diagram. These arrays are
%       generated in the constraint functions in "WP_WS_diagram.m".
%   - WSdes: wing-loading values of the design points selected. For
%       example, "WSdes.minbat" gives the wing loading value at which the
%       required battery power is minimized. Obtained from
%       "ComputeDesignPoint.m".
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 04-04-18
%%% Last modified: 10-07-24


%% Initialize
clc
clear 
close all
tic
warning on
addpath('Surrogate models and data') 
addpath('Input files') 


%% Main body

% Load Class-I input parameters (select one be uncommenting)
InputConventionalAircraft_OnlyClassI;
% InputConventionalAircraft;
% InputHybridAircraft;

% Load Class-II input parameters
if s.WeightEstimationClass == 2
%     InputConventionalAircraft_ClassII;
%     InputHybridAircraft_ClassII;
end

% Check input is consistent
if s.printMessages == 1
    disp([s.levelString '> Checking input settings'])
end
CheckInput;

% Start Class-II convergence loop 
errC2 = 1;
iterC2 = 0;
xManual0 = s.xManual;
while errC2 > s.errmax
    
    % Set iteration conter
    iterC2 = iterC2 + 1;
    
    % Assign/update aerodynamic characteristics
    AssignAeroProperties;
    
    % Generate wing-loading power-loading diagrams
    if s.printMessages == 1
        disp([s.levelString '> Evaluating W/S-W/P diagram'])
    end
    WP_WS_diagram;
    
    % Compute WS & WP of reference aircraft for weight estimation (Class I)
    if s.WeightEstimationClass ~= 2 || iterC2 == 1
        if s.printMessages == 1
            disp([s.levelString '> Computing reference aircraft WS/WP'])
        end
        [WSdes_ref,WPdes_ref] = ComputeReferencePowerLoading(a,m,p,s,c,f);
    end
    
    % Run mission analysis (incl weight estimation)
    if s.printMessages == 1
        disp([s.levelString '> Starting Mission Analysis'])
    end
    MissionAnalysis;
    
    % Steps not performed in Class-I analysis
    if s.WeightEstimationClass == 2
        
        % Determine geometry, CG, & tail size
        if s.printMessages == 1
            disp([s.levelString '> Determining geometry, CG, & tail size'])
        end
        CallConfigurationAndGeometry; 
        
        % Perform Class-II aero
        if s.AeroEstimationClass == 2
            if s.printMessages == 1
                disp([s.levelString '> Performing Class-II aero estimation'])
            end
            C2_in = C2;
            C2_in.settings.plotAero = 0;
            [a,errC2_aero,~] = ComputeClassIIAero(a,m,p,f,s,C2_in,aircraft,MA);
        else
            errC2_aero = 0;
        end
    end
    
    % Check convergence: for Class-I analysis, set error to zero and stop here
    if s.WeightEstimationClass ~= 2
        errC2 = 0;
        
    % For Class-II analysis, add error contributions. Note that individual
    % error contributions are computed inside CallConfigurationAndGeometry.
    % Initial values for next iteration are also updated there.
    else
        
        % Total error
        errC2 = abs(errC2_TOM) + abs(errC2_xCG) + ...
                abs(errC2_sHT) + abs(errC2_sVT) + abs(errC2_aero);  
           
        % Display progress
        if s.printMessages == 1
            disp([s.levelString 'Completed Class-II iteration ' num2str(iterC2) ...
                  '. Error = ' num2str(errC2)])
        end
    end
    
    % Break if not converging
    if iterC2 > s.itermax/4
        error('Class-II sizing did not converge');
    end    
    if isnan(errC2)
        error('Class-II convergence criterion attained NaN value')
    end
    
    % If fixed span, update wing loading selection for next iteration. Note
    % that this is the span limit defined in *spanwise* direction (i.e.
    % along the plane of each semi-wing), not in Y-direction
    if errC2 > s.errmax && strcmp(s.SelDes,'manual') && ...
       s.WeightEstimationClass == 2 && C2.settings.FixedSpan ~= 0
        WSlimit = a.AR*c.g*M.TOM/C2.settings.SpanLimit^2;
        xLimit = WSlimit/WSdes.minWS;
            
        % Equality constraint (set b = SpanLimit)
        if C2.settings.FixedSpan == 1  
            s.xManual = xLimit;
            
        % Inquality constraint (set b = SpanLimit only if currently b > SpanLimit)
        elseif C2.settings.FixedSpan == 2 
            if xLimit > xManual0 
                s.xManual = xLimit;
            else
                s.xManual = xManual0;   % Reset to original input value,
            end                         % in case modified in previous iter
        end
        
        % Limit to max wing loading at most
        if s.xManual > 1
            if s.printMessages == 1
                disp([s.levelString ...
                    '> Span obtained at max wing loading exceeds span limit'])
            end
            s.xManual = 1;
        end
    end
end


%% Generate plots if desired

% 0. Show constraint diagrams
if s.plotWPWS == 1 
    if s.printMessages == 1
        disp([s.levelString '> Generating constraint-diagram plots'])
    end
    [~,~,~,~,~,~,~,~,s.figs] = ComputeDesignPoint(WS,WP_select,TW,a,m,p,f,s,c);
end

% I. Create mission-analysis profiles
if s.plotMA == 1
    if s.printMessages == 1
        disp([s.levelString '> Generating mission-analysis plots'])
    end
    [s] = PlotMissionAnalysis(p,m,s,c,aircraft,M,MA,WSdes,WPdes,WS,WP_path);
end

% II. Powertrain plots
% (Note: the power paths used here come from the
% constraints, not the mission analysis! So the powers shown do not
% necessarily coincide with a given point on the MA mission profile)
if s.plotPowertrain == 1
    if s.printMessages == 1
        disp([s.levelString '> Generating powertrain plots'])
    end
    P_path.cr = structfun(@(x) 1./x/m.cr.f*M.TOM*c.g/1e6,...
        WP_path.cr,'UniformOutput',0);
    P_path.TO = structfun(@(x) 1./x/m.TO.f*M.TOM*c.g/1e6,...
        WP_path.TO,'UniformOutput',0);
    P_path.L = structfun(@(x) 1./x/m.L.f*M.TOM*c.g/1e6,...
        WP_path.L,'UniformOutput',0);
    [s.figs(end+1)] = PlotPowertrain(P_path.cr,WS.cr,...
        WSdes.(s.SelDes),s.figStart+size(s.figs,2),'%6.2f',...
        'Cruise constraint [MW]');
    [s.figs(end+1)] = PlotPowertrain(P_path.TO,WS.TO,...
        WSdes.(s.SelDes),s.figStart+size(s.figs,2),'%6.2f',...
        'Take-off constraint [MW]');
    [s.figs(end+1)] = PlotPowertrain(P_path.L,NaN,...
        NaN,s.figStart+size(s.figs,2),'%6.2f',...
        'Landing constraint [MW]');
    clear('P_path')
end

% III. Aerodynamic polar in cruise conditions
if s.Polar.plot == 1
    if s.printMessages == 1
        disp([s.levelString '> Generating cruise polar'])
    end
    [~,~,~,~,s.figs(end+1)] = CreateLiftDragPolarMap(WSdes.(s.SelDes),...
        'cr',a,m,p,f,s,c,1);
end

% IV. Power-control envelopes
if s.Env.plot == 1
    if s.printMessages == 1
        disp([s.levelString '> Generating power-control envelope(s)'])
    end
    [s] = CreatePowerControlEnvelope(p,s,f,c,WPdes,AEROdes,MA,M);
end

% V. Add drag requirements to stall constraint
if s.LandingCheck.plot == 1
    if s.printMessages == 1
        disp([s.levelString '> Adding landing drag requirements'])
    end
    [s,AEROdes] = CreateLandingDragRequirements_v2(a,m,p,f,s,c,WSdes,WPdes,AEROdes);
end

% VI. Plot overall efficiencies
if s.plotOverallEfficiencies == 1
    if s.printMessages == 1
        disp([s.levelString '> Plotting overall efficiencies'])
    end
    [s] = PlotOverallEfficiencies(a,m,p,f,s,c,MA);
end

% VII. Class-II aero
if s.WeightEstimationClass == 2
    [~,~,s] = ComputeClassIIAero(a,m,p,f,s,C2,aircraft,MA);
end
    
% VIII. Aircraft geometry
if s.WeightEstimationClass == 2
    if C2.settings.plotAircraft == 1
        [~] = ConfigurationAndGeometry_v2(p,C2,aircraft);
    end
end


%% Finish

% Clear loop variables (can be commented for debugging)
clear('FF_tot0','FF_miss0','DOH_tot0','DOH_miss0','DOHmiss',...
      'FF_tot1','FF_miss1','DOH_tot1','DOH_miss1','DOHtot',...
      'electric','count','err','TOM0','TOM1','i','conv',...
      'Ebat_miss1','Ebat_tot1','EbatMiss','EbatTot',...
      'Ef_miss1','Ef_tot1','Mbat_div','Mbat_miss0','Mbat_miss1',...
      'Mbat_tot0','Mbat_tot1','Mf_div','Mf_miss0','Mf_miss1',...
      'Mf_tot0','Mf_tot1','Ncon','R_climb','R_cruise',...
      'R_Dclimb','R_Dcruise','R_descent','R_Ddescent','j','k',...
      'Ebat_div','Ebat_end','Ebat_miss0','Ebat_tot0','aa','phiStatus',...
      'PhiStatus','xiStatus','names','DOH_miss','warnings',...
      'DOH_missref','DOH_totref','Pavail','Pnames','Preq','Pstrings',...
      'rho','Sstring','Ratiobat','Ratiobat0','RatioEM1','RatioEM10',...
      'RatioEM2','RatioEM20','RatioGTM','RatioGTM0','Ratios1','Ratios10',...
      'ScalingError','h_target','h_targetDiversion','t_target','vLimits',...
      'E_tot','EfEnd','EfStart','segmentNames','WfEnd','WfStart','a_n',...
      'E_div','E_div_incl_fractions','E_extra_div','E_extra_tot',...
      'E_tot_incl_fractions','errC2','errC2_aero','errC2_sHT','errC2_sVT',...
      'errC2_TOM','errC2_xCG','iterC2','n_n','C2_in','fnames',...
      'P_s_tot','s_in','v_n','WfLanding','WfTaxIn','WSlimit','xLimit',...
      'xManual0');
  
% Save workspace?
if s.Save == 1
    
    % Hide figures
    for ii = 1:length(s.figs)
        if isgraphics(s.figs(ii))
            set(s.figs(ii),'Visible','Off')
        end
    end
    clear('ii')
    
    % Generate .mat file with figures hidden
    save([s.SavePath '\' s.SaveFn])
    
    % Re-show figures
    for ii = 1:length(s.figs)
        if isgraphics(s.figs(ii))
            set(s.figs(ii),'Visible','On')
        end
    end
    clear('ii')
end

% End 
if s.printMessages == 1
    disp([s.levelString '> Completed. Run time: ' num2str(toc) ' seconds'])
end



