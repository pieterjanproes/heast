function [WS,WP_path,WP_comp,WP_loss,TW,a,m,p] = ConstraintTakeOffDistance_v3(a,m,p,f,s,c)
% This function computes the take-off distance using the TAKE-OFF PARAMETER
% Raymer's book. It's only sensitive to MAXIMUM LIFT COEFFICIENT during
% take-off.

% Take-off operating conditions
% Note: velocity margin changed in Initiatior!

% Atmospheric conditions 
[T_inf,aa,~,m.TO.rho] = atmosisa(m.TO.h);

% Select TP or TF anonymous function for TOP. Note that the TF TOP computes
% T/W and not W/P_s! (NB: haven't checked yet whether the code leads
% to proper results for jets)
if strcmp(a.ref.config,'TF')
    TOP_TF = f.TOP_TF;
elseif strcmp(a.ref.config,'TP')
    TOP_TP = f.TOP_TP;
else
    error('Incorrect reference configuration (s.RefConfig) specified')
end


%% Compute power loading (not guaranteeing equilibrium flight)

% Initialize loop variables
WS = linspace(s.WSmin,s.WSmax,s.n); 
WP = NaN(1,s.n);                           
TW = NaN(1,s.n);    
m.TO.v = NaN(1,s.n);         

% Loop over all wing loading values
for i = 1:s.n
    
    % Start loop parameters for iterations @ STALL SPEED to obtain
    % CLmax,tot
    errA = 1;               
    iter = 0;           
    
    % Initial guess for shaft power loading at stall and TO velocity
    WP0s = 0.2;
    WP0 = WP0s;
    
    % Select wing loading
    WS0 = WS(i);
     
    % Inital guess for stall speed in TO config (at screen height)
    vs = (WS0/a.TO.CLmax/0.5/m.TO.rho)^0.5;         
       
    % Initial values for propulsive efficiency and thrust share (these will be
    % the values that are ued if ComputeEtap = 0)
    if p.DP == 1
        etap = p.TO.etap1;
        etap_conv = p.TO.etap2;
    elseif p.DP == 2
        etap = p.TO.etap2;
        etap_conv = p.TO.etap1;
    elseif p.DP == 0
        if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
                strcmp(p.config,'e-1')
            etap = p.TO.etap2; 
            etap_conv = p.TO.etap1;
        elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
                strcmp(p.config,'e-2')
            etap = p.TO.etap1;
            etap_conv = p.TO.etap2;
        else
            etap = p.TO.etap2;
            etap_conv = p.TO.etap1;
        end
    end
    chi0 = p.TO.chi(end);
    detap = 0;
    eta_eff0 = 0.7;
    
    % Use same initial values for stall conditions
    etaps = etap;
    detaps = detap;
    etaps_conv = etap_conv;
    chi0s = chi0;
    eta_eff0s = eta_eff0;
    
    % Rotor sizing
    D2W_TO = f.D2W(WS0,a.AR,p);           
    D2W_TO = min([D2W_TO 1e9]);
    D2W_conv = f.D2W_conv(WS0,a.AR,p);
    D2W_conv = min([D2W_conv 1e9]);
    
    % Compute prop radius/wing chord ratio
    Rc = 0.5*(D2W_TO*WS0*a.AR)^0.5;
    
    % Compute stall speed for each wing loading
    while errA>s.errmax
        
        % Loop counter
        iter = iter+1;
        
        % Update effective propulsive efficiency in stall conditions
        if strcmp(p.config,'conventional') || strcmp(p.config,'e-1') ...
                || strcmp(p.config,'parallel')
            if p.DP == 0
                eta_eff1s = etaps_conv;
            else
                eta_eff1s = etaps + detaps;
            end
        elseif strcmp(p.config,'turboelectric') || strcmp(p.config,'e-2') ...
                || strcmp(p.config,'serial')
            if p.DP == 0
                eta_eff1s = etaps_conv;
            else
                eta_eff1s = etaps + detaps;
            end
        elseif strcmp(p.config,'SPPH') || strcmp(p.config,'PTE') ...
                || strcmp(p.config,'dual-e')
            if p.DP == 1
                eta_eff1s = (1-m.TO.Phi)*(etaps+detaps) + m.TO.Phi*etaps_conv;
            elseif p.DP == 2 || p.DP == 0
                eta_eff1s = (1-m.TO.Phi)*etaps_conv + m.TO.Phi*(etaps+detaps);
            end
        end
               
        % Thrust loading (in TO conditions at stall speed, not at TO-speed)
        TW0s = eta_eff1s/WP0s/vs;  
        
        % Compute thrust coefficients in stall conditions
        if TW0s==Inf
            Tcs = chi0s*1e9;
            Tcs_conv = (1-chi0s)*1e9;
        else
            Tcs = chi0s*TW0s/p.N/m.TO.rho/vs^2/D2W_TO;
            Tcs_conv = (1-chi0s)*TW0s/p.N_conv/m.TO.rho/vs^2/D2W_conv;
        end
        
        % Update propulsive efficiency and thrust share
        if s.ComputeEtap == 1
            
            % Compute propulsive efficiency based on assumed thrust coefficient 
            etaps = f.etap(Tcs);
            etaps_conv = f.etap(Tcs_conv);
            
            % Recompute thrust share
            if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
                    strcmp(p.config,'dual-e')
                if p.DP == 1
                    chi1s = 1/( m.TO.Phi / (1-m.TO.Phi) ...
                        * p.TO.etap2/(etap+detap) + 1);
                else
                    chi1s = 1/((1-m.TO.Phi)/ m.TO.Phi   ...
                        * p.TO.etap1/(etap+detap) + 1);
                end
            else
                chi1s = p.TO.chi(end);
            end
            
        % If eta_p does not have to be computed, use the thrust share
        % previously computed in CheckInput.m   
        else
            chi1s = p.TO.chi(end);
        end
        
        % Update aerodynamic properties if DP is used
        %         if p.DP == 1 || p.DP == 2
        
        % Estimate Mach & Re number
        M = vs/aa;
        Res = vs*a.c_ref*m.TO.rho/f.mu(T_inf);
        
        % Compute delta terms in stall conditions
        oper.e = a.TO.e;
        oper.CD0 = a.TO.CD0;
        oper.Gamma = p.TO.Gamma;
        oper.CL = a.TO.CLmax;
        oper.M = M;
        oper.etap = etaps;
        oper.Tc = Tcs;
        oper.Tc_conv = Tcs_conv;
        oper.Rc = Rc;
        oper.Re = Res;
        [dCL,~,~,detaps] = WingPropDeltas_v5(oper,a,p,f,s,c,0);
        
        %         % If no DP is used, all deltas are zero
        %         else
        %             dCL = 0;
        %             detaps = 0;
        %         end
        
        % Effective wing cl increase due to DP TV in stall conditions
        dCL_TV = chi1s*TW0s*WS0*sind(p.TO.Gamma)/0.5/m.TO.rho/vs^2;
        
        % Total maximum lift coefficient
        CL_tot = a.TO.CLmax + dCL + dCL_TV;      
        
        % Updated stall velocity
        vs = (WS0/CL_tot/0.5/m.TO.rho)^0.5;         

        % Shaft power loading at stall speed
        if strcmp(a.ref.config,'TF')
            TW1s = WS0/(m.TO.rho/c.rho_SL)/CL_tot/TOP_TF(m.TO.s)...
                    /(1 - chi1s + chi1s*cosd(p.TO.Gamma));
            WP1s = eta_eff1s/TW1s/vs;
        else
            WP1s = TOP_TP(m.TO.s)*CL_tot*(m.TO.rho/c.rho_SL)/WS0...
                    *(1 - chi1s + chi1s*cosd(p.TO.Gamma)); 
        end
        
        % Change initial point if WP returns NaN
        if isnan(WP1s)
            errA = 1;
            
            % Add small offset to avoid reiterating NaN values
            WP0s = 0.21;
            
        % Update error/values
        else
            err1 = abs(WP1s-WP0s)/abs(WP0s);
            err3 = abs(eta_eff1s-eta_eff0s)/abs(eta_eff0s);
            
            % Avoid NaNs if Tc is always zero
            if chi1s==0 && chi0s==0
                err2 = 0;
            else
                err2 = abs(chi1s-chi0s)/abs(chi0s);
            end
            errA = err1 + err2 + err3; 
            WP0s = WP1s;
            chi0s = chi1s;
            eta_eff0s = eta_eff1s;
        end    
        
        % If the error becomes a NaN or complex number anyway, the code
        % will crash or provide an unconverged solution. Note that err will
        % never be complex because of the abs function
        if isnan(errA)
            warning('Convergence error attained NaN value')
            WP1 = NaN;
            chi1 = NaN;
            eta_eff1 = NaN;
            break
        end
        
        % Stop at max iterations
        if iter>=s.itermax
            CL_tot = NaN;
            break
        end
    end
       
    % Start loop parameters for iterations @ TO SPEED
    errB = 1;
    iter2 = 0;
    
    % Take-off speed
    v = vs*m.TO.vMargin;
    M = v/aa;
    Re = v*a.c_ref*m.TO.rho/f.mu(T_inf);
    
    % Compute stall speed for each wing loading
    while errB>s.errmax
        
        % Loop counter
        iter2 = iter2+1;
        
        % Update effective propulsive efficiency in actual flight conditions
        if strcmp(p.config,'conventional') || strcmp(p.config,'e-1') ...
                || strcmp(p.config,'parallel')
            if p.DP == 0
                eta_eff1 = etap_conv;
            else
                eta_eff1 = etap + detap;
            end
        elseif strcmp(p.config,'turboelectric') || strcmp(p.config,'e-2') ...
                || strcmp(p.config,'serial')
            if p.DP == 0
                eta_eff1 = etap_conv;
            else
                eta_eff1 = etap + detap;
            end
        elseif strcmp(p.config,'SPPH') || strcmp(p.config,'PTE') ...
                || strcmp(p.config,'dual-e')
            if p.DP == 1
                eta_eff1 = (1-m.TO.Phi)*(etap+detap) + m.TO.Phi*etap_conv;
            elseif p.DP == 2 || p.DP == 0
                eta_eff1 = (1-m.TO.Phi)*etap_conv + m.TO.Phi*(etap+detap);
            end
        end      
                
        % Thrust loading (in TO conditions at TO-speed)
        TW0 = eta_eff1/WP0/v;
            
        % Compute thrust coefficient in stall conditions
        if TW0==Inf
            Tc = chi0*1e9;
            Tc_conv = (1-chi0)*1e9;
        else
            Tc = chi0*TW0/p.N/m.TO.rho/v^2/D2W_TO;
            Tc_conv = (1-chi0)*TW0/p.N_conv/m.TO.rho/v^2/D2W_conv;
        end
        
        % Update propulsive efficiency and thrust share
        if s.ComputeEtap == 1
            
            % Compute propulsive efficiency based on assumed thrust coefficient
            etap = f.etap(Tc);
            etap_conv = f.etap(Tc_conv);
            
            % Re-compute thrust share
            if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
                    strcmp(p.config,'dual-e')
                if p.DP == 1
                    chi1 = 1/( m.TO.Phi / (1-m.TO.Phi) ...
                        * etap_conv/(etap+detap) + 1);
                else
                    chi1 = 1/((1-m.TO.Phi)/ m.TO.Phi   ...
                        * etap_conv/(etap+detap) + 1);
                end
            else
                chi1 = p.TO.chi(end);
            end
        else
            chi1 = p.TO.chi(end);
        end
        
        % Update detap if DP is used
        %         if p.DP == 1 || p.DP == 2
        oper.e = a.TO.e;
        oper.CD0 = a.TO.CD0;
        oper.Gamma = p.TO.Gamma;
        oper.CL = a.TO.CLmax;
        oper.M = M;
        oper.etap = etap;
        oper.Tc = Tc;
        oper.Tc_conv = Tc_conv;
        oper.Rc = Rc;
        oper.Re = Re;
        [~,~,~,detap] = WingPropDeltas_v5(oper,a,p,f,s,c,0);
        %         else
        %             detap = 0;
        %         end
        
        % Shaft power loading at take-off speed.
        % Note: Not sure of the rho/rho_SL factor should be included when
        %   calculating shaft power (gas turbine altitude lapse should not
        %   be included). For TO at SL the results is the same, but caution
        %   should be exerted if TO/L is carried out at higher altitudes.
        if strcmp(a.ref.config,'TF')
            TW1 = WS0/(m.TO.rho/c.rho_SL)/(CL_tot/m.TO.vMargin^2)/...
                    TOP_TF(m.TO.s)/(1 - chi1 + chi1*cosd(p.TO.Gamma));
            WP1 = eta_eff1/TW1/v;
        else
            WP1 = TOP_TP(m.TO.s)*CL_tot/m.TO.vMargin^2*(m.TO.rho/c.rho_SL)/WS0...
                *(1 - chi1 + chi1*cosd(p.TO.Gamma));
        end
        
        % Change initial point if WP returns NaN
        if isnan(WP1)
            errB = 1;
            
            % Add small offset to avoid reiterating NaN values
            WP0 = 0.21;
            
        % Update error/values
        else
            err1 = abs(WP1-WP0)/abs(WP0);
            err3 = abs(eta_eff1-eta_eff0)/abs(eta_eff0);
            
            % Avoid NaNs if Tc is always zero (which is a valid solution)
            if chi1==0 && chi0==0
                err2 = 0;
            else
                err2 = abs(chi1-chi0)/abs(chi0);
            end
            errB = err1 + err2 + err3; 
            WP0 = WP1;
            chi0 = chi1;
            eta_eff0 = eta_eff1;
        end    
                
        % If the error becomes a NaN anyway, the code
        % will crash or provide an unconverged solution. Note that err will
        % never be complex because of the abs function
        if isnan(errB)
            warning('Convergence error attained NaN value')
            WP1 = NaN;
            chi1 = NaN;
            eta_eff1 = NaN;
            break
        end
        
        % Stop at max iterations
        if iter2>=s.itermax
            WP1 = NaN;
            chi1 = NaN;
            eta_eff1 = NaN;
            break
        end
    end    
    
    % Convert to propulsive power
    WP(i) = WP1/eta_eff1/m.TO.f;
    
    % Store speed, lift coefficient, and propulsive efficiency change
    m.TO.v(i) = v;
    m.TO.M(i) = M;
    m.TO.Re(i) = Re;
    a.TO.CL(i) = CL_tot/m.TO.vMargin^2;
    p.TO.detap(i) = detap;
    p.TO.etap(i) = etap;
    p.TO.chi(i) = chi1;
    p.TO.Tc(i) = Tc;           
    p.TO.etap_conv(i) = etap_conv;
    
    % Save thrust loading. Note: may not be very representative, since v2
    % speed is used as reference
    TW(i) = 1/WP(i)/m.TO.v(i);
end

      
%% Compute HEP component power loading

% All engines operative
OEI = 0;

% Replace NaNs with Inf's so that the code understands that WP is an input.
% Keep indices to revert changes later
indices = isnan(WP);
WP(indices) = Inf;

% Call sizing routine
[WP_path,WP_comp,WP_loss] = SizePowertrain(WP,'TO',OEI,m,p,f,s,c);

% Change -Inf to +Inf to avoid warnings; Inf is a possible solution for
% zero power flow
pathnames = fieldnames(WP_path);
for i = 1:size(pathnames,1)
    WP_path.(pathnames{i})(WP_path.(pathnames{i})==-Inf) = Inf;
    WP_path.(pathnames{i})(indices) = NaN;
end
compnames = fieldnames(WP_comp);
for i = 1:size(compnames,1)
    WP_loss.(compnames{i})(WP_loss.(compnames{i})==-Inf) = Inf;
    WP_comp.(compnames{i})(WP_comp.(compnames{i})==-Inf) = Inf;
    WP_loss.(compnames{i})(indices) = NaN;
    WP_comp.(compnames{i})(indices) = NaN;
end












