function [M] = WeightEstimationClass1(a,m,p,f,s,c,aircraft,M)
%%% Description
% This function estimates the "Class 1.5" weight breakdown of the aircraft
% for a given output of the WP-WS diagram and a determined battery and fuel
% energy required to complete the mission. A reference OEM is estimated
% as a function of TOM using Roskam's method. The weight of a reference
% (conventional aircraft) wing and gas turbine is subtracted to obtain an
% OEM which excludes wing and powerplant. The actual wing weight and
% powerplant weight are then computed using Roskam/Torenbeek methods.
% For now gearbox, PMAD and propulsor weight are neglected.

% Input:
%   - a,m,p,f,s,c: structures containing aircraft and program data (see
%       main input file)
%   - aircraft: structure containing:
%       - Pdes: installed power of the powerplant components on the
%           aircraft, as obtained from WPdes in the WP-WS diagram [W]
%       - TOM: initial guess of the take-off mass of the aircraft [kg]
%       - Sw: wing area [m2]
%       - Sw_ref, Pdes_ref: characteristics of reference aircraft
%       - PL: payload [kg]
%   - M: structure containing masses in [kg] of:
%           - f: fuel mass
%           - bat: installed battery mass
%           - bat_miss: battery mass required for nominal mission
%           - bat_E: battery mass required for total mission energy (incl. div.)
%           - bat_P: battery mass required to meet power requirements
%           - PL: payload 
%
% Output:
%   - M: updated structure, additionally containing masses in [kg] of:
%       - EM1: primary electrical machines (total) mass
%       - EM2: secondary electrical machines (total) mass
%       - GT: gas turbines (total) mass
%       - w: wing mass
%       - OEM: operative empty mass EXCLUDING wing and powertrain
%       - TOM: total take-off mass
%
%
%%% Reynard de Vries
%%% First created: 28-03-22
%%% Last modified: 28-03-22


%% Components that do not depend on TOM

% Structure containing installed power of GT and EM's
P = aircraft.Pdes;

% Mass of reference gas turbines [kg]. Select TP or TF weight correlation
% depending on reference configuration.
P_GTM_ref = aircraft.Pdes_ref.GTM/a.ref.N; 
if strcmp(a.ref.config,'TP')
    M_GT_ref = a.ref.N*f.W.GT(P_GTM_ref);
elseif strcmp(a.ref.config,'TF')
    
    % Estimate engine thrust by dividing installed power by velocity at
    % take-off. Not totally correct since thrust varies with speed.
    % If the design is limited by the TO constraint, then the thrust 
    % estimated here is correct, since for RefConfig = 'TF', the TOP is
    % used in terms of thrust, and therefore the TW obtained at the design
    % point is the correct value, while the WP is estimated using the
    % velocity at take-off.
    T_ref = P_GTM_ref/aircraft.v_TO*aircraft.etap1; % GT connected to P1 only  % THIS DOES NOT WORK
                                                    % (NB: v_TO & etap1 include A-P effecs!)
    M_GT_ref = a.ref.N*f.W.TF(T_ref);
else
    error('Incorrect reference configuration specified')
end

% Actual mass of gas turbines [kg]
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') ...
                          || strcmp(p.config,'dual-e')
    M_GT = 0;
else
    
    % For the conventional configuration, use TF weight estimation instead.
    % If we want a TF-like aircraft
    % In that case, gearbox and fan weight should be 0!
    if strcmp(p.config,'conventional') && strcmp(a.ref.config,'TF')
        M_GT = p.N1*f.W.TF(T_ref);
    else    
        M_GT = p.N1*f.W.GT(P.GTM/p.N1);
    end
end

% Weight of electrical machines [kg]. Use throttle-corrected (M) values
if ~isnan(P.EM1M) 
    M_EM1 = p.N1*f.W.EM1(P.EM1M/p.N1); 
else
    M_EM1 = 0;
end
if ~isnan(P.EM2M) 
    M_EM2 = p.N2*f.W.EM2(P.EM2M/p.N2); 
else 
    M_EM2 = 0; 
end


%% Components that depend on TOM

% Initial guess for TOM
TOM0 = aircraft.TOM;

% Loop till TOM convergence
err = 1;
iter = 0;
while err > s.errmax
    
    % Update counter
    iter = iter + 1;
    
    % Estimate operative empty mass (incl. wing and conventional power
    % plant)
    if strcmp(a.ref.config,'TF')
        OEM_ref = f.W.OEM_TF(TOM0);
    elseif strcmp(a.ref.config,'TP')
        OEM_ref = f.W.OEM_TP(TOM0);
    end
    
    % Compute zero fuel weight
    MZFW = TOM0-M.f;
    
    % Compute reference aircraft (no-DP) wing weight
    b_ref = (aircraft.Sw_ref*a.ref.AR)^0.5;                     % Wing span [m]
    tr_ref = a.tc*(aircraft.Sw_ref/a.ref.AR)^0.5*2/(1+a.TR);    % Root thickness [m]
    bs_ref = b_ref/cosd(a.Lambda);                              % Corrected span [m]
    
    % Reference wing mass (Torenbeek S. 8.4.1b, p.280). Ignoring weight
    % reduction due to wing-mounted engines (would require specifying
    % whether engines are or not on wing, and probably not accurate for 
    % e.g. tip-mounted engines and DP).
    M_w_ref = MZFW*6.67e-3*bs_ref^0.75*(1+(1.905/bs_ref)^0.5)*...
                a.nUlt^0.55*((bs_ref/tr_ref)/(MZFW/aircraft.Sw_ref))^0.3;
    
    % OEM excl. wing and conventional powerplant
    OEM = OEM_ref - M_w_ref - M_GT_ref;
    
    % Actual wing mass 
    bs = (aircraft.Sw*a.AR)^0.5/cosd(a.Lambda);
    tr = a.tc*(aircraft.Sw/a.AR)^0.5*2/(1+a.TR);
    M_w = MZFW*6.67e-3*bs^0.75*(1+(1.905/bs)^0.5)*...
                a.nUlt^0.55*((bs/tr)/(MZFW/aircraft.Sw))^0.3;

    % Recompute TOM
    TOM1 = OEM + aircraft.PL + M.bat + M.f + M_w + M_GT + M_EM1 + M_EM2;
    
    % Check error and update
    err = abs((TOM1-TOM0)/TOM0);
    TOM0 = TOM1;
    
    if iter > s.itermax
        if s.printMessages == 1
            disp([s.levelString '    > TOM did not converge!'])
        end
        TOM1 = NaN;
        break
    end
end

% Add results to output structure
M.EM1 = M_EM1;
M.EM2 = M_EM2;
M.GT = M_GT;
M.w = M_w;
M.OEM = OEM;
M.TOM = TOM1;





