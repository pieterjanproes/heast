function [dCL,dCD0,dCDi,detap] = WingPropDeltas_v5(oper,a,p,f,s,c,options)
% This function computes the lift, zero-lift drag, and thrust-induced drag
% increase of a wing featuring wing-mounted, leading-edge or over-the-wing
% distributed propulsion systems.
%
% Input:
%   - oper: structure containing current operating conditions: Thrust
%       coefficient, Mach number, airframe-only lift coefficient, iso prop
%       efficiency, and other variables which vary throughout the code such
%       as the propeller-radius-to-wing-chord ratio, thrust vectoring
%       angle, and oswald factor.
%   - a,p,f,s,c: structures from Input file
%   - options: plots will be shown for the selected aero-propulsive model
%       if set to 1.
%
% Output:
%   - dCL: lift coefficient increased due to DP w.r.t. clean wing. 
%   - dCD0: zero-lift/zero-thrust drag penalty due to e.g. pylons and
%       nacelles of DP system and increase in friction drag
%   - dCDi: thrust-induced drag, i.e. sectional drag coefficient increase
%       due to the DP system in power-on conditions.
%   - detap: change in propulsive efficiency, etap_installed - etap_iso.
%
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 21-06-17
%%% Last modified: 10-06-21


%% Extract input data

% Operating conditions
CD0 = oper.CD0;         % Zero-lift drag coefficient [-]
e = oper.e;             % Oswald factor [-]
Gamma = oper.Gamma;     % DP-system thrust vectoring angle [deg]
CL = oper.CL;           % Isolated wing CL [-]
M = oper.M;             % Freestream mach number [-]
etap_iso_dp = oper.etap;% Isolated prop eta_p [-]
Tc = oper.Tc;           % DP props thrust coefficient, per prop, Tc = T_dp/(rho*v_inf^2*D*2) [-]
Tc_conv = oper.Tc_conv; % Main props thrust coefficient, per prop, T_conv/rho/V_inf^2/D_conv^2 [-]
Re = oper.Re;           % Wing-chord-based freestream Reynolds number

% General geometrical parameters
Rc = oper.Rc;           % Ratio between propeller radius and wing chord [-]
b_dp = p.geom.b_dp;     % Wing span-fraction occupied by DP propulsor(s) [-]
N_dp = p.N;             % Number of DP propellers [-]
N_conv = p.N_conv;      % Number of non-DP propellers [-]
AR = a.AR;              % Wing aspect ratio [-]
Lambda = a.Lambda;      % Wing half-chord sweep angle [deg]

% LEDP/OTW parameters
dy = p.geom.dy;         % Clearance between adjacent props, as a fraction of prop diameter [-]
xc = p.geom.xp;         % Axial position of propeller, w.r.t wing LE (positive downstream), fraction of wing chord [-]

% OTW  parameters
ip = p.geom.ip;         % Incidence angle of prop w.r.t. chord line, positive nose-up

% BLI parameters
ARf = p.geom.ARf;       % Fuselage aspect ratio L_fus/D_fus [-]
bRatio = p.geom.bRatio; % Ratio between fuselage diameter and wing span [-]
L1 = p.geom.L1;         % Length of upstream tailcone, as a fraction of Dfus [-]
L3 = p.geom.L3;         % Length of downstream tailcone, as a fraction of Dfus [-]
L2 = p.geom.L2;         % Length of cylindrical part of tailcone, as a fraction of Dfus [-]
Dcyl = p.geom.Dcyl;     % Diameter of cylindrical of tailcone part, as a fraction of Dfus [-]
Re_f = p.geom.Re;       % Fuselage length & freestream-velocity based Reynolds number [-]

% LLM parameters
b_conv = p.geom.b_conv; % D_main_props / b [-]
J_conv = p.geom.J_conv; % Main props advance ratio, V_inf/D_main/n_main [-]
J_dp = p.geom.J_dp;     % DP props advance ratio, V_inf/D_dp/n_dp [-]
dy_tip = p.geom.dy_tip; % dy_tip / b_free [-], where dy_tip is distance between wing tip
                        %   and most outboard DP propsulor, and b_free is
                        %   the avaiable space for DP propulsors


%% Select aero-propulsive model
switch p.AeroPropModel
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 1. Leading-Edge Distributed Propulsion, usinng analytical model
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 'LEDP'
    
    % 1. Lift coefficient increase
    % This method is based on an AD + flat plate assumption, described in
    % Patterson & German (2015), Patterson et al. (2016) and Patterson's 
    % PhD disseration (2016). An empirical correction/surrogate model
    % for finite slipstream height based on a surrogate model is applied.    
   
    % Velocity INCREASE at propeller disk [m/s] (v_effective = v_inf + v_p)
    % (Veldhuis 2005, Appendix A)
    a_p = 0.5*(sqrt(1+8/pi*Tc)-1);
    
    % Axial position of propeller as a fraction of propeller radius,
    % POSITIVE if propeller is ahead of LE. 
    %   Old version: w.r.t. c/4 -> xr = -(xc-1/4)/Rc;
    xr = -(xc-0.0)/Rc;
    
    % Fraction between slipstream diameter and propeller diameter at wing 
    % LE (Veldhuis 2005, Appendix C)
    kSS = @(a,x) ((1+a)./(1+a.*(1+x./(x.^2+1).^0.5))).^0.5;
    k_LE = kSS(a_p,xr);
    
    % Conservation of mass: velocity at LE[m/s]
    a_LE = (1+a_p)/k_LE^2-1;

    % From Patteson PhD 2016 (p. 93/94).
    % Surrogate model is valid for SS velocity ratios of 1.25-2.25, i.e.
    % for v_p/v_inf values of 0.125-0.625, R/c values of 0.125-3, and xc
    % positions ranging from -0.25 to -3. Note: if the thrust coefficient
    % is too low, a < 0.125 may be obtained, which could lead to
    % unrealistically high beta values. In this case beta is limited to 1.
    % Column vector X. Uses the velocity ratio of the jet far downstream,
    % and according to AD theory a_farDownstream = 2*a_p. The model also
    % includes the position of the
    % propeller, supposedly to account for contraction of the slipstream
    % radius.
    X = [1 -xc (-xc)^2 -xc*(2*a_p+1) (2*a_p+1) (2*a_p+1)^2]';
    
    % Constants from surrogate model (Patterson, 2016)
    K0 = [0.378269 0.748135 -0.179986 -0.056464 -0.146746 -0.015255];
    K1 = [3.071020 -1.769885 0.436595 0.148643 -0.989332 0.197940];
    K2 = [-2.827730 2.054064 -0.467410 -0.277325 0.698981 -0.008226];
    K3 = [0.997936 -0.916118 0.199829 0.157810 -0.143368 -0.057385];
    K4 = [-0.127645 0.135543 -0.028919 -0.026546 0.010470 0.012221];

    % Beta factor
    Beta = K0*X + K1*X*Rc + K2*X*Rc^2 + K3*X*Rc^3 + K4*X*Rc^4;
    
    % Due to extrapolation/surrogate model, beta might be slightly larger 
    % than 1. Limit to 1.
    Beta = min([Beta 1]);
    
    % Wing AoA
    % Method for finite wings from Roskam: Methods for estimation drag
    % polars of subsonic airplanes, S. 3.3.1. Assuming 2D lift curve slope
    % of airfoil sections = 2*pi.
    Mcorr = (1-M^2)^0.5;
    CLalpha = 2*pi*AR/(2+(AR^2*Mcorr^2*(1+(tand(Lambda))^2/Mcorr^2)+4)^0.5);
    alfa = rad2deg(CL/CLalpha);
    
    % Angle between propeller axis and wing chord [deg]
    ip = Gamma-alfa;
    
    % Sectional lift coefficient increase (Patterson 2015)
    dCl = 2*pi*((sind(alfa)-Beta*a_LE*sind(ip))*...
        (Beta^2*a_LE^2+2*Beta*a_LE*cosd(alfa+ip)+1)^0.5 - sind(alfa));

    % Plot
    if options == 1
        figure('name','Lift coefficient increase, tractor configuration')
        
        % Streamtube contraction
        subplot(1,2,1)
        xp = -5:0.05:5;
        k_plot = kSS(a_p,xp);
        a_plot = (1+a_p)./k_plot.^2-1;
        grid on; hold on; box on
        plot(xp,k_plot,'b')
        plot(xp,a_plot,'r')
        for aa = [0 0.2 0.4 0.6 0.8 1]
            k_plot = kSS(aa,xp);
            a_plot = (1+aa)./k_plot.^2-1;
            plot(xp,k_plot,'--')
            plot(xp,a_plot,'-.')
        end
        xlabel('x/R')
        ylabel('Velocity & Area ratio')
        legend('Slipstream radius','V/V_\infty-1',...
            'Slipstream radius, a = 0.0','V/V_\infty-1, a = 0.0',...
            'Slipstream radius, a = 0.2','V/V_\infty-1, a = 0.2',...
            'Slipstream radius, a = 0.4','V/V_\infty-1, a = 0.4',...
            'Slipstream radius, a = 0.6','V/V_\infty-1, a = 0.6',...
            'Slipstream radius, a = 0.8','V/V_\infty-1, a = 0.8',...
            'Slipstream radius, a = 1.0','V/V_\infty-1, a = 1.0')
        plot(xr,k_LE,'ob')
        plot(xr,a_LE,'or')
        axis([min(xp) max(xp) 0 3])
        title(['x/R = ' num2str(xr,'%.3f') ', a = ' num2str(a_p,'%.3f') ...
            ', a_{p} = ' num2str(a_p,'%.3f') ', r_{c/4}/R = ' ...
            num2str(k_LE,'%.3f') ', R/c = ' num2str(Rc,'%.3f') ...
            ', T_c = ' num2str(Tc,'%.3f')])
        
        % Finite slipstream height correction
        subplot(1,2,2)
        rc_array = 0.125:0.125:3;
        a_array = 0.125:0.02:0.625;
        beta_array = zeros(length(rc_array),length(a_array));
        for i = 1:length(rc_array)
            for j = 1:length(a_array)
                X = [1 -xc (-xc)^2 -xc*(2*a_array(j)+1) (2*a_array(j)+1) (2*a_array(j)+1)^2]';
                beta_array(i,j) = K0*X + K1*X*rc_array(i) + K2*X*rc_array(i)^2 + K3*X*rc_array(i)^3 + K4*X*rc_array(i)^4;
                beta_array(i,j) = min([beta_array(i,j) 1]);
            end
        end
        hold on; box on; grid on
        h1 = surf(rc_array,a_array,beta_array');
        rc_array2 = 0:0.125:3.5;
        a_array2 = 0.005:0.02:0.705;
        beta_array2 = zeros(length(rc_array2),length(a_array2));
        for i = 1:length(rc_array2)
            for j = 1:length(a_array2)
                X = [1 -xc (-xc)^2 -xc*(2*a_array2(j)+1) (2*a_array2(j)+1) (2*a_array2(j)+1)^2]';
                beta_array2(i,j) = K0*X + K1*X*rc_array2(i) + K2*X*rc_array2(i)^2 + K3*X*rc_array2(i)^3 + K4*X*rc_array2(i)^4;
                beta_array2(i,j) = min([beta_array2(i,j) 1]);
            end
        end
        hold on; box on; grid on
        surf(rc_array2,a_array2,beta_array2','facecolor','none')
        h2 = scatter3(Rc,a_p,Beta,'markerfacecolor','r','markeredgecolor','r');
        xlabel('R/c')
        ylabel('a = v_p/v_\infty')
        zlabel('\beta = {\Delta}c_l/{\Delta}c_{l,h=\infty}')
        title(['{\Delta}C_l = ' num2str(dCl,'%.3f') ' (' num2str(100*dCl/CL,'%.3f') '%)'])
        legend([h1,h2],'Surrogate model limits','Design point')
    end

    % Convert to 3D lift coefficient
    dCL = dCl*(b_dp - 2*N_dp*dy/AR*Rc);

    % 2. Zero-lift drag coefficient increase
    Cf = 0.009; % Local skin friction coefficient (see Biber 2011) 
    dCd0 = Cf*a_LE^2;
    dCD0 = dCd0*(b_dp - 2*N_dp*dy/AR*Rc);
        
    % 3. Lift-induced drag increase. Two effects: increase in CDi due to
    % deltaCL, and increase in CDi due to change in oswald factor. Oswald 
    % factor penalty not considered at this stage.
    deltae=0;
    e_eff = e + deltae;
    dCDi = (CL + dCL)^2/pi/AR/e_eff - CL^2/pi/AR/e;
    
    % 4. Propulsive efficiency not affected
    detap = 0;
   
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 2. OTW configurations (based on numerical model validated with LST)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Note: This model was based on Pepijn's numerical method up to version
    % WingPropDeltas_v4.m
    case 'OTW'

        % Load surrogate model data (data obtained from .mat file in
        % CheckInput.m)
        SM = p.geom.SM;
        
        % Convert Tc to the "TC" definition used in the SM, which is
        % non-dimensionalized with q_inf and propeller disk area
        TC = Tc*8/pi;
        
        % Compute Reynolds number exponent
        Rexp = log10(Re);
        
        % Diameter-to-chord ratio
        Dc = 2*Rc;
        
        % Gather input variables ("CL" is called "CL_iso" in SM). 
        in.CL_iso = CL;
        in.TC = TC;
        in.xp = xc;
        in.ip = ip;
        in.Dc = Dc;
        in.Rexp = Rexp;
        in.M = M;
        
        % Check specified separation between props is consistent with the
        % one of the numerical model. If not, the size of the propellers
        % relative to the wing segment is not consistent
        % Note dy = separation as a fraction of diameter; SM.d = separation
        % as a fraction of radius
        if dy ~= SM.dR/2
            warning(['Clearance between propellers should be ' num2str(SM.dR/2)...
                     ' times the propeller diameter for the OTW surrogate model'])
        end
        
        % Check propeller is indeed over the wing
        if xc < 0 || xc > 1
            warning('Propeller is not placed above the wing!')
        end
        
        % Check input variables are not out of bounds and constrain them if so
        varNames = SM.XVarNames; 
        for i = 1:length(varNames)
            if in.(varNames{i}) < SM.modelBounds.(varNames{i})(1)
                % warning([varNames{i} ' = ' num2str(in.(varNames{i})) ...
                %     ' is too low for SM. Limiting to ' ...
                %     num2str(SM.modelBounds.(varNames{i})(1))])
                in.(varNames{i}) = SM.modelBounds.(varNames{i})(1);
            elseif in.(varNames{i}) > SM.modelBounds.(varNames{i})(2)
                % warning([varNames{i} ' = ' num2str(in.(varNames{i})) ...
                %     ' is too high for SM. Limiting to ' ...
                %     num2str(SM.modelBounds.(varNames{i})(2))])
                in.(varNames{i}) = SM.modelBounds.(varNames{i})(2);
            end
        end
        
        % Gather input array & scale from 0 to 1
        X = zeros(1,length(varNames));
        for i = 1:length(varNames)
            X(1,i) = in.(varNames{i});
            X(1,i) = (X(1,i) - SM.modelScales.(varNames{i})(1))/...
                     (SM.modelScales.(varNames{i})(2) - SM.modelScales.(varNames{i})(1));
        end
        
        % Evaluate surrogate models to compute dCL/dCD (of one wing segment
        % of span D + d) and detap (expressed as a ratio of eta_iso)
        dCl = EvaluateSurrogateModel(SM.coeff.delta_CL,SM.E,X);
        dCd = EvaluateSurrogateModel(SM.coeff.delta_CD,SM.E,X);
        detap_ratio = EvaluateSurrogateModel(SM.coeff.delta_eta_ratio,SM.E,X);
        
        % Un-scale output parameters
        dCl = SM.modelScales.CL_delta(1) + dCl*(...
            SM.modelScales.CL_delta(2) - SM.modelScales.CL_delta(1));
        dCd = SM.modelScales.CD_delta(1) + dCd*(...
            SM.modelScales.CD_delta(2) - SM.modelScales.CD_delta(1));
        detap_ratio = SM.modelScales.eta_delta_ratio(1) + detap_ratio*(...
            SM.modelScales.eta_delta_ratio(2) - SM.modelScales.eta_delta_ratio(1));
        
        % Lift and drag: relate to total wing reference area
        dCL = dCl*b_dp;
        dCD = dCd*b_dp;
        
        % Consider all changes in drag as "lift-induced" drag
        dCDi = dCD;
        dCD0 = 0;
        
        % Propulsive efficiency: multiply by isolated propeller efficiency
        % to get absolute delta
        detap = detap_ratio*etap_iso_dp;        
        
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 3. Boundary-Layer Ingestion (can also work with LLM for main wing)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {'BLI','BLIandLLM'}
        
        % 0. Check input settings
        
        % Propeller diameter over fuselage diameter Dp/Dfus [-]. Since N
        % must be 1, and dy is ignored, it can be related to b_dp = Dp/b
        Dp = b_dp/bRatio;         
        
        % Program settings
        N = 50;                 % Number of points in BL to compute average velocity
        Nx = 100;               % Number of points along tailcone to integrate loads
        
        % Avoid indeterminates
        L1(L1 == 0) = 1e-20;
        L2(L2 == 0) = 1e-20;
        L3(L3 == 0) = 1e-20;
        Dcyl(Dcyl == 0) = 1e-20;

        
        % 1. Compute boundary layer thickness & pressure coefficient at fuselage TE
        
        % Ratio between BL thickness and fuselage length
        % Based on data of ESDU 79020. Last subplot of Fig. 7 (effect of Re) & Fig.
        % 5 (effect of ARf). Effect of M neglected; this whole method assumes
        % incompressible flow anyway. See FitESDUd99Data.m for the calculation of
        % the fitting function
        d99L = 0.03715*(0.4951*(ARf/10)^-1.051 + 0.4841) ...
            *(36.12*exp(-7.6200*log10(Re_f)/7) + ...
            1.939*exp(-0.6801*log10(Re_f)/7));
        
        % Ratio between boundary layer thickness (delta99) and fuselage RADIUS [-]
        dRatio = 2*ARf*d99L;
        
        % Tail cone aspect ratio; Ltc/Df = L1 + L2 + L3
        ARtc = L1 + L2 + L3;
        
        % Ratio between BL edge velocity at fuselage TE and FS velocity
        % Based on data of ESDU 78037, Figs. 6 and 7b. Effect of Re neglected, only
        % has small effect for typical bodies considered. See FitESDUvEdgeData.m
        % for the calculation of the fitting function
        vEdge = 1 + 0.5*(1-sign(-1 + 0.916*(-0.1044*(ARtc/2.56)^-1.032 + 1.109) ...
            *(-0.09516*(ARf/5)^-1.676 + 1.094)))*...
            (-1 + 0.916*(-0.1044*(ARtc/2.56)^-1.032 + 1.109) ...
            *(-0.09516*(ARf/5)^-1.676 + 1.094));
        
        % Fuselage pressure coefficient in prop-off conditions, at fuselage
        % trailing edge
        Cp = 1-vEdge^2;
        
        
        % 2. Process boundary layer velocity distribution
        
        % Create y-array in the propeller interval
        % y is expressed as fraction of fuselage radius! (N.B: Dp/Df = Rp/Rf)
        y = linspace(0,Dp,N);
        
        % Evaluate boundary-layer profile, with y as a fraction of BL thickness
        % (w.r.t. edge velocity!)
        u = f.BLprofile(y/dRatio);
        
        % Outside boundary-layer limits, substitute with 1 (still w.r.t. edge
        % velocity!)
        u(y > dRatio) = 1;
        
        % MEAN propeller inflow velocity w.r.t. edge velocity. Note cylindrical
        % coordinates!
        dr = diff(y);
        rMean = 0.5*(y(1:end-1) + y(2:end));
        uMean = 0.5*(u(1:end-1) + u(2:end));
        uInt = sum(uMean.*rMean.*dr);
        vIn = 2*uInt/Dp^2;
        
        % Express vIn w.r.t freestream velocity instead if edge velocity
        vIn = vIn*vEdge;
        
        % Correct to EQUIVALENT inflow velocity (which accounts for static pressure
        % differences), as a fraction of FS velocity
        vEq = (vIn^2 + Cp)^0.5;
        
        % Generate array for plotting, if requested
        if options == 1
            
            % Create y-array, normalized with fuselage radius
            yMax = 1.5*max([1 Dp dRatio]);
            yArray = linspace(0,yMax,floor(N/2));
            
            % Create velocity profile, normalized w.r.t FS velocity
            uArray = f.BLprofile(yArray/dRatio);
            uArray(yArray > dRatio) = 1;
            uArray = uArray*vEdge;
        end
        

        % 3. Propulsive efficiency change
        
        % Equivalent AD radius, for same disk loading as annular AD, as fraction of
        % fuselage diameter
        Req = 0.5*(Dp^2 - Dcyl^2)^0.5;
        
        % Propulsive effciency of reference configuration (with same disk loading
        % -> correct Tc)
        etap_iso = f.etap(Tc*Dp^2/Req^2/4);
        
        % Make sure thrust coefficient is expressed w.r.t. local inflow for BLI,
        % and w.r.t equivalent disk area, for the same total thrust
        Tceq = Tc/vEq^2*Dp^2/Req^2/4;
        
        % Compute isolated propeller efficiency in uniform inflow, eta_p = T*V_inf/Ps [-]
        etap = f.etap(Tceq);
        
        % Change in propulsive efficiency
        detap = etap/vEq - etap_iso;        
        
        % 4. Fuselage drag change. Numerical integration. x = 0 -> location of prop. All
        % geometrical dimensions are non-dimensionalized with fuselage diameter.
        
        % Compute friction drag coefficient: Prandtl-Schlichting formula,
        % Schlichting Ch. XXI, p. 369-641, flat-plate turbulent BL (incompressible)
        Cf = 0.455/(log10(Re_f))^2.58;
                
        % Compute induction at AD based on equivalent thrust coefficient
        a = 0.5*((1+8/pi*Tceq)^0.5-1);
        
        % Create x-array. Add extra points near propeller to capture pressure jump
        % correctly. Make sure the edge points are always included
        x = linspace(-(L1+L2),L3,Nx);
        x = [x -1e-20 -L2];
        if L3 > 0
            x = [x +1e-20];
        end
        x = unique(x);
        
        % Compute fuselage radius distribution
        r = 0.25*(2*Dcyl - (1+sign(x)).*Dcyl/L3.*x -...
            (1+sign(-x-L2)).*(1-Dcyl)/L1.*(x+L2));
        
        % Velocity distribution induced by propeller, normalized with equivalent
        % inflow velocity
        dv = a*(1+x/Req./(1+(x/Req).^2).^0.5);
        
        % Pressure distribution shape induced by propeller
        s = sign(x) - x/Req./(1 + (x/Req).^2).^0.5;
        
        % Width of x-step for integration. For N points, we have N-1 intervals
        dx = x(2:end)-x(1:end-1);
        
        % Average quantities in each interval
        rMid = (r(2:end)+r(1:end-1))/2;
        dvMid = (dv(2:end)+dv(1:end-1))/2;
        sMid = (s(2:end)+s(1:end-1))/2;
        
        % Slope
        drdx = (r(2:end)-r(1:end-1))./dx;
        
        % Friction drag distribution, dCfdx
        dCfdx = 2*pi*Cf*AR*bRatio^2*rMid.*(2*vEdge*vEq*dvMid + vEq^2*dvMid.^2);
        
        % Pressure drag distribution, dCpdx (NB: not a pressure coefficient)
        dCpdx = vEq^2*8*AR*bRatio^2*Tceq*rMid.*drdx.*sMid;
                
        % Total drag
        dCDf = sum(dCfdx.*dx);
        dCDp = sum(dCpdx.*dx);
        dCD0 = dCDf + dCDp;

        % EVEN SIMPLER METHOD: Simple tailcone, pressure only, prop at tip
        % of cone
        %{
        k = 4/pi*Tceq*vEq^2;
        c = Dp/2/ARtc;
        dCD0_fref = k*(1 - (c^2+1)^0.5 - c^2*log(((1+c^2)^0.5 - 1)/c)); % Fuselage ref area based
        dCD0 = dCD0_fref * pi/4 * ARw * bRatio^2; % Wing ref area based
        %}
        
        % 5. Plotting
        if options == 1
            
            % Create figure
            fig = figure; fig.Name = 'BLI model';
            fig.Position = [1921 31 1920 973];
            
            % Subplot 1: fuselage shape and BL velocity profile
            subplot(2,1,1)
            hold on; grid on; box on;
            xlabel('x/D_f, v/V_\infty [-]'); ylabel('y/D_f [-]')
            
            % Draw velocity profile
            H1(1) = quiver(zeros(size(yArray)),yArray/2,uArray,zeros(size(uArray)),0,'b');
            plot(uArray,yArray/2,'-b')
            
            % Draw inflow/equivalent velocity
            H1(2) = quiver(0,Dp/4,vIn,0,0,'r','linewidth',2);
            plot([vIn vIn],[-Dp/2 Dp/2],'--r')
            H1(3) = quiver(0,Dp/3,vEq,0,0,'m','linewidth',2);
            plot([vEq vEq],[-Dp/2 Dp/2],'--m')
            
            % Draw propeller diameter
            H1(4) = plot([0 0],[-Dp/2 Dp/2],'k','linewidth',5);
            
            % Draw fuselage tail cone
            xPlot = [-(L1+L2) -L2     0         L3      0       -L2      -(L1+L2)];
            yPlot = [0.5      Dcyl/2  Dcyl/2    0       -Dcyl/2 -Dcyl/2  -0.5];
            H1(5) = plot(xPlot,yPlot,'k','linewidth',2);
            
            % Set axes
            plot([-(L1+L2+L3) 1.5],[0 0],'--k')
            axis equal
            
            % Title with info
            title(['C_{p,TE} = ' num2str(Cp) ...
                ', V_{edge}/V_\infty= ' num2str(vEdge) ...
                ',.V_{in}/V_\infty = ' num2str(vIn)...
                ', V_{eq}/V_\infty = ' num2str(vEq) ...
                ', \delta_{99}/R_f = ' num2str(dRatio) ...
                ', {\Delta}\eta_p = ' num2str(detap)])
            
            % Add legend
            legend(H1,'BL velocity/V_{inf}','Inflow velocity/V_{inf}','Equivalent velocity/V_{inf}',...
                'Propeller','Tail cone');
            
            % Subplot 2: propeller-induced quantities
            subplot(2,1,2)
            hold on; grid on; box on;
            xlabel('x/D_f [-]'); ylabel('Variable [-]')
            xMid = (x(2:end)+x(1:end-1))/2;
            
            % Fuselage radius & propeller-induced quantities
            H2(1) = plot(x,r,'r');
            H2(2) = plot(x,dv,'g');
            H2(3) = plot(x,s/2,'b');
            plot(xMid,rMid,'.r')
            plot(xMid,dvMid,'.g');
            plot(xMid,sMid/2,'.b');
            H2(4) = plot(xMid,drdx,'.-c');
            
            % Loading distributions
            H2(5) = plot(xMid,100*dCfdx,'-','color',[0.5 0.5 0.5]);
            H2(6) = plot(xMid,100*dCpdx,'--','color',[0.5 0.5 0.5]);
            H2(7) = plot(xMid,100*dCpdx+100*dCfdx,'-k');
            
            % Set axes
            axis equal
            
            % Title with info
            title(['C_f = ' num2str(Cf) ...
                ', {\Delta}C_{Dp} = ' num2str(dCDp)...
                ', {\Delta}C_{Df} = ' num2str(dCDf)...
                ', {\Delta}C_{D0} = ' num2str(dCD0)])
            
            % Add legend
            legend(H2,'R/D_f','\Delta{v}','\Delta{p} shape','slope',...
                'Sectional friction drag (x100)','Sectional pressure drag (x100)',...
                'Total sectional drag (x100)');
        end
     
        % For BLI only, there is no change in lift or lift-induced drag
        if strcmp(p.AeroPropModel,'BLI')
            dCL = 0;
            dCDi = 0;
           
        % 6. IF MAIN PROPS ARE ALSO MODELLED USING LLM
        %    (For detailed comments see LLM section below)
        else
            
            % 6.1. Prepare input data & check bounds
            Rfus = 0.05;
            dy_dp = 0.05;
            dy_main = 0.025;
            SM = p.geom.SM;
            if N_conv == 0
                b_conv = 0;
                Tc_conv = 0;
            elseif N_conv ~= 0 && N_conv ~= 2
                error('LLM surrogate model only applicable to zero or two main, non-DP propulsors')
            end
            D_main = b_conv/2;
            K = 0.5 - Rfus - dy_main - D_main;
            dY = b_dp*(0.5 - (0.5 + dy_dp)/N_dp/(1 + dy_dp))/ ...
                (K - b_dp/N_dp*dy_dp/(1+dy_dp)) + dy_tip; 
            if AR > 16;             AR = 16;            
            elseif AR < 8;          AR = 8;             end 
            if D_main > 0.2;        D_main = 0.2;       
            elseif D_main < 0;      D_main = 0;         end
            if Tc_conv > 1;         Tc_conv = 1;        
            elseif Tc_conv < 0;     Tc_conv = 0;        end
            if J_conv > 2.5;        J_conv = 2.5;       
            elseif J_conv < 0.25;   J_conv = 0.25;      end
            if CL > 2.89;           CL = 2.89;          
            elseif CL < -0.075;     CL = -0.075;        end 
        
            % 6.2. Evaluate surrogate model
            if isnan(Tc_conv); Tc_conv_in = 0; else; Tc_conv_in = Tc_conv;  end
            if Tc_conv_in == 0 || b_conv == 0
                dCL = 0;
                dCD = 0;
            else
                x = [AR D_main Tc_conv_in J_conv CL];
                X = (x - SM.input.mean{1})./SM.input.range{1};
                dCLout = EvaluateSurrogateModel(SM.dCL.coeff{1},SM.dCL.exponents{1},X);
                dCDout = EvaluateSurrogateModel(SM.dCD.coeff{1},SM.dCD.exponents{1},X);
                dCL = dCLout*SM.dCL.range{1} + SM.dCL.mean{1};
                dCD = dCDout*SM.dCD.range{1} + SM.dCD.mean{1};
            end
            dCDi = dCD;
            
            % 6.3. Plotting
            if options == 1
                D_dp = (K*(dY - dy_tip) / ((N_dp/2) - 0.5 + dy_dp*((N_dp/2) - 1 + dY -dy_tip)));
                b_free = 0.5 - Rfus - D_main - dy_main - dy_dp*D_dp;
                fig = figure;
                fig.Color = [ 1 1 1];
                fig.Position = [1921          31        1920         973]; % TEMP
                hold on; grid on; box on
                theta = linspace(-90,90,50);
                h(1) = plot([0 0.5],[0 0],'color',[0.8 0.8 0.8],'linewidth',7);
                h(2) = plot(Rfus*cosd(theta),Rfus*sind(theta),'-','color',[0.8 0.8 0.8],'linewidth',2);
                plot([Rfus, Rfus+dy_main, Rfus+dy_main+D_main, Rfus+dy_main+D_main+dy_dp*D_dp],...
                    [0 0 0 0],'+k');
                theta = linspace(0,360,100);
                xcMain = Rfus+dy_main+D_main/2;
                xxMain = xcMain + D_main/2*cosd(theta);
                yyMain = D_main/2*sind(theta);
                h(3) = plot(xxMain,yyMain,'--k');
                plot(xcMain,0,'xk')
                xcDP = 0;
                h(4) = plot(NaN,NaN,'-k');
                h(5) = plot([0.5 0.5-b_free],[0 0],'b');
                h(6) = plot([0.5 0.5-b_free*dY],[0 0],'--r');
                h(7) = plot([xcDP(1)+0.5*D_dp*(1+dy_dp) xcDP(1)+0.5*D_dp*(1+dy_dp) ...
                    xcDP(end)-0.5*D_dp*(1+dy_dp) xcDP(end)-0.5*D_dp*(1+dy_dp) ...
                    xcDP(1)+0.5*D_dp*(1+dy_dp)],[-0.5*D_dp*(1+dy_dp) 0.5*D_dp*(1+dy_dp) ...
                    0.5*D_dp*(1+dy_dp) -0.5*D_dp*(1+dy_dp) -0.5*D_dp*(1+dy_dp)],':g');
                legend(h,'Wing','Fuselage','Main prop','Distr. props','b_{free}','b_{used}','b_{dp}')
                title(['C_{L,iso} = ' num2str(CL,'%.4f') ', ' ...
                    'T_{c,main} = ' num2str(Tc_conv,'%.4f') ', ' ...
                    'T_{c,dp} = ' num2str(Tc,'%.4f') ', ' ...
                    'J_{main} = ' num2str(J_conv,'%.4f') ', ' ...
                    'J_{dp} = ' num2str(J_dp,'%.4f') '       >>>       ' ...
                    '\Delta{C_L} = ' num2str(dCL,'%.4f') ', ' ...
                    '\Delta{C_D} = ' num2str(dCD,'%.6f')]);
                axis equal
                xlabel('y/b');ylabel('z/b')
            end
        end
     
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 4. Lifting line method for LEDP and/or tip mounted propulsion. Includes primary propulsors!
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Note that this method assumes:
    %   - Taper ratio = 1 (just like the LEDP method)
    %   - Wing thickness-to-chord ratio = 0.12
    %   - Clearance between DP props dy/D_dp = 0.05
    %   - DP props are one DP-prop-radius upstream of LE 
    %   - Main props are one main-prop-radius upstream of LE 
    % This might be inconsistent with the p.geom.dy, a.tc, p.geom.xc, and 
    % a.TR variables in the input file, which are not used here.
    % The props rotate inboard up.
    
    case 'LLM'
     
        % 1. Prepare input data & check bounds
        
        % Constants used in the LLM
        Rfus = 0.05;            % Fuselage radius over wing span, Rfus/D [-]
        dy_dp = 0.05;           % DP prop clearance over DP prop diameter [-], is actually the same as geom.dy!
        dy_main = 0.025;        % Clearance between main prop and fuselage, as a fraction of wing span [-]

        % Load surrogate model data (data obtained from .mat file in
        % CheckInput.m)
        SM = p.geom.SM;
        
        % For zero main props, set b_conv to zero, so zero diameter
        if N_conv == 0
            b_conv = 0;
            Tc_conv = 0;
            
        % If neither zero nor two is specified, the model can't be applied
        elseif N_conv ~= 0 && N_conv ~= 2
            error('LLM surrogate model only applicable to zero or two main, non-DP propulsors')
        end
        
        % Check number of DP props is even and within bounds
        if N_dp > 20; error('LLM surrogate model is only applicable to 0 to 10 DP-props per semi-wing'); end
        if N_dp/2 ~= round(N_dp/2); error('An even number of DP propulsors is required for the LLM surrogate model'); end
        
        % Ratio between ONE main prop diameter and wing span
        % (assuming N_main = 2! if no main props, set b_conv to zero)
        D_main = b_conv/2;
        
        % Compute ratio between used DP space (b_used) and available DP space (b_free)
        K = 0.5 - Rfus - dy_main - D_main;
        dY = b_dp*(0.5 - (0.5 + dy_dp)/N_dp/(1 + dy_dp))/ ...
            (K - b_dp/N_dp*dy_dp/(1+dy_dp)) + dy_tip;
        
        % Check input variables are not out of bounds
        if AR > 16;             AR = 16;            %               % warning('AR is too high for SM. Limiting to 16')
        elseif AR < 8;          AR = 8;             end             % warning('AR is too low for SM. Limiting to 8'); end
        if D_main > 0.2;        D_main = 0.2;       %               % warning('D_main is too high for SM. Limiting to 0.2')
        elseif D_main < 0;      D_main = 0;         end             % warning('D_main is too low for SM. Limiting to 0'); end
        if Tc_conv > 1;         Tc_conv = 1;        %               % warning('Tc_conv is too high for SM. Limiting to 1')
        elseif Tc_conv < 0;     Tc_conv = 0;        end             % warning('Tc_conv is too low for SM. Limiting to 0'); end
        if J_conv > 2.5;        J_conv = 2.5;       %               % warning('Tc_conv is too high for SM. Limiting to 2.5')
        elseif J_conv < 0.25;   J_conv = 0.25;      end             % warning('Tc_conv is too low for SM. Limiting to 0.25'); end
        if CL > 2.89;           CL = 2.89;          %               % warning('CL_iso is too high for SM. Limiting to 2.89')
        elseif CL < -0.075;     CL = -0.075;        end             % warning('CL_iso is too low for SM. Limiting to -0.075'); end
        if dy_tip > 0.5;        dy_tip = 0.5;       %               % warning('dy_tip is too high for SM. Limiting to 0.5')
        elseif dy_tip < 0;      dy_tip = 0;         end             % warning('dy_tip is too low for SM. Limiting to 0'); end
        if Tc > 1;              Tc = 1;             % Happens often % warning('Tc_dp is too high for SM. Limiting to 1')
        elseif Tc < 0;          Tc = 0;             end             % warning('Tc_dp is too low for SM. Limiting to 0'); end
        if J_dp > 2.5;          J_dp = 2.5;         %               % warning('J_dp is too high for SM. Limiting to 2.5')
        elseif J_dp < 0.25;     J_dp = 0.25;        end             % warning('J_dp is too low for SM. Limiting to 0.25'); end
        
        % Separate warning for span fraction, to tell the corresponding b_dp value
        if dY > 1
            dY = 1;
            b_dp_lim = 2*K*(1-dy_tip)/(1 + 2/N_dp/(1 + dy_dp)* ...
                (dy_dp*(1 - dy_tip) - dy_tip - 0.5));
            warning(['Ratio between DP span and available span is ' ...
                'too high for SM. Limiting to 1 (b_dp = ' num2str(b_dp_lim) ')'])
            
        elseif dY < 0
            dY = 0;
            warning(['Ratio between DP span and available span is '...
                'too low for SM. Limiting to 0 (b_dp = 0)']);
        end
              
        
        % 2. Evaluate surrogate model
        
        % If thrust coefficient is NaN (often because D2W is zero and then Matlab
        % gets a 0/0), set to zero instead, to provide zero deltas. Be careful with
        % error "propagation", since now the NaN will not be propagated.
        if isnan(Tc_conv); Tc_conv_in = 0; else; Tc_conv_in = Tc_conv;  end
        if isnan(Tc); Tc_dp_in = 0; else; Tc_dp_in = Tc; end
        
        % If DP array thrust is zero or span fraction is zero, model as if
        % no DP props were used
        if Tc ==0 || b_dp == 0
            N_dp_in = 0;
        else
            N_dp_in = N_dp/2;
        end
        
        % Distinguish between zero-DP case and positive DP case
        if N_dp_in == 0
            
            % For zero thrust, provide zero deltas
            if Tc_conv_in == 0 || b_conv == 0
                dCL = 0;
                dCD = 0;
                
                % For a positive Tc, evaluate surrogate model
            else
                
                % Gather input array & scale
                x = [AR D_main Tc_conv_in J_conv CL];
                X = (x - SM.input.mean{1})./SM.input.range{1};
                
                % Evaluate dCL & dCD
                dCLout = EvaluateSurrogateModel(SM.dCL.coeff{1},SM.dCL.exponents{1},X);
                dCDout = EvaluateSurrogateModel(SM.dCD.coeff{1},SM.dCD.exponents{1},X);
                
                % De-scale
                dCL = dCLout*SM.dCL.range{1} + SM.dCL.mean{1};
                dCD = dCDout*SM.dCD.range{1} + SM.dCD.mean{1};
            end
            
        % Generic number of DP propulsors
        else
            
            % For zero or undefined thrust, provide zero deltas
            if Tc_conv_in == 0 && Tc_dp_in == 0
                dCL = 0;
                dCD = 0;
                
            % For a positive Tc, evaluate surrogate model
            else
                
                % Index corresponding to N props
                idx = N_dp_in + 1;
                
                % Gather input array & scale
                x = [AR D_main Tc_conv_in J_conv CL dY dy_tip Tc_dp_in J_dp];
                X = (x - SM.input.mean{idx})./SM.input.range{idx};
                
                % Evaluate dCL & dCD
                dCLout = EvaluateSurrogateModel(SM.dCL.coeff{idx},SM.dCL.exponents{idx},X);
                dCDout = EvaluateSurrogateModel(SM.dCD.coeff{idx},SM.dCD.exponents{idx},X);
                
                % De-scale
                dCL = dCLout*SM.dCL.range{idx} + SM.dCL.mean{idx};
                dCD = dCDout*SM.dCD.range{idx} + SM.dCD.mean{idx};
            end
            
        end
        
        
        % 3. Plotting
        if options == 1
            
            % Ratio between single DP prop diameter and wing span (for plotting)
            D_dp = (K*(dY - dy_tip) / ((N_dp/2) - 0.5 + dy_dp*((N_dp/2) - 1 + dY -dy_tip)));
            
            % Ratio between free space per semi span, and wing span (for plotting)
            b_free = 0.5 - Rfus - D_main - dy_main - dy_dp*D_dp;
            
            % Create figure showing position of props
            fig = figure;
            fig.Color = [ 1 1 1];
            fig.Position = [1921          31        1920         973]; % TEMP
            hold on; grid on; box on
            
            % Plot wing and fuselage
            theta = linspace(-90,90,50);
            h(1) = plot([0 0.5],[0 0],'color',[0.8 0.8 0.8],'linewidth',7);
            h(2) = plot(Rfus*cosd(theta),Rfus*sind(theta),'-','color',[0.8 0.8 0.8],'linewidth',2);
            
            % Plot locations of fuselage radius, main prop inside, main prop outside,
            % and end of used main space
            plot([Rfus, Rfus+dy_main, Rfus+dy_main+D_main, Rfus+dy_main+D_main+dy_dp*D_dp],...
                [0 0 0 0],'+k');
            
            % Draw main prop
            theta = linspace(0,360,100);
            xcMain = Rfus+dy_main+D_main/2;
            xxMain = xcMain + D_main/2*cosd(theta);
            yyMain = D_main/2*sind(theta);
            h(3) = plot(xxMain,yyMain,'--k');
            plot(xcMain,0,'xk')
            
            % Draw DP props
            xStart = 0.5-dy_tip*b_free; % center of most outboard prop
            if N_dp ~= 0
                xcDP = zeros(1,N_dp/2);
            else
                xcDP = 0;
                h(4) = plot(NaN,NaN,'-k');
            end
            for i = 1:N_dp/2          % loop over DP props
                xcDP(i) = xStart - (i-1)*(D_dp + dy_dp*D_dp);
                xxDP = xcDP(i) + D_dp/2*cosd(theta);
                yyDP = D_dp/2*sind(theta);
                h(4) = plot(xxDP,yyDP,'-k');
                plot(xcDP(i),0,'xk')
            end
            
            % Add lines to show b_free and b_dp
            h(5) = plot([0.5 0.5-b_free],[0 0],'b');
            h(6) = plot([0.5 0.5-b_free*dY],[0 0],'--r');
            h(7) = plot([xcDP(1)+0.5*D_dp*(1+dy_dp) xcDP(1)+0.5*D_dp*(1+dy_dp) ...
                xcDP(end)-0.5*D_dp*(1+dy_dp) xcDP(end)-0.5*D_dp*(1+dy_dp) ...
                xcDP(1)+0.5*D_dp*(1+dy_dp)],[-0.5*D_dp*(1+dy_dp) 0.5*D_dp*(1+dy_dp) ...
                0.5*D_dp*(1+dy_dp) -0.5*D_dp*(1+dy_dp) -0.5*D_dp*(1+dy_dp)],':g');
            
            % Legend
            legend(h,'Wing','Fuselage','Main prop','Distr. props','b_{free}','b_{used}','b_{dp}')
            
            % Add data in title
            title(['C_{L,iso} = ' num2str(CL,'%.4f') ', ' ...
                'T_{c,main} = ' num2str(Tc_conv,'%.4f') ', ' ...
                'T_{c,dp} = ' num2str(Tc,'%.4f') ', ' ...
                'J_{main} = ' num2str(J_conv,'%.4f') ', ' ...
                'J_{dp} = ' num2str(J_dp,'%.4f') '       >>>       ' ...
                '\Delta{C_L} = ' num2str(dCL,'%.4f') ', ' ...
                '\Delta{C_D} = ' num2str(dCD,'%.6f')]);
            
            % Format axis
            axis equal
            xlabel('y/b');ylabel('z/b')
        end
    
        % No effect on propulsive efficiency
        detap = 0;
        
        % All drag included as  dCDi, even though it also contains a dCD0
        % component
        dCDi = dCD;
        dCD0 = 0;
        
     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 5. No effect of propeller on wing 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 'none'
    dCL = 0;
    dCDi = 0;
    dCD0 = 0;
    detap = 0;
   
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 6. Wrongly specified configuration
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    otherwise
    error('The specified aero-propulsive model does not exist')
end



