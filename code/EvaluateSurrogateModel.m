function Y = EvaluateSurrogateModel(coeff,E,X)
% This function evaluates a polynomial fit of order N over K variables,
% generated by CreateSurrogateModel.m. 
% 
% Input:
%   - coeff: 1D array of length M containing the monomial coefficients,
%       where M equals the number of monomials
%   - E: 2D array of size [M x K] containing the exponents of the variables
%       in each monomial.
%   - X: Query vector where the fit is evaluated. Size(X) = [n x K], where
%       n is the number of query points. Each column is a variable. The
%       number and order of variables must be the same as the one used in
%       CreateSurrogateModel.m
%
% Output:
%   - Y: Dependent parameter (1D array of length n) obtained from the
%       surrogate model.
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 17-05-19
%%% Last modified: 17-05-19


% Number of variables
K = size(X,2);

% Number of monomials
M = length(coeff);

% Number of data points
n = size(X,1);

% Loop over monomials
mon = ones(n,M);
for i = 1:M
    
    % Multiply 1 by monomial coefficient
    mon(:,i) = mon(:,i)*coeff(i);
    
    % Multiply monomial by all variables and their corresponding exponent
    for k = 1:K   
        mon(:,i) = mon(:,i).*X(:,k).^E(i,k);
    end
end

% Add monomials to get function value
Y = sum(mon,2);

